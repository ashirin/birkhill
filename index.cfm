
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

	<head>
		<CFINCLUDE TEMPLATE="./_includes/_head_meta.cfm" />
    <CFINCLUDE TEMPLATE="./_includes/_head_css.cfm" />
    <link type="text/css" rel="stylesheet" href="./_css/carousel.css">
    <CFINCLUDE TEMPLATE="./_includes/_head_shim.cfm" />
  </head>


  <body>
    <div class="wrapper">

      <CFINCLUDE template="./parts/_header.cfm" />
      <cfinclude template="./_includes/_carousel.cfm" />
      <CFINCLUDE template="./parts/_marketing.cfm" />
      <CFINCLUDE template="./parts/_circles.cfm" />
      <cfinclude template="./parts/_testimonials.cfm" />
      <CFINCLUDE template="./parts/_boxes1.cfm" />
      <cfinclude template="./parts/_socials.cfm" />
  		<footer>
				<div id="footer">
					<div class="container">
						<CFINCLUDE template="./parts/_footer.cfm" />
					</div><!--- ./container --->
    		</div>
			</footer>
		</div><!--- /#wrapper --->
		<CFINCLUDE TEMPLATE="./_includes/_head_js.cfm" />
	</body>
</html>
