CHANGELOG
=========

*25/08/2016*

Gallery db table added

    CREATE TABLE [dbo].[gallery](
      [image_id] [int] IDENTITY(1,1) NOT NULL,
      [page_uuid] [varchar](50) NULL,
      [image_url] [varchar](50) NULL,
     CONSTRAINT [PK_gallery_1] PRIMARY KEY CLUSTERED
    (
      [image_id] ASC
    )WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
    ) ON [PRIMARY]
