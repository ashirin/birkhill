
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

  	<head>

  		<CFINCLUDE TEMPLATE="./_includes/_head_meta.cfm" />

        <CFINCLUDE TEMPLATE="./_includes/_head_css.cfm" />

        <CFINCLUDE TEMPLATE="./_includes/_head_shim.cfm" />

	</head>


	<body>

		<div class="wrapper">

  			<main>

				<CFINCLUDE template="./parts/_header.cfm" />

				<div id="layerslider" style="width: 100%; height: 365px; margin:0 auto; margin-bottom: 0px;">

					<div class="ls-slide" data-ls="slidedelay:5000;transition2d:5;">
						<img src="./_img/test_slider1.jpg" class="ls-bg" />
						<h1 class="ls-l" style="top:23px;left:577px;font-weight: 300;font-family:Lato, 'Open Sans', sans-serif;font-size:55px;color:#ef3e37;white-space: nowrap;" data-ls="offsetxin:50;durationin:2000;rotateyin:60;transformoriginin:right 50% 0;offsetxout:-50;durationout:2000;showuntil:7500;rotateyout:-60;transformoriginout:left 50% 0;">MHRA-Licensed</h1>
						<h1 class="ls-l" style="top:71px;left:424px;font-weight: 500; text-align: center; opacity: .7;font-family:Oswald, Lato, 'Open Sans', sans-serif;font-size:80px;color:#ffffff;white-space: nowrap;" data-ls="offsetxin:0;durationin:3000;delayin:1500;easingin:easeOutElastic;rotatexin:90;transformoriginin:50% bottom 0;offsetxout:0;durationout:2000;rotateout:-90;transformoriginout:left bottom 0;">Pan-European Presence</h1>
						<h2 class="ls-l" style="top:160px;left:512px;font-weight: 500;font-family:Lato, 'Open Sans', sans-serif;font-size:60px;color:#ef3e37;white-space: nowrap;" data-ls="offsetxin:-50;durationin:3000;delayin:1200;rotateyin:-60;transformoriginin:left 50% 0;offsetxout:50;durationout:2000;showuntil:6000;rotateyout:60;transformoriginout:right 50% 0;">SHORT LINE</h2>
						<h2 class="ls-l" style="top:223px;left:582px;font-weight: 500;font-family:Lato, 'Open Sans', sans-serif;font-size:45px;color:#ef3e37;white-space: nowrap;" data-ls="offsetxin:-50;durationin:3000;delayin:2000;rotateyin:-60;transformoriginin:left 50% 0;offsetxout:50;durationout:3000;showuntil:7500;rotateyout:60;transformoriginout:right 50% 0;"> WHOLESALER</h2>
						<p class="ls-l" style="top:286px;left:492px;font-weight: 300;height:40px;padding-right:10px;padding-left:10px;font-family:Lato, 'Open Sans', sans-serif;font-size:36px;line-height:37px;color:#ffffff;background:#ef3e37;border-radius:4px;white-space: nowrap;" data-ls="durationin:2200;delayin:2500;rotatein:20;rotatexin:30;scalexin:1.5;scaleyin:1.5;transformoriginin:left 50% 0;durationout:1500;showuntil:6000;rotateout:20;rotatexout:-30;scalexout:0;scaleyout:0;transformoriginout:left 50% 0;">SINCE 1959</p>
					</div>

					<div class="ls-slide" data-ls="slidedelay:5000;transition2d:5;">
						<img src="./_img/test_slider2.jpg" class="ls-bg" />
						<h1 class="ls-l" style="top:19px;left:445px;font-weight: 300;font-family:Oswald, Lato;font-size:60px;color:#ffffff;white-space: nowrap;" data-ls="offsetxin:50;durationin:2000;rotateyin:60;transformoriginin:right 50% 0;offsetxout:-50;durationout:3000;showuntil:6000;rotateyout:-60;transformoriginout:left 50% 0;">Parallel Imports</h1>
						<h2 class="ls-l" style="top:100px;left:364px;font-weight: 800;font-family:Lato, 'Open Sans', sans-serif;font-size:40px;color:#ef3e37;white-space: nowrap;" data-ls="offsetxin:-50;durationin:2000;delayin:500;rotateyin:-60;transformoriginin:left 50% 0;offsetxout:50;durationout:3000;showuntil:8000;rotateyout:60;transformoriginout:right 50% 0;">& Generic</h2>
						<h3 class="ls-l" style="top:140px;left:472px;font-weight: 500;font-family:Oswald, Lato;font-size:60px;color:#ffffff;white-space: nowrap;" data-ls="offsetxin:0;durationin:3500;delayin:1500;easingin:easeOutElastic;rotateyin:90;transformoriginin:left 50% 0;offsetxout:0;durationout:1000;showuntil:6000;rotateyout:90;transformoriginout:left 50% 0;">PHARMACEUTICALS</h3>
						<h3 class="ls-l" style="top:217px;left:404px;font-weight: 300;font-family:Lato, 'Open Sans', sans-serif;font-size:52px;color:#ef3e37;white-space: nowrap;" data-ls="offsetxin:0;durationin:3500;delayin:2500;easingin:easeOutElastic;rotateyin:90;transformoriginin:left 50% 0;offsetxout:0;durationout:500;rotateyout:90;transformoriginout:left 50% 0;">For Retail Pharmacies</h3>
						<h1 class="ls-l" style="top:292px;left:481px;font-weight: 300;height:40px;padding-right:15px;padding-left:15px;font-family:Lato, HelveticaNeue-UltraLight, HelveticaNeue-Light, HelveticaNeue, Helvetica, Arial, serif;font-size:38px;line-height:37px;color:#ffffff;background:#ef3e37;border-radius:8px;white-space: nowrap;" data-ls="offsetxin:0;durationin:2000;delayin:2000;easingin:easeInQuint;rotateyin:180;transformoriginin:right 50% 0;offsetxout:0;durationout:1000;showuntil:7000;easingout:easeOutQuint;transformoriginout:right 50% 0;">& the NHS</h1>
					</div>

					<div class="ls-slide" data-ls="slidedelay:5000;transition2d:5;">
						<img src="./_img/test_slider3.jpg" class="ls-bg" />
						<h1 class="ls-l" style="top:31px;left:585px;font-weight: 300;font-family:Lato, 'Open Sans', sans-serif;font-size:66px;color:#ffffff;white-space: nowrap;" data-ls="offsetxin:50;durationin:2000;rotateyin:60;transformoriginin:right 50% 0;offsetxout:-50;durationout:3000;showuntil:7000;rotateyout:-60;transformoriginout:left 50% 0;">DELIVERING QUALITY</h1>
						<h2 class="ls-l" style="top:96px;left:499px;font-weight: 800;font-family:Lato, 'Open Sans', sans-serif;font-size:60px;color:#ef3e37;white-space: nowrap;" data-ls="offsetxin:-50;durationin:3000;delayin:500;rotateyin:-60;transformoriginin:left 50% 0;offsetxout:50;durationout:3000;showuntil:8000;rotateyout:60;transformoriginout:right 50% 0;">& Service</h2>
						<h1 class="ls-l" style="top:172px;left:587px;font-weight: 300;height:40px;padding-top:15px;padding-right:15px;padding-bottom:15px;padding-left:15px;font-family:Lato, HelveticaNeue-UltraLight, HelveticaNeue-Light, HelveticaNeue, Helvetica, Arial, serif;font-size:55px;line-height:37px;color:#ffffff;background:#ef3e37;border-radius:8px;white-space: nowrap;" data-ls="offsetxin:0;durationin:2000;easingin:easeInQuint;rotateyin:180;transformoriginin:right 50% 0;offsetxout:0;durationout:1000;showuntil:7000;easingout:easeOutQuint;transformoriginout:right 50% 0;">Without Compromising</h1>
						<h3 class="ls-l" style="top:243px;left:509px;font-weight: 300;font-family:Oswald, Lato;font-size:44px;color:#ffffff;white-space: nowrap;" data-ls="offsetxin:0;durationin:3500;delayin:2500;easingin:easeOutElastic;rotateyin:90;transformoriginin:left 50% 0;offsetxout:0;durationout:500;rotateyout:90;transformoriginout:left 50% 0;">On Price</h3>
					</div>

				</div>

      			<section class="module content">
        			<div class="container">
          				<CFOUTPUT QUERY="pagedetails">
							#page_content1#
						</CFOUTPUT>
						<br />
						<CFINCLUDE template="./parts/_boxes1.cfm" />
        			</div><!--- ./container --->
      			</section>

      			<section class="module parallax parallax-2">
        			<div class="overlay2">
						<div class="container">
							<CFOUTPUT QUERY="pagedetails">
								#page_content2#
							</CFOUTPUT>
							<br />
							<CFINCLUDE template="./parts/_boxes2.cfm" />
        				</div><!--- ./container --->
					</div><!--- ./overlay1 --->
				</section>

      			<section class="module content">
        			<div class="container">
						<div class="row">
							<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 contact">
								<CFOUTPUT QUERY="pagedetails">
									#page_content3#
								</CFOUTPUT>
							</div><!--- /.col --->
							<div class="col-lg-6 col-md-6 hidden-sm hidden-xs">
								<img src="./_img/europe_map.jpg" />
							</div><!--- /.col --->
						</div><!--- /.row --->
						<br />
						<a class="btn btn-lg btn-primary" href="./contact.cfm"><span style="color: #fff">Contact Us</span></a>
					</div><!--- ./container --->
      			</section>

  			</main>

  			<footer>
				<div id="footer">
					<div class="container">
						<br /><br />
						<CFINCLUDE template="./parts/_footer.cfm" />
					</div><!--- ./container --->
    			</div>
			</footer>

		</div><!--- /#wrapper --->

		<CFINCLUDE TEMPLATE="./_includes/_head_js.cfm" />

		<script type="text/javascript" language="javascript">
			$(document).ready(function() {
        		$('#layerslider').layerSlider({
        			responsiveUnder: 1280,
        			layersContainer: 1280,
        			startInViewport: false,
        			pauseOnHover: false,
        			forceLoopNum: false,
        			keybNav: false,
        			navPrevNext: false,
        			hoverPrevNext: false,
        			navStartStop: false,
        			navButtons: false,
        			autoPlayVideos: false,
        			skinsPath: './_plugins/layerslider/layerslider/skins/'});
    		});
		</script>

	</body>

</html>
