
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

  	<head>

  		<CFINCLUDE TEMPLATE="./_includes/_head_meta.cfm" />

        <CFINCLUDE TEMPLATE="./_includes/_head_css.cfm" />

        <CFINCLUDE TEMPLATE="./_includes/_head_shim.cfm" />

	</head>


	<body>

		<div class="wrapper">

  			<main>

				<CFINCLUDE template="./parts/_header.cfm" />

      			<section class="module parallax parallax-3">
					<div class="overlay1">
	        			<div class="container">
	        				<br />
							<div class="row">
								<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
									<h1><CFOUTPUT>#pagedetails.menu_name#</CFOUTPUT></h1>
								</div><!--- ./col --->
							</div><!--- ./row --->
	        			</div><!--- ./container --->
					</div><!--- ./overlay1 --->
      			</section>

      			<section class="module content">
        			<div class="container">
          				<CFOUTPUT QUERY="pagedetails">
							#page_content1#
						</CFOUTPUT>
        			</div><!--- ./container --->
      			</section>

      			<section class="module parallax parallax-2">
        			<div class="overlay2">
						<div class="container">
							<CFOUTPUT QUERY="pagedetails">
								#page_content2#
							</CFOUTPUT>
        				</div><!--- ./container --->
					</div><!--- ./overlay1 --->
				</section>

      			<section class="module content">
        			<div class="container">
						<div class="row">
							<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 contact">
								<CFOUTPUT QUERY="pagedetails">
									#page_content3#
								</CFOUTPUT>
							</div><!--- /.col --->
							<div class="col-lg-6 col-md-6 hidden-sm hidden-xs">
								<img src="./_img/europe_map.jpg" />
							</div><!--- /.col --->
						</div><!--- /.row --->
						<br />
						<a class="btn btn-lg btn-primary" href="./contact.cfm"><span style="color: #fff">Contact Us</span></a>
					</div><!--- ./container --->
      			</section>

  			</main>

  			<footer>
				<div id="footer">
					<div class="container">
						<br /><br />
						<CFINCLUDE template="./parts/_footer.cfm" />
					</div><!--- ./container --->
    			</div>
			</footer>

		</div><!--- /#wrapper --->

		<CFINCLUDE TEMPLATE="./_includes/_head_js.cfm" />

	</body>

</html>
