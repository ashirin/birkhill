
<!--- Authentication of user --->
<CFINCLUDE TEMPLATE="../authenticate.cfm">

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
  <CFINCLUDE TEMPLATE="../_includes/_head_meta.cfm" />
  <CFINCLUDE TEMPLATE="../_includes/_head_css.cfm" />
  <style>
    label.error {
      margin-top: 10px;
      color: #DD4B39;
    }
  </style>
  <CFINCLUDE TEMPLATE="../_includes/_head_shim.cfm" />
  <!--- Menu Bookmark Variable --->
  <CFSET menupage = 'managecircles' />
</head>
<!--- Logged in user details --->
<CFSET loginObj = CreateObject("component","birkhillincludes.login") />
<CFSCRIPT>
  loginObj.dsn = dsn;
  loginObj.username = cookie.myadmin_username;
  loginObj.password = cookie.myadmin_password;
  logindetails = loginObj.getLoginDetails();
</CFSCRIPT>

<!--- Get Dropdown details --->
<CFSET pagesObj = CreateObject("component","birkhillincludes.pages") />
<CFSCRIPT>
  pagesObj.dsn = #dsn#;
  toplevelmenus = pagesObj.getTopLevelMenus();
</CFSCRIPT>

<!--- Get page info if not an add --->
<CFSET qObj = CreateObject("component","birkhillincludes.box") />
<CFSCRIPT>
  qObj.dsn = #dsn#;
  qObj.id = 1;
  testimonials = qObj.gettestimonials();
</CFSCRIPT>
<CFSET menupage = 'managetestimonials' />
<body class="hold-transition sidebar-mini skin-blue">
  <div class="wrapper">
    <CFINCLUDE TEMPLATE="../_includes/_header.cfm" />
    <CFINCLUDE TEMPLATE="../_includes/_menu.cfm" />
    <!--- Content Wrapper. Contains page content --->
    <div class="content-wrapper">
      <section class="content-header">
        <h1>
          Edit Testimonials
        </h1>
        <ol class="breadcrumb">
          <li><a href="<CFOUTPUT>#request.baseurl#</CFOUTPUT>dashboard/dashboard.cfm"><i class="fa fa-dashboard"></i> Dashboard</a></li>
          <li class="active"><i class="fa fa-file-text"></i> Manage Testimonials</li>
        </ol>
      </section>
      <section class="content">
        <div class="row">
          <form role="form" method="post" action="<CFOUTPUT>#request.baseurl#</CFOUTPUT>pages/testimonials_process.cfm" id="myform" name="myform" enctype="multipart/form-data">
            <CFOUTPUT>
              <input type="hidden" name="action" value="edit">
            </CFOUTPUT>
            <div class="col-md-12">
              <div class="row">
                <div class="col-md-8">
                  <div class="form-group">
                    <label for="line1">Testimonial 1</label>
                    <input type="text" class="form-control" name="line1" id="line1" placeholder="Link to" <CFOUTPUT QUERY="testimonials">value="#line1#"</CFOUTPUT>/>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-8">
                  <div class="form-group">
                    <label for="line2">Testimonial 2</label>
                    <input type="text" class="form-control" name="line2" id="line2" placeholder="Link to" <CFOUTPUT QUERY="testimonials">value="#line2#"</CFOUTPUT>/>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-8">
                  <div class="form-group">
                    <label for="line3">Testimonial 3</label>
                    <input type="text" class="form-control" name="line3" id="line3" placeholder="Link to" <CFOUTPUT QUERY="testimonials">value="#line3#"</CFOUTPUT>/>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-8">
                  <div class="form-group">
                    <label for="line4">Testimonial 4</label>
                    <input type="text" class="form-control" name="line4" id="line4" placeholder="Link to" <CFOUTPUT QUERY="testimonials">value="#line4#"</CFOUTPUT>/>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-12">
              <div class="row">
                <div class="col-md-8">
                  <div class="form-group">
                    <label for="line1">Testimonial 5</label>
                    <input type="text" class="form-control" name="line5" id="line5" placeholder="Link to" <CFOUTPUT QUERY="testimonials">value="#line5#"</CFOUTPUT>/>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-8">
                  <div class="form-group">
                    <label for="line2">Testimonial 6</label>
                    <input type="text" class="form-control" name="line6" id="line6" placeholder="Link to" <CFOUTPUT QUERY="testimonials">value="#line6#"</CFOUTPUT>/>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12">
                  <div class="box-footer">
                    <button type="submit" class="btn btn-primary"><CFOUTPUT>Edit Testimonials</CFOUTPUT></button>
                  </div><!--- /.box-footer --->
                </div><!--- /.col --->
              </div><!--- /.row --->
            </div><!--- /.row --->
          </form>
        </div>
      </section><!--- /.content --->
    </div><!--- /.content-wrapper --->
    <CFINCLUDE TEMPLATE="../_includes/_footer.cfm" />
    <CFINCLUDE TEMPLATE="../_includes/_settings.cfm" />
  </div><!--- ./wrapper --->
  <CFINCLUDE TEMPLATE="../_includes/_head_js.cfm" />
  <script type="text/javascript" language="javascript">
    $(document).ready(function() {
      /* Form validation */
      $("#myform").validate({
        rules: {
          box_heading1: "required",
          box_heading2: "required",
          box_heading3: "required",
          box_content1: "required",
          box_content2: "required",
          box_content3: "required"
        },
        messages: {
          box_heading1: "Please enter Box Heading 1",
          box_heading2: "Please enter Box Heading 2",
          box_heading3: "Please enter Box Heading 3",
          box_content1: "Please enter Page Content 1",
          box_content2: "Please enter Page Content 2",
          box_content3: "Please enter Page Content 3"
        }
      });

      /* Upload image files */
      $('.btn-file :file').on('fileselect', function(event, numFiles, label) {

        var input = $(this).parents('.input-group').find(':text'),
        log = numFiles > 1 ? numFiles + ' files selected' : label;

        if( input.length ) {
          input.val(log);
        } else {
          if( log ) alert(log);
        }

      });

      /* Upload image files */
      $(document).on('change', '.btn-file :file', function() {
        var input = $(this),
        numFiles = input.get(0).files ? input.get(0).files.length : 1,
        label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
        input.trigger('fileselect', [numFiles, label]);
      });

      /* CKEditor initilizations */
      CKEDITOR.replace('box_content1');
      CKEDITOR.replace('box_content2');
      CKEDITOR.replace('box_content3');
    });
  </script>
  <CFINCLUDE TEMPLATE="../_includes/_toast.cfm" />
</body>
</html>
