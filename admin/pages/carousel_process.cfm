<!--- Authentication of user --->
<CFINCLUDE TEMPLATE="../authenticate.cfm">
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
  <CFINCLUDE TEMPLATE="../_includes/_head_meta.cfm" />
  <CFINCLUDE TEMPLATE="../_includes/_head_css.cfm" />
  <style>
    label.error {
      margin-top: 10px;
      color: #DD4B39;
    }
  </style>
  <CFINCLUDE TEMPLATE="../_includes/_head_shim.cfm" />
  <!--- Menu Bookmark Variable --->
  <CFSET menupage = 'manageboxes' />
</head>
<!--- Logged in user details --->
<CFSET loginObj = CreateObject("component","birkhillincludes.login") />
<CFSCRIPT>
  loginObj.dsn = dsn;
  loginObj.username = cookie.myadmin_username;
  loginObj.password = cookie.myadmin_password;
  logindetails = loginObj.getLoginDetails();
</CFSCRIPT>
<body class="hold-transition sidebar-mini skin-blue">
  <div class="wrapper">
    <CFINCLUDE TEMPLATE="../_includes/_header.cfm" />
    <CFINCLUDE TEMPLATE="../_includes/_menu.cfm" />
    <!--- Content Wrapper. Contains page content --->
    <div class="content-wrapper">
      <section class="content-header">
        <h1>
          Edit Boxes
        </h1>
        <ol class="breadcrumb">
          <li><a href="<CFOUTPUT>#request.baseurl#</CFOUTPUT>dashboard/dashboard.cfm"><i class="fa fa-dashboard"></i> Dashboard</a></li>
          <li class="active"><i class="fa fa-file-text"></i> Edit Box</li>
        </ol>
      </section>
      <section class="content">
        <div class="row">
          <!--- Send form details to the database --->
          <CFSET carouselObj = CreateObject("component","birkhillincludes.box") />
          <CFSCRIPT>
            carouselObj.dsn = dsn;
            carouselObj.action = 'edit';

            carouselObj.header1 = header1;
            carouselObj.header2 = header2;
            carouselObj.header3 = header3;
            carouselObj.header4 = header4;

            carouselObj.span1 = span1;
            carouselObj.span2 = span2;
            carouselObj.span3 = span3;
            carouselObj.span4 = span4;

            carouselObj.currentimage1 = currentimage1;
            carouselObj.currentimage2 = currentimage2;
            carouselObj.currentimage3 = currentimage3;
            carouselObj.currentimage4 = currentimage4;

            carouselObj.uploadfile1 = uploadfile1;
            carouselObj.uploadfile2 = uploadfile2;
            carouselObj.uploadfile3 = uploadfile3;
            carouselObj.uploadfile4 = uploadfile4;

            myresult = carouselObj.carousel_process();
          </CFSCRIPT>
          <CFOUTPUT>
            <CFSET locationurl = "./carousel.cfm?action=edit&toastcode=" & "#myresult.toastCode#" & "&toasttitle=" & "#myresult.toastTitle#" & "&toastmessage=" & "#myresult.toastMessage#" />
          </CFOUTPUT>
          <CFLOCATION URL="#locationurl#" />
        </div><!--- /.row --->
      </section><!--- /.content --->
    </div><!--- /.content-wrapper --->
    <CFINCLUDE TEMPLATE="../_includes/_footer.cfm" />
    <CFINCLUDE TEMPLATE="../_includes/_settings.cfm" />
  </div><!--- ./wrapper --->
  <CFINCLUDE TEMPLATE="../_includes/_head_js.cfm" />
</body>
</html>
