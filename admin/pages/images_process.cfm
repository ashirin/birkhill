
<!--- Authentication of user --->
<CFINCLUDE TEMPLATE="../authenticate.cfm" />

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <CFINCLUDE TEMPLATE="../_includes/_head_meta.cfm" />
    <CFINCLUDE TEMPLATE="../_includes/_head_css.cfm" />
    <CFINCLUDE TEMPLATE="../_includes/_head_shim.cfm" />

    <!--- Menu Bookmark Variable --->
    <CFSET menupage = 'managepages' />
</head>

<!--- Send form details to the database --->
<CFSET imagesObj = CreateObject("component","birkhillincludes.pages") />
<CFIF form.action == 'add'>
    <CFIF #form.UPLOADER_COUNT# LTE 0>
        <cfset myresult.toastCode = -1 />
        <cfset myresult.toastTitle = "Error" />
        <cfset myresult.toastMessage = "Sorry, you must upload an image!" />
    <CFELSE>
        <CFLOOP index="i" list="#Form.FieldNames#" delimiters=",">
            <CFSCRIPT>
                if ((Left(i, 8) == 'UPLOADER') AND (Right(i, 4) EQ 'NAME')) {
                    imagesObj.dsn = dsn;
                    imagesObj.action = form.action;
                    imagesObj.page_uuid = form.page_uuid;
                    imagesObj.image_url = form[i];
                    myresult = imagesObj.imageProcess();
                    if (myresult.toastCode == -1) {
                        break;
                    } else {
                        myresult.toastMessage = "Images has been created!";
                    }
                }
            </CFSCRIPT>
        </CFLOOP>
    </CFIF>
<CFELSE>
    <CFSCRIPT>
        imagesObj.dsn = dsn;
        imagesObj.action = form.action;
        imagesObj.image_id = form.image_id;
        imagesObj.page_uuid = form.page_uuid;
        if (form.action neq 'delete') {
            if (form.image_url neq '') {
                imagesObj.image_url = form.image_url;
            }
        }
        myresult = imagesObj.imageProcess();
    </CFSCRIPT>
</CFIF>
<body class="hold-transition sidebar-mini skin-blue">

    <div class="wrapper">
        <CFINCLUDE TEMPLATE="../_includes/_head_js.cfm" />
        <script type="text/javascript">
            // var dom = window.opener.document;
            // $(dom).find('#gallery-tab').tab('show');
            window.onunload = refreshParent;
            window.close();
            function refreshParent() {
                window.opener.location.reload();
                localStorage.setItem("imageAdd", true);
            }
        </script>
        <!--- Content Wrapper. Contains page content --->
        <div class="content-wrapper">
            <!--- <CFSET locationurl = "./manage_images.cfm?toastcode=" & "#myresult.toastCode#" & "&toasttitle=" & "#myresult.toastTitle#" & "&toastmessage=" & "#myresult.toastMessage#" />

            <CFLOCATION URL="#locationurl#" /> --->

            <cfdump var="#myresult#" />
        </div>
        <!--- /.content-wrapper --->

        <CFINCLUDE TEMPLATE="../_includes/_footer.cfm" />
        <CFINCLUDE TEMPLATE="../_includes/_settings.cfm" />
    </div>
    <!--- ./wrapper --->

</body>
</html>