<!--- Authentication of user --->
<CFINCLUDE TEMPLATE="../authenticate.cfm">

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <CFINCLUDE TEMPLATE="../_includes/_head_meta.cfm" />
    <CFINCLUDE TEMPLATE="../_includes/_head_css.cfm" />

    <style>
        label.error {
            margin-top: 10px;
            color: #DD4B39;
        }
    </style>

    <CFINCLUDE TEMPLATE="../_includes/_head_shim.cfm" />

    <!--- Menu Bookmark Variable --->
    <CFSET menupage = 'managepages' />
    <link rel="stylesheet" href="<CFOUTPUT>#request.baseurl#</CFOUTPUT>_js/upload/jquery.ui.plupload/css/jquery.ui.plupload.css" type="text/css" />

</head>

<!--- Logged in user details --->
<CFSET loginObj = CreateObject("component","birkhillincludes.login") />
<CFSCRIPT>
    loginObj.dsn = dsn;
    loginObj.username = cookie.myadmin_username;
    loginObj.password = cookie.myadmin_password;
    logindetails = loginObj.getLoginDetails();
</CFSCRIPT>

<!--- Get Dropdown details --->
<CFSET imagesObj = CreateObject("component","birkhillincludes.pages") />
<CFSCRIPT>
    imagesObj.dsn = #dsn#;
    toplevelmenus = imagesObj.getTopLevelMenus();
</CFSCRIPT>

<!--- Get page info if not an add --->
<CFIF #url.action# NEQ 'add'>
    <CFSCRIPT>
        imagesObj.image_id = url.image_id;
        imagedetails = imagesObj.getImageDetails();
    </CFSCRIPT>
</CFIF>

<body class="hold-transition sidebar-mini skin-blue">
    <div class="wrapper">
        <CFINCLUDE TEMPLATE="../_includes/_header.cfm" />
        <CFINCLUDE TEMPLATE="../_includes/_menu.cfm" />

        <!--- Content Wrapper. Contains page content --->
        <div class="content-wrapper">
            <section class="content-header">
                <h1>
                    <CFOUTPUT>#UCASE(url.action)#</CFOUTPUT> Image
                </h1>
                <ol class="breadcrumb">
                    <li><a href="<CFOUTPUT>#request.baseurl#</CFOUTPUT>dashboard/dashboard.cfm"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                    <li><a href="<CFOUTPUT>#request.baseurl#</CFOUTPUT>pages/manage_pages.cfm"><i class="fa fa-file-text"></i> Manage Pages</a></li>
                    <CFIF #url.action# EQ 'add'>
                        <li class="active"><i class="fa fa-file-text"></i> Add Image</li>
                    <CFELSEIF #url.action# EQ 'edit'>
                        <li class="active"><i class="fa fa-file-text"></i> Edit Image</li>
                    <CFELSEIF #url.action# EQ 'delete'>
                        <li class="active"><i class="fa fa-file-text"></i> Delete Image</li>
                    </CFIF>
                </ol>
            </section>
            <section class="content">
                <div class="row">
                    <form role="form" method="post" action="<CFOUTPUT>#request.baseurl#</CFOUTPUT>pages/images_process.cfm" id="myform" name="myform" enctype="multipart/form-data">
                        <CFOUTPUT>
                            <input type="hidden" name="page_uuid" value="#url.page_uuid#">
                            <input type="hidden" name="image_id" value="<CFIF #url.action# NEQ 'add'>#image_id#</CFIF>">
                            <input type="hidden" name="action" value="#url.action#">
                        </CFOUTPUT>
                        <div class="col-md-12">
                            <div class="nav-tabs-custom">
                                <div class="tab-content">
                                    <div class="active tab-pane">
                                        <CFIF #url.action# EQ 'add'>
                                            <div class="row">
                                                <div class="col-md-10">
                                                    <div class="form-group">
                                                        <div id="uploader">
                                                            <p>Your browser doesn't have Flash, Silverlight or HTML5 support.</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div><!--- /.row --->
                                        <CFELSEIF #url.action# EQ 'edit' >
                                            <div class="box-header with-border">
                                                <p>Fields marked with an <span class="text-red" style="font-size: 20px;">*</span> must be filled in</p>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="row">
                                                    <div class="form-group">
                                                        <label>Current File</label>
                                                        <img src="<CFOUTPUT>#request.baseurl#../images/uploads/#url.page_uuid#/thumb_#imagedetails.image_url#</CFOUTPUT>" />
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="form-group">
                                                        <label>Upload File</label><span class="text-red" style="font-size: 20px;">*</span>
                                                        <div class="input-group">
                                                            <span class="input-group-btn">
                                                                <span class="btn btn-file btn-info">
                                                                    Browse&hellip; <input type="file" name="image_url" id="uploadfile1">
                                                                </span>
                                                            </span>
                                                            <input type="text" class="form-control" readonly>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        <CFELSEIF #url.action# EQ 'delete' >
                                            <div class="box-header with-border">
                                                <p>Image will be permanently deleted</p>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <img src="<CFOUTPUT>#request.baseurl#../images/uploads/#url.page_uuid#/thumb_#imagedetails.image_url#</CFOUTPUT>" />
                                                    <br />
                                                </div>
                                            </div>
                                        </CFIF>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="box-footer">
                                                    <button type="submit" class="btn <CFIF #url.action# EQ "delete">btn-danger<CFELSE>btn-primary</CFIF>">
                                                        <CFIF #url.action# NEQ 'add'>
                                                            <CFOUTPUT>#UCase(url.action)# Image</CFOUTPUT>
                                                        <CFELSE>
                                                            <CFOUTPUT>#UCase(url.action)# Images</CFOUTPUT>
                                                        </CFIF>
                                                    </button>
                                                </div><!--- /.box-footer --->
                                        </div><!--- /.col --->
                                    </div>
                                </div>
                            </div><!--- /.row --->
                        </div>
                        <div class="hiddens"></div>
                    </form>
                </div><!--- /.row --->
            </section><!--- /.content --->
        </div><!--- /.content-wrapper --->

        <CFINCLUDE TEMPLATE="../_includes/_footer.cfm" />
        <CFINCLUDE TEMPLATE="../_includes/_settings.cfm" />
    </div><!--- ./wrapper --->

    <CFINCLUDE TEMPLATE="../_includes/_head_js.cfm" />
    <script type="text/javascript" src="<CFOUTPUT>#request.baseurl#</CFOUTPUT>_js/upload/plupload.full.min.js"></script>
    <script type="text/javascript" src="<CFOUTPUT>#request.baseurl#</CFOUTPUT>_js/upload/jquery.ui.plupload/jquery.ui.plupload.min.js"></script>
    <script type="text/javascript" language="javascript">

        $(function() {
            window.onunload = refreshParent;
            function refreshParent() {
                window.opener.location.reload();
                localStorage.setItem("imageAdd", true);
            }
            $("#uploader").plupload({
                // General settings
                runtimes : 'html5,flash,silverlight,html4',
                url : '<CFOUTPUT>#request.baseurl#</CFOUTPUT>_cfcs/upload.cfc?method=upload&page=<CFOUTPUT>#url.page_uuid#</CFOUTPUT>',

                // User can upload no more then 20 files in one go (sets multiple_queues to false)
                max_file_count: 20,

                chunk_size: '1mb',

                // Resize images on clientside if we can
                resize : {
                    width : 200,
                    height : 200,
                    quality : 90,
                    crop: true // crop to exact dimensions
                },

                filters : {
                    // Maximum file size
                    max_file_size : '1000mb',
                    // Specify what files to browse for
                    mime_types: [
                        {title : "Image files", extensions : "jpg,gif,png"},
                        {title : "Zip files", extensions : "zip"}
                    ]
                },

                // Rename files by clicking on their titles
                rename: true,

                // Sort files
                sortable: true,

                // Enable ability to drag'n'drop files onto the widget (currently only HTML5 supports that)
                dragdrop: true,

                // Views to activate
                views: {
                    list: true,
                    thumbs: true, // Show thumbs
                    active: 'thumbs'
                },

                // Flash settings
                flash_swf_url : '../../js/Moxie.swf',

                // Silverlight settings
                silverlight_xap_url : '../../js/Moxie.xap'
            });
        });
        $(document).ready(function() {
            /* Upload image files */
            $('.btn-file :file').on('fileselect', function(event, numFiles, label) {

                var input = $(this).parents('.input-group').find(':text'),
                log = numFiles > 1 ? numFiles + ' files selected' : label;

                if( input.length ) {
                    input.val(log);
                } else {
                    if( log ) alert(log);
                }

            });


            /* Upload image files */
            $(document).on('change', '.btn-file :file', function() {
                var input = $(this),
                numFiles = input.get(0).files ? input.get(0).files.length : 1,
                label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
                input.trigger('fileselect', [numFiles, label]);
            });
        });
    </script>
    <cfif #url.action# == 'add'>
        <script type="text/javascript">
            $(function() {
                // Handle the case when form was submitted before uploading has finished
                $('#myform').submit(function(e) {
                        e.preventDefault();
                        // Files in queue upload them first
                        if ($('#uploader').plupload('getFiles').length > 0) {

                            // When all files are uploaded submit form
                            $('#uploader').on('complete', function() {
                                // var files = $('#uploader').plupload('getFiles');
                                // var inputs = '';
                                // $.each(files, function() {
                                //     var name = prompt('Image name for ' + $(this)[0].name);
                                //     inputs = inputs + '<input type="hidden" name="image_name" value="' + name + '" />';
                                // });
                                // $('#myform .hiddens').html(inputs);

                                $(document).find('#myform')[0].submit();
                            });

                            $('#uploader').plupload('start');
                        } else {
                            alert("You must have at least one file in the queue.");
                        }
                    return false; // Keep the form from submitting
                });
            });
        </script>
    </cfif>
</body>
</html>
