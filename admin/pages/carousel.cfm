
<!--- Authentication of user --->
<CFINCLUDE TEMPLATE="../authenticate.cfm">

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
  <CFINCLUDE TEMPLATE="../_includes/_head_meta.cfm" />
  <CFINCLUDE TEMPLATE="../_includes/_head_css.cfm" />
  <style>
    label.error {
      margin-top: 10px;
      color: #DD4B39;
    }
  </style>
  <CFINCLUDE TEMPLATE="../_includes/_head_shim.cfm" />
  <!--- Menu Bookmark Variable --->
  <CFSET menupage = 'managecarousel' />
</head>
<!--- Logged in user details --->
<CFSET loginObj = CreateObject("component","birkhillincludes.login") />
<CFSCRIPT>
  loginObj.dsn = dsn;
  loginObj.username = cookie.myadmin_username;
  loginObj.password = cookie.myadmin_password;
  logindetails = loginObj.getLoginDetails();
</CFSCRIPT>

<!--- Get Dropdown details --->
<CFSET pagesObj = CreateObject("component","birkhillincludes.pages") />
<CFSCRIPT>
  pagesObj.dsn = #dsn#;
  toplevelmenus = pagesObj.getTopLevelMenus();
</CFSCRIPT>

<!--- Get page info if not an add --->
<CFSET carouselObj = CreateObject("component","birkhillincludes.box") />
<CFSCRIPT>
  carouselObj.dsn = #dsn#;
  carouselObj.id = 1;
  getcarousel1 = carouselObj.getcarousel();
  carouselObj.id = 2;
  getcarousel2 = carouselObj.getcarousel();
  carouselObj.id = 3;
  getcarousel3 = carouselObj.getcarousel();
  carouselObj.id = 4;
  getcarousel4 = carouselObj.getcarousel();
</CFSCRIPT>
<body class="hold-transition sidebar-mini skin-blue">
  <div class="wrapper">
    <CFINCLUDE TEMPLATE="../_includes/_header.cfm" />
    <CFINCLUDE TEMPLATE="../_includes/_menu.cfm" />
    <!--- Content Wrapper. Contains page content --->
    <div class="content-wrapper">
      <section class="content-header">
        <h1>
          Edit Boxes
        </h1>
        <ol class="breadcrumb">
          <li><a href="<CFOUTPUT>#request.baseurl#</CFOUTPUT>dashboard/dashboard.cfm"><i class="fa fa-dashboard"></i> Dashboard</a></li>
          <li class="active"><i class="fa fa-file-text"></i> Edit Box</li>
        </ol>
      </section>
      <section class="content">
        <div class="row">
          <form role="form" method="post" action="<CFOUTPUT>#request.baseurl#</CFOUTPUT>pages/carousel_process.cfm" id="myform" name="myform" enctype="multipart/form-data">
            <CFOUTPUT>
              <input type="hidden" name="action" value="edit">
            </CFOUTPUT>
            <div class="col-md-12">
              <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                  <li class="active"><a href="#content1" data-toggle="tab">Box 1</a></li>
                  <li><a href="#content2" data-toggle="tab">Box 2</a></li>
                  <li><a href="#content3" data-toggle="tab">Box 3</a></li>
                  <li><a href="#content4" data-toggle="tab">Box 4</a></li>
                </ul>
                <div class="tab-content">
                  <div class="active tab-pane" id="content1">
                    <div class="row">
                      <div class="form-group">
                        <div class="col-md-6">
                          <label for="header1">Header </label> <span class="text-red" style="font-size: 20px;">*</span>
                          <input type="text" class="form-control" name="header1" id="header1" placeholder="Header" <CFOUTPUT QUERY="getcarousel1">value="#header#"</CFOUTPUT>/>
                        </div>
                      </div>
                    </div>
                    <div class="row hidden">
                      <div class="form-group">
                        <div class="col-md-8">
                          <label for="span1">Span </label>
                          <input type="text" class="form-control" name="span1" id="span1" placeholder="Span" <CFOUTPUT QUERY="getcarousel1">value="#span#"</CFOUTPUT>/>
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="form-group">
                        <div class="col-md-6">
                          <label for="page_name">Current Image </label>
                          <input type="text" class="form-control" name="currentimage1" id="currentimage1" placeholder="Box Image" <CFOUTPUT QUERY="getcarousel1">value="#image#"</CFOUTPUT> />
                        </div>
                        <div class="col-md-6">
                          <label>Upload File</label><span class="text-red" style="font-size: 20px;">*</span>
                          <div class="input-group">
                            <span class="input-group-btn">
                              <span class="btn btn-file btn-info">
                                Browse&hellip; <input type="file" name="uploadfile1" id="uploadfile1" multiple>
                              </span>
                            </span>
                            <input type="text" class="form-control" readonly>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-12">
                        <div class="box-footer">
                          <button type="submit" class="btn btn-primary"><CFOUTPUT>Edit Box</CFOUTPUT></button>
                        </div><!--- /.box-footer --->
                      </div><!--- /.col --->
                    </div><!--- /.row --->
                  </div>
                  <div class="tab-pane" id="content2">
                    <div class="row">
                      <div class="form-group">
                        <div class="col-md-6">
                          <label for="header2">Header </label> <span class="text-red" style="font-size: 20px;">*</span>
                          <input type="text" class="form-control" name="header2" id="header2" placeholder="Header" <CFOUTPUT QUERY="getcarousel2">value="#header#"</CFOUTPUT>/>
                        </div>
                      </div>
                    </div>
                    <div class="row hidden">
                      <div class="form-group">
                        <div class="col-md-8">
                          <label for="span2">Span </label>
                          <input type="text" class="form-control" name="span2" id="span2" placeholder="Span" <CFOUTPUT QUERY="getcarousel2">value="#span#"</CFOUTPUT>/>
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="form-group">
                        <div class="col-md-6">
                          <label for="page_name">Current Image </label>
                          <input type="text" class="form-control" name="currentimage2" id="currentimage2" placeholder="Box Image" <CFOUTPUT QUERY="getcarousel2">value="#image#"</CFOUTPUT> />
                        </div>
                        <div class="col-md-6">
                          <label>Upload File</label><span class="text-red" style="font-size: 20px;">*</span>
                          <div class="input-group">
                            <span class="input-group-btn">
                              <span class="btn btn-file btn-info">
                                Browse&hellip; <input type="file" name="uploadfile2" id="uploadfile2" multiple>
                              </span>
                            </span>
                            <input type="text" class="form-control" readonly>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-12">
                        <div class="box-footer">
                          <button type="submit" class="btn btn-primary"><CFOUTPUT>Edit Box</CFOUTPUT></button>
                        </div><!--- /.box-footer --->
                      </div><!--- /.col --->
                    </div><!--- /.row --->
                  </div>
                  <div class="tab-pane" id="content3">
                    <div class="row">
                      <div class="form-group">
                        <div class="col-md-6">
                          <label for="header3">Header </label> <span class="text-red" style="font-size: 20px;">*</span>
                          <input type="text" class="form-control" name="header3" id="header3" placeholder="Header" <CFOUTPUT QUERY="getcarousel3">value="#header#"</CFOUTPUT>/>
                        </div>
                      </div>
                    </div>
                    <div class="row hidden">
                      <div class="form-group">
                        <div class="col-md-8">
                          <label for="span3">Span </label>
                          <input type="text" class="form-control" name="span3" id="span3" placeholder="Span" <CFOUTPUT QUERY="getcarousel3">value="#span#"</CFOUTPUT>/>
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="form-group">
                        <div class="col-md-6">
                          <label for="page_name">Current Image </label>
                          <input type="text" class="form-control" name="currentimage3" id="currentimage3" placeholder="Box Image" <CFOUTPUT QUERY="getcarousel3">value="#image#"</CFOUTPUT> />
                        </div>
                        <div class="col-md-6">
                          <label>Upload File</label><span class="text-red" style="font-size: 20px;">*</span>
                          <div class="input-group">
                            <span class="input-group-btn">
                              <span class="btn btn-file btn-info">
                                Browse&hellip; <input type="file" name="uploadfile3" id="uploadfile3" multiple>
                              </span>
                            </span>
                            <input type="text" class="form-control" readonly>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-12">
                        <div class="box-footer">
                          <button type="submit" class="btn btn-primary"><CFOUTPUT>Edit Box</CFOUTPUT></button>
                        </div><!--- /.box-footer --->
                      </div><!--- /.col --->
                    </div><!--- /.row --->
                  </div>
                  <div class="tab-pane" id="content4">
                    <div class="row">
                      <div class="form-group">
                        <div class="col-md-6">
                          <label for="header4">Header </label> <span class="text-red" style="font-size: 20px;">*</span>
                          <input type="text" class="form-control" name="header4" id="header4" placeholder="Header" <CFOUTPUT QUERY="getcarousel4">value="#header#"</CFOUTPUT>/>
                        </div>
                      </div>
                    </div>
                    <div class="row hidden">
                      <div class="form-group">
                        <div class="col-md-8">
                          <label for="span4">Span </label>
                          <input type="text" class="form-control" name="span4" id="span4" placeholder="Span" <CFOUTPUT QUERY="getcarousel4">value="#span#"</CFOUTPUT>/>
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="form-group">
                        <div class="col-md-6">
                          <label for="page_name">Current Image </label>
                          <input type="text" class="form-control" name="currentimage4" id="currentimage4" placeholder="Box Image" <CFOUTPUT QUERY="getcarousel4">value="#image#"</CFOUTPUT> />
                        </div>
                        <div class="col-md-6">
                          <label>Upload File</label><span class="text-red" style="font-size: 20px;">*</span>
                          <div class="input-group">
                            <span class="input-group-btn">
                              <span class="btn btn-file btn-info">
                                Browse&hellip; <input type="file" name="uploadfile4" id="uploadfile4" multiple>
                              </span>
                            </span>
                            <input type="text" class="form-control" readonly>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-12">
                        <div class="box-footer">
                          <button type="submit" class="btn btn-primary"><CFOUTPUT>Edit Box</CFOUTPUT></button>
                        </div><!--- /.box-footer --->
                      </div><!--- /.col --->
                    </div><!--- /.row --->
                  </div>
                </div><!--- /.tab-content --->
              </div><!--- /.nav-tabs-custom --->
            </form>
          </div><!--- /.row --->
        </section><!--- /.content --->
      </div><!--- /.content-wrapper --->
    <CFINCLUDE TEMPLATE="../_includes/_footer.cfm" />
    <CFINCLUDE TEMPLATE="../_includes/_settings.cfm" />
  </div><!--- ./wrapper --->
  <CFINCLUDE TEMPLATE="../_includes/_head_js.cfm" />
  <script type="text/javascript" language="javascript">
    $(document).ready(function() {
      /* Form validation */
      $("#myform").validate({
        rules: {
          box_heading1: "required",
          box_heading2: "required",
          box_heading3: "required",
          box_content1: "required",
          box_content2: "required",
          box_content3: "required"
        },
        messages: {
          box_heading1: "Please enter Box Heading 1",
          box_heading2: "Please enter Box Heading 2",
          box_heading3: "Please enter Box Heading 3",
          box_content1: "Please enter Page Content 1",
          box_content2: "Please enter Page Content 2",
          box_content3: "Please enter Page Content 3"
        }
      });

      /* Upload image files */
      $('.btn-file :file').on('fileselect', function(event, numFiles, label) {

        var input = $(this).parents('.input-group').find(':text'),
        log = numFiles > 1 ? numFiles + ' files selected' : label;

        if( input.length ) {
          input.val(log);
        } else {
          if( log ) alert(log);
        }

      });

      /* Upload image files */
      $(document).on('change', '.btn-file :file', function() {
        var input = $(this),
        numFiles = input.get(0).files ? input.get(0).files.length : 1,
        label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
        input.trigger('fileselect', [numFiles, label]);
      });
    });
  </script>
  <CFINCLUDE TEMPLATE="../_includes/_toast.cfm" />
</body>
</html>
