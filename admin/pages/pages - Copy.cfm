<!--- Authentication of user --->
<CFINCLUDE TEMPLATE="../authenticate.cfm">

  <!DOCTYPE html>
    <html>
     <head>
       <CFINCLUDE TEMPLATE="../_includes/_head_meta.cfm" />
       <CFINCLUDE TEMPLATE="../_includes/_head_css.cfm" />
       <style>
        label.error {
         margin-top: 10px;
         color: #DD4B39;
       }
     </style>
     <CFINCLUDE TEMPLATE="../_includes/_head_shim.cfm" />
     <!--- Menu Bookmark Variable --->
     <CFSET menupage = 'managepages' />
   </head>

   <!--- Logged in user details --->
   <CFSET loginObj = CreateObject("component","birkhillincludes.login") />
   <CFSCRIPT>
    loginObj.dsn = dsn;
    loginObj.username = cookie.myadmin_username;
    loginObj.password = cookie.myadmin_password;
    logindetails = loginObj.getLoginDetails();
  </CFSCRIPT>
  <!--- Get Dropdown details --->
  <CFSET pagesObj = CreateObject("component","birkhillincludes.pages") />
  <CFSCRIPT>
   pagesObj.dsn = #dsn#;
   toplevelmenus = pagesObj.getTopLevelMenus();
 </CFSCRIPT>
 <!--- Get page info if not an add --->
 <cfif #url.action# neq 'add'>
  <cfscript>
    pagesobj.page_uuid = url.page_uuid;
    pagedetails = pagesobj.getpagedetails();
    images = pagesobj.getimages();
  </cfscript>
</cfif>

<body class="hold-transition sidebar-mini skin-blue">
	<div class="wrapper">
   <CFINCLUDE TEMPLATE="../_includes/_header.cfm" />
   <CFINCLUDE TEMPLATE="../_includes/_menu.cfm" />

   <!--- Content Wrapper. Contains page content --->
   <div class="content-wrapper">
     <section class="content-header">
       <h1>
         <CFOUTPUT>#UCASE(url.action)#</CFOUTPUT> Page
       </h1>
       <ol class="breadcrumb">
         <li><a href="<CFOUTPUT>#request.baseurl#</CFOUTPUT>dashboard/dashboard.cfm"><i class="fa fa-dashboard"></i> Dashboard</a></li>
         <li><a href="<CFOUTPUT>#request.baseurl#</CFOUTPUT>pages/manage_pages.cfm"><i class="fa fa-file-text"></i> Manage Pages</a></li>
         <CFIF #url.action# EQ 'add'>
          <li class="active"><i class="fa fa-file-text"></i> Add Page</li>
          <CFELSEIF #url.action# EQ 'edit'>
            <li class="active"><i class="fa fa-file-text"></i> Edit Page</li>
            <CFELSEIF #url.action# EQ 'delete'>
              <li class="active"><i class="fa fa-file-text"></i> Delete Page</li>
            </CFIF>
          </ol>
        </section>
        <section class="content">
          <div class="row">
            <form role="form" method="post" action="<CFOUTPUT>#request.baseurl#</CFOUTPUT>pages/pages_process.cfm" id="myform" name="myform" enctype='multipart/form-data'>
              <CFOUTPUT>
                <input type="hidden" name="action" value="#url.action#">
                <input type="hidden" name="page_UUID" value="<CFIF #url.action# NEQ 'add'>#page_UUID#</CFIF>">
              </CFOUTPUT>
              <div class="col-md-12">
               <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                  <li class="active"><a href="#page_details" data-toggle="tab">Page Details</a></li>
                  <li><a href="#top" data-toggle="tab">Top picture</a></li>

                  <li><a href="#content1" data-toggle="tab">Content 1</a></li>
                  <li><a href="#content2" data-toggle="tab">Content 2</a></li>
                  <li><a href="#content3" data-toggle="tab">Content 3</a></li>
                  <li><a href="#gallery" id="gallery-tab" data-toggle="tab">Gallery</a></li>
                </ul>
                <div class="tab-content">
                 <div class="active tab-pane" id="page_details">
                   <div class="box-header with-border">
                     <p>Fields marked with an <span class="text-red" style="font-size: 20px;">*</span> must be filled in</p>
                   </div>
                   <div class="row">
                    <div class="col-md-3">
                     <div class="form-group">
                      <label for="page_reference">Page Reference</label> <span class="text-red" style="font-size: 20px;">*</span>
                      <input type="text" class="form-control" name="page_reference" id="page_reference" placeholder="Page Reference" <CFIF #url.action# NEQ "add"><CFOUTPUT QUERY="pagedetails">value="#page_reference#"</CFOUTPUT></CFIF> />
                    </div>
                    <div class="form-group">
                      <label for="page_name">Page Filename</label> <span class="text-red" style="font-size: 20px;">*</span>
                      <input type="text" class="form-control" name="page_name" id="page_name" placeholder="Page Name" <CFIF #url.action# NEQ "add"><CFOUTPUT QUERY="pagedetails">value="#page_name#"</CFOUTPUT></CFIF> />
                    </div>
                    <div class="form-group">
                      <label>Template</label> <span class="text-red" style="font-size: 20px;">*</span>
                      <select class="form-control select2" name="page_template" id="page_template" style="width: 100%;">
                        <option value="_template1.cfm" <CFIF action NEQ 'add' AND pagedetails.page_template EQ '_template1.cfm'>selected</CFIF>>Template 1</option>
                        <option value="_template2.cfm" <CFIF action NEQ 'add' AND pagedetails.page_template EQ '_template2.cfm'>selected</CFIF>>Template 2</option>
                        <option value="_template3.cfm" <CFIF action NEQ 'add' AND pagedetails.page_template EQ '_template3.cfm'>selected</CFIF>>Template 3</option>
                      </select>
                    </div>
                  </div>
                  <div class="col-md-3">
                   <div class="form-group">
                    <label for="page_title">Page Title</label> <span style="font-size: 20px;">&nbsp;</span>
                    <input type="text" class="form-control" name="page_title" id="page_title" placeholder="Page Title" <CFIF #url.action# NEQ "add"><CFOUTPUT QUERY="pagedetails">value="#page_title#"</CFOUTPUT></CFIF> />
                  </div>
                  <div class="form-group">
                    <label>Top Level Menu</label> <span style="font-size: 20px;">&nbsp;</span>
                    <select class="form-control select2-menu" name="menu_UUID" id="menu_UUID" style="width: 100%;">
                      <option value="" <CFIF action EQ 'add'>selected</CFIF>>&nbsp;</option>
                      <CFOUTPUT QUERY="toplevelmenus">
                        <option value="#menu_UUID#" <CFIF action NEQ 'add'><CFIF pagedetails.menu_UUID EQ menu_UUID>selected</CFIF></CFIF>>#menu_name#</option>
                      </CFOUTPUT>
                    </select>
                  </div>
                </div>
                <div class="col-md-6">
                 <div class="form-group">
                  <label for="page_description">Meta Description</label> <span style="font-size: 20px;">&nbsp;</span>
                  <textarea class="form-control" name="page_description" id="page_description"><CFIF #url.action# NEQ "add"><CFOUTPUT QUERY="pagedetails">#page_description#</CFOUTPUT></CFIF></textarea>
                </div>
                <div class="form-group">
                  <label for="page_keywords">Meta Keywords</label> <span style="font-size: 20px;">&nbsp;</span>
                  <textarea class="form-control" name="page_keywords" id="page_keywords"><CFIF #url.action# NEQ "add"><CFOUTPUT QUERY="pagedetails">#page_keywords#</CFOUTPUT></CFIF></textarea>
                </div>
              </div>
            </div><!--- /.row --->
            <div class="row">
             <div class="col-md-12">
              <div class="box-footer">
                <button type="submit" class="btn <CFIF #url.action# EQ "delete">btn-danger<CFELSE>btn-primary</CFIF>"><CFOUTPUT>#UCase(url.action)# Page</CFOUTPUT></button>
              </div><!--- /.box-footer --->
            </div><!--- /.col --->
          </div><!--- /.row --->
        </div><!--- /.tab-pane --->
        <div class="tab-pane" id="content1">
         <div class="row">
          <div class="col-md-12">
            <div class="form-group">
              <label for="page_content1">Page Content 1</label> <span class="text-red" style="font-size: 20px;">*</span>
              <textarea name="page_content1" id="page_content1"><CFIF #url.action# NEQ "add"><CFOUTPUT QUERY="pagedetails">#page_content1#</CFOUTPUT></CFIF></textarea>
            </div>
          </div><!--- /.col --->
        </div>
        <div class="row">
         <div class="col-md-12">
          <div class="box-footer">
            <button type="submit" class="btn <CFIF #url.action# EQ "delete">btn-danger<CFELSE>btn-primary</CFIF>"><CFOUTPUT>#UCase(url.action)# Page</CFOUTPUT></button>
          </div><!--- /.box-footer --->
        </div><!--- /.col --->
      </div><!--- /.row --->
    </div>
    <div class="tab-pane" id="top">
      <div class="row">
        <div class="col-md-12">
          <div class="form-group">
            <div class="col-md-3 pull-left">
              <label for="span1">Span </label>
              <input type="text" class="form-control" name="span1" id="span1" placeholder="Span" <CFOUTPUT QUERY="pagedetails">value="#banner_text#"</CFOUTPUT> />
            </div>
          </div>
        </div>
      </div>
      <div class="col-md-12">
        <div class="row">
          <div class="form-group">
            <div class="col-md-6">
              <label for="page_name">Current Image </label>
              <input type="text" class="form-control" name="currentimage1" id="currentimage1" placeholder="Box Image" <CFOUTPUT QUERY="pagedetails">value="#banner_image#"</CFOUTPUT> />
            </div>
            <div class="col-md-6">
              <label>Upload File</label>
              <div class="input-group">
                <span class="input-group-btn">
                  <span class="btn btn-file btn-info">
                    Browse&hellip; <input type="file" name="uploadfile1" id="uploadfile1" multiple>
                  </span>
                </span>
                <input type="text" class="form-control" readonly>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12">
          <div class="box-footer">
            <button type="submit" class="btn <CFIF #url.action# EQ "delete">btn-danger<CFELSE>btn-primary</CFIF>"><CFOUTPUT>#UCase(url.action)# Page</CFOUTPUT></button>
          </div><!--- /.box-footer --->
        </div><!--- /.col --->
      </div><!--- /.row --->
    </div>
    <div class="tab-pane" id="content2">
     <div class="row">
      <div class="col-md-12">
        <div class="form-group">
          <label for="page_content2">Page Content 2</label> <span class="text-red" style="font-size: 20px;">*</span>
          <textarea name="page_content2" id="page_content2"><CFIF #url.action# NEQ "add"><CFOUTPUT QUERY="pagedetails">#page_content2#</CFOUTPUT></CFIF></textarea>
        </div>
      </div><!--- /.col --->
    </div>
    <div class="row">
     <div class="col-md-12">
      <div class="box-footer">
        <button type="submit" class="btn <CFIF #url.action# EQ "delete">btn-danger<CFELSE>btn-primary</CFIF>"><CFOUTPUT>#UCase(url.action)# Page</CFOUTPUT></button>
      </div><!--- /.box-footer --->
    </div><!--- /.col --->
  </div><!--- /.row --->
</div>
<div class="tab-pane" id="content3">
 <div class="row">
  <div class="col-md-12">
    <div class="form-group">
      <label for="page_content3">Page Content 3</label> <span class="text-red" style="font-size: 20px;">*</span>
      <textarea name="page_content3" id="page_content3"><CFIF #url.action# NEQ "add"><CFOUTPUT QUERY="pagedetails">#page_content3#</CFOUTPUT></CFIF></textarea>
    </div>
  </div><!--- /.col --->
</div>
<div class="row">
 <div class="col-md-12">
  <div class="box-footer">
    <button type="submit" class="btn <CFIF #url.action# EQ "delete">btn-danger<CFELSE>btn-primary</CFIF>"><CFOUTPUT>#UCase(url.action)# Page</CFOUTPUT></button>
  </div><!--- /.box-footer --->
</div><!--- /.col --->
</div><!--- /.row --->
</div>
<div class="tab-pane" id="gallery">
  <section class="content-header">
    <h1>
      <cfoutput>
        Images in gallery
        <span class="label label-primary label-sm">
          <cfif #url.action# neq 'add'>
            #images.RECORDCOUNT#
            <cfelse>
              0
            </cfif>
          </span>
          <a class="btn btn-success btn-sm" href="##" onClick="info();window.open('<cfoutput>#request.baseurl#</cfoutput>pages/images.cfm?page_uuid=#url.page_UUID#&action=add', '_blank')">
            <strong>ADD Images</strong>
          </a>
        </cfoutput>
      </h1>
    </section>
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="box box-primary">
            <div class="box-body">
              <table id="myTable" class="table table-bordered table-hover">
                <thead>
                  <tr>
                    <th>Image Url</th>
                    <th>Thumbnail</th>
                    <th>Actions</th>
                  </tr>
                </thead>
                <tbody>
                  <cfif #url.action# neq 'add'>
                    <cfoutput query="images">
                      <tr>
                        <td>#image_url#</td>
                        <td><img src="#request.baseurl#../images/uploads/#url.page_uuid#/thumb_#image_url#" /></td>
                        <td>
                          <a class="btn btn-xs btn-primary" target="_blank" onClick="info()" href="#request.baseurl#pages/images.cfm?page_uuid=#url.page_uuid#&image_id=#image_id#&action=edit">Edit</a>
                          &nbsp;
                          <a class="btn btn-xs btn-danger" target="_blank" onClick="info()" href="#request.baseurl#pages/images.cfm?page_uuid=#url.page_uuid#&image_id=#image_id#&action=delete">Delete</a>
                        </td>
                      </tr>
                    </cfoutput>
                  </cfif>
                </tbody>
              </table>
            </div><!--- /.box-body --->
          </div><!--- /.box --->
        </div><!--- /.col --->
      </div><!--- /.row --->
    </section>
  </div>
</div><!--- /.tab-content --->
</div><!--- /.nav-tabs-custom --->
</form>
</div><!--- /.row --->
</section><!--- /.content --->
</div><!--- /.content-wrapper --->
<CFINCLUDE TEMPLATE="../_includes/_footer.cfm" />
<CFINCLUDE TEMPLATE="../_includes/_settings.cfm" />
</div><!--- ./wrapper --->
<CFINCLUDE TEMPLATE="../_includes/_head_js.cfm" />
<script type="text/javascript" language="javascript">
  function info() {
    $('body').append('<div style="display:block;z-index:1031;width:100%;height:100%;background:rgba(0,0,0,0.5);position:fixed;top:0;"><div style="z-index:1030;position:absolute;left:0;right:0;top:0;bottom:0;margin:auto;width:200px;height:100px"><h1 style="color:white;text-align:center;">Please Wait</h1></div></div>');
  }
  $(document).ready(function() {
    if(localStorage.getItem("imageAdd")) {
      $(document).find('#gallery-tab').tab('show');
      $(document).find('.tab-content > div.active').removeClass('active');
      $(document).find('.tab-content > div#gallery').addClass('active');
      localStorage.removeItem("imageAdd");
    }
    /* Form validation */
    $("#myform").validate({
      rules: {
       page_reference: "required",
       page_name: "required",
       page_content1: "required",
       page_content2: "required",
       page_content3: "required"
     },
     messages: {
       page_reference: "Please enter a Page Reference",
       page_name: "Please enter a Page Filename",
       page_content1: "Please enter Page Content 1",
       page_content2: "Please enter Page Content 2",
       page_content3: "Please enter Page Content 3"
     }
   });

    /* Select2 initilization */
    $(".select2").select2( {
      minimumResultsForSearch: Infinity
    });

    $(".select2-menu").select2( {
      minimumResultsForSearch: Infinity,
      placeholder: 'Select a menu...'
    });

    $('#myTable').DataTable({
      "bLengthChange": false,
      "info": false
    });

    /* CKEditor initilizations */
    CKEDITOR.replace('page_content1');
    CKEDITOR.replace('page_content2');
    CKEDITOR.replace('page_content3');

    /* Upload image files */
    $('.btn-file :file').on('fileselect', function(event, numFiles, label) {

      var input = $(this).parents('.input-group').find(':text'),
      log = numFiles > 1 ? numFiles + ' files selected' : label;

      if( input.length ) {
        input.val(log);
      } else {
        if( log ) alert(log);
      }

    });

    /* Upload image files */
    $(document).on('change', '.btn-file :file', function() {
      var input = $(this),
      numFiles = input.get(0).files ? input.get(0).files.length : 1,
      label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
      input.trigger('fileselect', [numFiles, label]);
    });
  });
</script>
<!--- Disabled fields if deleting content --->
<CFIF #url.action# EQ "delete">
  <script type="text/javascript" language="javascript">
   $(document).ready(function() {
    $('#page_reference').prop('disabled', true);
    $('#page_title').prop('disabled', true);
    $('#page_name').prop('disabled', true);
    $('#menu_UUID').prop('disabled', true);
    $('#page_description').prop('disabled', true);
    $('#page_keywords').prop('disabled', true);
    $('#page_content1').prop('disabled', true);
    $('#page_content2').prop('disabled', true);
    $('#page_content3').prop('disabled', true);
    $('.select2-selection--single').css('cursor', 'not-allowed');


  });

 </script>
</CFIF>

</body>

</html>
