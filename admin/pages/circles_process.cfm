<!--- Authentication of user --->
<CFINCLUDE TEMPLATE="../authenticate.cfm">
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
  <CFINCLUDE TEMPLATE="../_includes/_head_meta.cfm" />
  <CFINCLUDE TEMPLATE="../_includes/_head_css.cfm" />
  <style>
    label.error {
      margin-top: 10px;
      color: #DD4B39;
    }
  </style>
  <CFINCLUDE TEMPLATE="../_includes/_head_shim.cfm" />
  <!--- Menu Bookmark Variable --->
  <CFSET menupage = 'manageboxes' />
</head>
<!--- Logged in user details --->
<CFSET loginObj = CreateObject("component","birkhillincludes.login") />
<CFSCRIPT>
  loginObj.dsn = dsn;
  loginObj.username = cookie.myadmin_username;
  loginObj.password = cookie.myadmin_password;
  logindetails = loginObj.getLoginDetails();
</CFSCRIPT>
<body class="hold-transition sidebar-mini skin-blue">
  <div class="wrapper">
    <CFINCLUDE TEMPLATE="../_includes/_header.cfm" />
    <CFINCLUDE TEMPLATE="../_includes/_menu.cfm" />
    <!--- Content Wrapper. Contains page content --->
    <div class="content-wrapper">
      <section class="content-header">
        <h1>
          Edit Boxes
        </h1>
        <ol class="breadcrumb">
          <li><a href="<CFOUTPUT>#request.baseurl#</CFOUTPUT>dashboard/dashboard.cfm"><i class="fa fa-dashboard"></i> Dashboard</a></li>
          <li class="active"><i class="fa fa-file-text"></i> Edit Box</li>
        </ol>
      </section>
      <section class="content">
        <div class="row">
          <!--- Send form details to the database --->
          <CFSET circlesObj = CreateObject("component","birkhillincludes.box") />
          <CFSCRIPT>
            circlesObj.dsn = dsn;

            circlesObj.line1 = line1;
            circlesObj.line2 = line2;
            circlesObj.line3 = line3;
            circlesObj.line4 = line4;
            circlesObj.line5 = line5;
            circlesObj.line6 = line6;

            circlesObj.uploadfile1 = icon1;
            circlesObj.uploadfile2 = icon2;
            circlesObj.uploadfile3 = icon3;
            circlesObj.uploadfile4 = icon4;
            circlesObj.uploadfile5 = icon5;
            circlesObj.uploadfile6 = icon6;

            circlesObj.currentimage1 = icon1Old;
            circlesObj.currentimage2 = icon2Old;
            circlesObj.currentimage3 = icon3Old;
            circlesObj.currentimage4 = icon4Old;
            circlesObj.currentimage5 = icon5Old;
            circlesObj.currentimage6 = icon6Old;

            myresult = circlesObj.circles_process();
          </CFSCRIPT>
          <CFOUTPUT>
            <CFSET locationurl = "./circles.cfm?action=edit&toastcode=" & "#myresult.toastCode#" & "&toasttitle=" & "#myresult.toastTitle#" & "&toastmessage=" & "#myresult.toastMessage#" />
          </CFOUTPUT>
          <CFLOCATION URL="#locationurl#" />
        </div><!--- /.row --->
      </section><!--- /.content --->
    </div><!--- /.content-wrapper --->
    <CFINCLUDE TEMPLATE="../_includes/_footer.cfm" />
    <CFINCLUDE TEMPLATE="../_includes/_settings.cfm" />
  </div><!--- ./wrapper --->
  <CFINCLUDE TEMPLATE="../_includes/_head_js.cfm" />
</body>
</html>
