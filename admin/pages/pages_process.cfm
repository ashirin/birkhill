
<!--- Authentication of user --->
<CFINCLUDE TEMPLATE="../authenticate.cfm">

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

  	<head>

		<CFINCLUDE TEMPLATE="../_includes/_head_meta.cfm" />

        <CFINCLUDE TEMPLATE="../_includes/_head_css.cfm" />

        <CFINCLUDE TEMPLATE="../_includes/_head_shim.cfm" />

        <!--- Menu Bookmark Variable --->
        <CFSET menupage = 'managepages' />

	</head>


     <!--- Logged in user details --->
    <CFSET loginObj = CreateObject("component","birkhillincludes.login") />
	<CFSCRIPT>
        loginObj.dsn = dsn;
		loginObj.username = cookie.myadmin_username;
		loginObj.password = cookie.myadmin_password;
        logindetails = loginObj.getLoginDetails();
    </CFSCRIPT>

    <!--- Send form details to the database --->
    <CFSET pagesObj = CreateObject("component","birkhillincludes.pages") />
	<CFSCRIPT>
        pagesObj.dsn = dsn;
        pagesObj.action = form.action;
        pagesObj.page_UUID = form.page_UUID;
        pagesObj.logged_user = cookie.myadmin_username;
		if (form.action != 'delete') {
            pagesObj.page_reference = form.page_reference;
            pagesObj.page_title = form.page_title;
            pagesObj.page_name = form.page_name;
            pagesObj.menu_UUID = form.menu_UUID;
            pagesObj.page_template = form.page_template;
            pagesObj.page_content1 = form.page_content1;
            pagesObj.page_content2 = form.page_content2;
            pagesObj.page_content3 = form.page_content3;
            pagesObj.page_description = form.page_description;
            pagesObj.page_keywords = form.page_keywords;
            pagesObj.span = form.span1;
            pagesObj.span2 = form.span2;
            pagesObj.currentimage = form.currentimage1;
            pagesObj.uploadfile = form.uploadfile1;
            pagesObj.currentimage2 = form.currentimage2;
            pagesObj.uploadfile2 = form.uploadfile2;
             if(structKeyExists(form,'ribbon')) {
                pagesObj.ribbon = 1;
            }
        }
        myresult = pagesObj.pageProcess();
    </CFSCRIPT>


	<body class="hold-transition sidebar-mini skin-blue">

    	<div class="wrapper">

        	<CFINCLUDE TEMPLATE="../_includes/_header.cfm" />

            <CFINCLUDE TEMPLATE="../_includes/_menu.cfm" />

            <!--- Content Wrapper. Contains page content --->
      		<div class="content-wrapper">

				<CFSET locationurl = "./manage_pages.cfm?toastcode=" & "#myresult.toastCode#" & "&toasttitle=" & "#myresult.toastTitle#" & "&toastmessage=" & "#myresult.toastMessage#" />

                <CFLOCATION URL="#locationurl#" />

            </div><!--- /.content-wrapper --->


            <CFINCLUDE TEMPLATE="../_includes/_footer.cfm" />

            <CFINCLUDE TEMPLATE="../_includes/_settings.cfm" />

    	</div><!--- ./wrapper --->

        <CFINCLUDE TEMPLATE="../_includes/_head_js.cfm" />

    </body>

</html>
