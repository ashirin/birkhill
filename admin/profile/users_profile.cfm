
<!--- Authentication of user --->
<CFINCLUDE TEMPLATE="../authenticate.cfm">

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

  	<head>

		<CFINCLUDE TEMPLATE="../_includes/_head_meta.cfm" />

        <CFINCLUDE TEMPLATE="../_includes/_head_css.cfm" />

        <style>
			.nav-tabs-custom > .nav-tabs > li.active {
				border-top-color: #00C0EF;
			}

			label.error {
				margin-top: 10px;
				color: #DD4B39;
			}
		</style>

        <CFINCLUDE TEMPLATE="../_includes/_head_shim.cfm" />

        <!--- Menu Bookmark Variable --->
        <CFSET menupage = 'x' />

	</head>


    <!--- Logged in user details --->
    <CFSET loginObj = CreateObject("component","birkhillincludes.login") />
	<CFSCRIPT>
        loginObj.dsn = dsn;
		loginObj.username = cookie.myadmin_username;
		loginObj.password = cookie.myadmin_password;
        logindetails = loginObj.getLoginDetails();
    </CFSCRIPT>

	<!--- Get user info --->
    <CFSET usersObj = CreateObject("component","birkhillincludes.users") />
    <CFSCRIPT>
		usersObj.dsn = dsn;
		getlocations = usersObj.getLocations();
		usersObj.user_details_UUID = logindetails.user_details_UUID;
        getuser = usersObj.getUserDetails();
	</CFSCRIPT>


    <body class="hold-transition sidebar-mini skin-blue">

    	<div class="wrapper">

        	<CFINCLUDE TEMPLATE="../_includes/_header.cfm" />

            <CFINCLUDE TEMPLATE="../_includes/_menu.cfm" />


            <!--- Content Wrapper. Contains page content --->
      		<div class="content-wrapper">

            	<section class="content-header">
                  	<h1>
                    	User Profile
                  	</h1>
                    <ol class="breadcrumb">
                    	<li><a href="<CFOUTPUT>#request.baseurl#</CFOUTPUT>dashboard/dashboard.cfm"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                        <li class="active"><i class="fa fa-user"></i> User Profile</li>
                	</ol>
                </section><!--- /.content-header --->

                <!--- Main content --->
                <section class="content">

          			<div class="row">

            			<div class="col-md-3">

              				<!--- Profile Image --->
              				<div class="box box-info">

                				<div class="box-body">

                                	<!--- Insert logged in user avatar or default to generic avatar --->
									<CFIF #logindetails.uploadfile1# NEQ ''>
                                        <CFOUTPUT QUERY = 'logindetails'>
                                            <img src="<CFOUTPUT>#request.baseurl#</CFOUTPUT>_img/avatars/#user_details_UUID#/#uploadfile1#" class="profile-user-img img-responsive img-circle" alt="User Image" />
                                        </CFOUTPUT>
                                    <CFELSE>
                                        <img src="<CFOUTPUT>#request.baseurl#</CFOUTPUT>_img/generic_avatar.png" class="profile-user-img img-responsive img-circle" alt="User Image" />
                                    </CFIF>

                  					<h3 class="profile-username text-center"><CFOUTPUT QUERY="logindetails"><CFIF #IsDefined('logindetails.user_firstname')#>#user_firstname# #user_lastname#<CFELSE>#email_name#</CFIF></CFOUTPUT></h3>

                  					<p class="text-muted text-center"><CFOUTPUT QUERY="logindetails">#user_role#</CFOUTPUT></p>

                  					<ul class="list-group list-group-unbordered">
                    					<li class="list-group-item">
                      						<b>Last Login</b> <span class="label label-info pull-right"><CFOUTPUT>#DateFormat(logindetails.last_login, 'dd/mm/yyyy')#</CFOUTPUT></span>
                    					</li>
                                        <li class="list-group-item">
                      						<b>Last Modified</b> <span class="label label-info pull-right"><CFOUTPUT>#DateFormat(logindetails.last_modified, 'dd/mm/yyyy')#</CFOUTPUT></span>
                    					</li>
                  					</ul>

                				</div><!--- /.box-body --->

              				</div><!--- /.box --->

            			</div><!--- /.col --->

            			<div class="col-md-9">

							<!--- First tab to update details and possibiliteis for other user data store in tabs --->
              				<div class="nav-tabs-custom">

                				<ul class="nav nav-tabs">
                  					<li class="active"><a href="#account" data-toggle="tab">Account</a></li>
                				</ul>

                				<div class="tab-content">

                  					<div class="active tab-pane" id="account">

                                        <form role="form" method="post" action="<CFOUTPUT>#request.baseurl#</CFOUTPUT>profile/profile_process.cfm" name="myform" id="myform" enctype="multipart/form-data">

                                            <!--- Only used to circumvent the autocomplete feature of browsers --->
                                            <input style="display: none;">
                                            <input type="password" style="display: none;">
                                            <!--- Browsers are stupid --->

                                            <CFOUTPUT>
                                                <input type="hidden" name="user_UUID" value="#logindetails.user_UUID#">
                                                <input type="hidden" name="user_details_UUID" value="#logindetails.user_details_UUID#">
                                            </CFOUTPUT>

                                            <div class="row">

                                                <!--- column width --->
                                                <div class="col-md-8">

                                                	<p style="font-size: 18px;"><strong>Account:</strong> <CFOUTPUT>#getuser.user_email#</CFOUTPUT></p>

                                                	<div class="row">

                                                        <div class="col-md-6">

                                                            <div class="form-group">
                                                                <label for="user_firstname">First Name</label> <span style="font-size: 20px;">&nbsp;</span>
                                                                <input type="text" class="form-control" name="user_firstname" id="user_firstname" placeholder="First Name" <CFOUTPUT QUERY="getuser">value="#user_firstname#"</CFOUTPUT> />
                                                            </div>

                                                            <div class="form-group">
                                                                <label for="user_phone">Phone</label> <span style="font-size: 20px;">&nbsp;</span>
                                                                <input type="text" class="form-control" name="user_phone" id="user_phone" placeholder="Phone" <CFOUTPUT QUERY="getuser">value="#user_phone#"</CFOUTPUT> />
                                                            </div>

                                                            <div class="form-group">
                                                                <label for="user_organisation">Organisation</label> <span style="font-size: 20px;">&nbsp;</span>
                                                                <input type="text" class="form-control" name="user_organisation" id="user_organisation" placeholder="Organisation"<CFOUTPUT QUERY="getuser">value="#user_organisation#"</CFOUTPUT> />
                                                            </div>

                                                            <div class="form-group">
                                                                <label>Location</label> <span style="font-size: 20px;">&nbsp;</span>
                                                                <select class="form-control select2-location" name="user_location" id="user_location" style="width: 100%;">
                                                                    <option value="">&nbsp;</option>
                                                                    <CFOUTPUT QUERY="getlocations">
                                                                        <option value="#location_UUID#" <CFIF getuser.location_UUID EQ location_UUID>selected</CFIF>>#location#</option>
                                                                    </CFOUTPUT>
                                                                </select>
                                                            </div>

                                                        </div>

                                                        <div class="col-md-6">

                                                           <div class="form-group">
                                                                <label for="user_lastname">Last Name</label> <span style="font-size: 20px;">&nbsp;</span>
                                                                <input type="text" class="form-control" name="user_lastname" id="user_lastname" placeholder="Last Name" <CFOUTPUT QUERY="getuser">value="#user_lastname#"</CFOUTPUT> />
                                                            </div>

                                                            <div class="form-group">
                                                                <label for="new_password">New Password</label> <span style="font-size: 20px;">&nbsp;</span>
                                                                <input type="password" class="form-control" name="new_password" id="new_password"placeholder="New Password" />
                                                            </div>

                                                            <div class="form-group">
                                                                <label for="repeat_new_password">Confirm Password</label> <span style="font-size: 20px;">&nbsp;</span>
                                                                <input type="password" class="form-control" name="repeat_new_password" id="repeat_new_password" placeholder="New Password" />
                                                            </div>

                                                        </div>

                                                    </div>

                                                </div>

                                                <!--- column width --->
                                                <div class="col-md-4">

                                                	<p style="font-size: 18px;">&nbsp;</p>

                                                	<div class="row">

                                                        <div class="col-md-12">

                                                            <label>Upload Avatar</label> <span style="font-size: 20px;">&nbsp;</span>
                                                            <div class="input-group">
                                                                <span class="input-group-btn">
                                                                    <span class="btn btn-info btn-file">
                                                                        Browse&hellip; <input type="file" name="uploadfile1" multiple>
                                                                    </span>
                                                                </span>
                                                                <input type="text" class="form-control" readonly>
                                                            </div>

                                                        </div>

                                                    </div>

                                                    <br />

                                                    <CFIF #getuser.uploadfile1# NEQ ''>

                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <div>
                                                                    <CFOUTPUT>
                                                                        <img src="../_img/avatars/#logindetails.user_details_UUID#/#getuser.uploadfile1#" style="border: 1px solid ##ccc; display: block; margin: auto;" />
                                                                    </CFOUTPUT>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <br />

                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <div>
                                                                    <div class="form-group">
                                                                        <label for="currentimage1">Current Image</label> (delete to remove)
                                                                        <input type="text" class="form-control" id="currentimage1" name="currentimage1" <CFOUTPUT>value="#getuser.uploadfile1#"</CFOUTPUT> />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    <CFELSE>

                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <div>
                                                                    <img src="../_img/generic_avatar.png" style="border: 1px solid #ccc; display: block; margin: auto;" />
                                                                    <input type="hidden" name="currentimage1" id="currentimage1" />
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </CFIF>

                                            	</div><!--- /.col --->

											</div><!--- /.row --->

                                        	<div class="row">

                                            	<div class="col-md-12">

                                                    <div class="box-footer">
                                                        <button type="submit" class="btn btn-info">UPDATE Profile</button>
                                                    </div><!--- /.box-footer --->

                                                </div><!--- /.col --->

                                            </div><!--- /.row --->

                                    	</form>

                            		</div><!--- /.tab-pane --->

                            	</div><!--- /.tab-content --->

                        	</div><!--- /.nav-tabs-custom --->

            			</div><!--- /.col --->

          			</div><!--- /.row --->

        		</section><!--- /.content --->

      		</div><!--- /.content-wrapper --->


			<CFINCLUDE TEMPLATE="../_includes/_footer.cfm" />

            <CFINCLUDE TEMPLATE="../_includes/_settings.cfm" />

    	</div><!--- ./wrapper --->

        <CFINCLUDE TEMPLATE="../_includes/_head_js.cfm" />

        <script type="text/javascript" language="javascript">
			$(document).ready(function() {
				/* Validation rules */
				$("#myform").validate({
					rules: {
						repeat_new_password: {
							equalTo: "#new_password"
						}
					},
					messages: {
						repeat_new_password: {
							equalTo: "Your Passwords do not match"
						}
					}
				});

				/* Initializing select2 dropdown */
				$(".select2-location").select2( {
					placeholder: "Select a location..."
				});

				/* Submitting image file */
				$('.btn-file :file').on('fileselect', function(event, numFiles, label) {

        			var input = $(this).parents('.input-group').find(':text'),
            		log = numFiles > 1 ? numFiles + ' files selected' : label;

        			if( input.length ) {
            			input.val(log);
        			} else {
            			if( log ) alert(log);
        			}

   				});
			});

			/* Submitting image file */
			$(document).on('change', '.btn-file :file', function() {
  				var input = $(this),
      			numFiles = input.get(0).files ? input.get(0).files.length : 1,
      			label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
  				input.trigger('fileselect', [numFiles, label]);
			});
		</script>

    </body>

</html>
