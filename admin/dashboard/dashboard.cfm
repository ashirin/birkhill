
<!--- Authentication of user --->
<CFINCLUDE TEMPLATE="../authenticate.cfm">

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

  	<head>

		<CFINCLUDE TEMPLATE="../_includes/_head_meta.cfm" />

        <CFINCLUDE TEMPLATE="../_includes/_head_css.cfm" />

        <CFINCLUDE TEMPLATE="../_includes/_head_shim.cfm" />

        <CFSET menupage = 'dashboard' />

	</head>


    <!--- Logged in user details --->
    <CFSET loginObj = CreateObject("component","birkhillincludes.login") />
	<CFSCRIPT>
        loginObj.dsn = dsn;
		loginObj.username = cookie.myadmin_username;
		loginObj.password = cookie.myadmin_password;
        logindetails = loginObj.getLoginDetails();
    </CFSCRIPT>


    <body class="hold-transition skin-blue sidebar-mini">

    	<div class="wrapper">

        	<CFINCLUDE TEMPLATE="../_includes/_header.cfm" />

            <CFINCLUDE TEMPLATE="../_includes/_menu.cfm" />

            <!--- Content Wrapper. Contains page content --->
            <div class="content-wrapper">

				<!--- Content Header (Page header) --->
                <section class="content-header">
                  	<h1>
                    	BannTech
                  	</h1>
                  	<ol class="breadcrumb">
                    	<li class="active"><i class="fa fa-dashboard"></i> Dashboard</li>
                  	</ol>
                </section><!--- /.content-header --->

				<!--- Main content --->
        		<section class="content">

          			<div class="row">

						<div class="col-md-3">

                        	<div class="row">

                                <div class="col-md-12">

                                    <div class="small-box bg-aqua">

                                        <div class="inner">
                                            <h3><CFOUTPUT>#usersnumber.userscount#</CFOUTPUT></h3>
                                            <p><strong>Users</strong></p>
                                        </div>

                                        <div class="icon">
                                            <i class="fa fa-users"></i>
                                        </div>

                                    </div>

                                </div>

                            </div>

                        </div>

                        <div class="col-md-9">



                        </div><!--- /.col --->

                    </div><!--- /row --->

        		</section><!--- /.content --->

			</div><!--- /.content-wrapper --->


            <CFINCLUDE TEMPLATE="../_includes/_footer.cfm" />

            <CFINCLUDE TEMPLATE="../_includes/_settings.cfm" />

    	</div><!--- ./wrapper --->

        <CFINCLUDE TEMPLATE="../_includes/_head_js.cfm" />

        <!--- This needs to stay after the _head_js.cfm because of dependencies --->
        <CFINCLUDE TEMPLATE="../_includes/_toast.cfm" />

    </body>

</html>
