
<!--- Set the default username and password to x --->
<CFSET myadmin_username_hash = 'x' />
<CFSET myadmin_password_hash = 'x' />

<!--- Logging in using the session username and password hashes --->
<CFIF #IsDefined('cookie.myadmin_username')# AND #IsDefined('cookie.myadmin_password')# AND #cookie.myadmin_username# NEQ -99 AND #cookie.myadmin_password# NEQ -99>

    <CFSET myadmin_login = 'no' />
	<CFSET myadmin_username_hash = #cookie.myadmin_username# />
	<CFSET myadmin_password_hash = #cookie.myadmin_password# />

<CFELSE>

	<CFSET myadmin_login = 'yes' />

	<!--- Logging in using the sign in form--->
	<CFIF #IsDefined('form.admin_username')# AND #IsDefined('form.admin_password')#>

        <CFSET myID = '#request.mysalt#' & '#form.admin_username#' & '#request.mysalt#' />
        <CFSET mypassword = '#request.mysalt#' & '#form.admin_password#' & '#request.mysalt#' />
        <CFSET myadmin_username_hash = #hash(myID)# />
        <CFSET myadmin_password_hash = #hash(mypassword)# />

 	<!--- Logging in using the remember me method and rememeber cookies --->
	<CFELSEIF #IsDefined('cookie.remember_username')# AND #IsDefined('cookie.remember_password')# AND #cookie.remember_username# NEQ -99 AND #cookie.remember_password# NEQ -99>

        <CFSET myadmin_username_hash = #cookie.remember_username# />
        <CFSET myadmin_password_hash = #cookie.remember_password# />

    </CFIF>

</CFIF>


<CFTRY>

	<CFLOCK TIMEOUT="20">

    	<CFTRANSACTION>

			<!--- Get the user from the system if they currently exist --->
            <CFQUERY NAME="getuser" DATASOURCE="#dsn#">

                SELECT *

                    FROM sys_users a
                            LEFT JOIN sys_users_details b ON a.user_details_UUID = b.user_details_UUID
                            LEFT JOIN sys_users_roles c ON a.user_role_UUID = c.user_role_UUID

                    WHERE a.deleted IS NULL

                    AND a.username_hash = <CFQUERYPARAM VALUE="#myadmin_username_hash#" CFSQLTYPE="cf_sql_varchar">
                    AND a.password_hash = <CFQUERYPARAM VALUE="#myadmin_password_hash#" CFSQLTYPE="cf_sql_varchar">

            </CFQUERY>


            <!--- If logging in record the date and time that the user logged into the system --->
            <CFIF (#myadmin_login# EQ 'yes') AND (#getuser.user_UUID# NEQ "")>

                <CFQUERY NAME="loginuser" DATASOURCE="#dsn#">

                    INSERT INTO sys_logins (login_UUID, user_UUID, login_date)
                        VALUES (
                                <CFQUERYPARAM VALUE="#CreateUUID()#" CFSQLTYPE="cf_sql_varchar">,
                                <CFQUERYPARAM VALUE="#getuser.user_UUID#" CFSQLTYPE="cf_sql_varchar">,
                                <CFQUERYPARAM VALUE="#Now()#" CFSQLTYPE="cf_sql_timestamp">
                                )

                </CFQUERY>

            </CFIF>

        </CFTRANSACTION>

    </CFLOCK>


	<!--- Check whether the user record was found. If not direct to the login page. --->
    <CFIF #getuser.RecordCount# EQ 1>

        <CFIF (#IsDefined('form.remember')#) OR (#IsDefined('cookie.remember_username')# AND #IsDefined('cookie.remember_password')#)>
            <CFCOOKIE NAME="remember_username" VALUE="#getuser.username_hash#" EXPIRES="21">
            <CFCOOKIE NAME="remember_password" VALUE="#getuser.password_hash#" EXPIRES="21">
        </CFIF>

        <CFCOOKIE NAME="myadmin_username" VALUE="#getuser.username_hash#" EXPIRES="1">
        <CFCOOKIE NAME="myadmin_password" VALUE="#getuser.password_hash#" EXPIRES="1">

    <CFELSE>

        <!--- If user is not in the database, wipe cookies and return to login screen with a toast message --->
        <CFCOOKIE NAME="remember_username" VALUE="-99" EXPIRES="NOW">
        <CFCOOKIE NAME="remember_password" VALUE="-99" EXPIRES="NOW">
        <CFCOOKIE NAME="myadmin_username" VALUE="-99" EXPIRES="NOW">
        <CFCOOKIE NAME="myadmin_password" VALUE="-99" EXPIRES="NOW">

        <CFLOCATION URL="../login.cfm?toastcode=-1&toasttitle=User%20Details%20Error&toastmessage=The%20user%20doesn't%20exist%20in%20the%20database" />

    </CFIF>

    <CFCATCH>

    	<!--- Error messgae if login fails --->
    	<CFLOCATION URL="../login.cfm?toastcode=-1&toasttitle=Login%20Error&toastmessage=There%20was%20an%20error%20logging%20in" />

    </CFCATCH>

</CFTRY>
