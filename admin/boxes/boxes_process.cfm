
<!--- Authentication of user --->
<CFINCLUDE TEMPLATE="../authenticate.cfm">

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

  	<head>

		<CFINCLUDE TEMPLATE="../_includes/_head_meta.cfm" />

        <CFINCLUDE TEMPLATE="../_includes/_head_css.cfm" />

        <CFINCLUDE TEMPLATE="../_includes/_head_shim.cfm" />

        <!--- Menu Bookmark Variable --->
        <CFSET menupage = 'manageboxes' />

	</head>


    <!--- Logged in user details --->
    <CFSET loginObj = CreateObject("component","birkhillincludes.login") />
	<CFSCRIPT>
        loginObj.dsn = dsn;
		loginObj.username = cookie.myadmin_username;
		loginObj.password = cookie.myadmin_password;
        logindetails = loginObj.getLoginDetails();
    </CFSCRIPT>

	<!--- Send form details to the database --->
	<CFSET boxObj = CreateObject("component","birkhillincludes.boxes") />
	<CFSCRIPT>
		boxObj.dsn = dsn;
		boxObj.action = 'edit';
		boxObj.box_link1 = box_link1;
		boxObj.box_link2 = box_link2;
		boxObj.box_link3 = box_link3;
		boxObj.box_link4 = box_link4;
		boxObj.box_link5 = box_link5;
		boxObj.box_link6 = box_link6;
		boxObj.box_heading1 = box_heading1;
		boxObj.box_heading2 = box_heading2;
		boxObj.box_heading3 = box_heading3;
		boxObj.box_heading4 = box_heading4;
		boxObj.box_heading5 = box_heading5;
		boxObj.box_heading6 = box_heading6;
		boxObj.box_content1 = box_content1;
		boxObj.box_content2 = box_content2;
		boxObj.box_content3 = box_content3;
		boxObj.box_content4 = box_content4;
		boxObj.box_content5 = box_content5;
		boxObj.box_content6 = box_content6;
		boxObj.currentimage1 = currentimage1;
		boxObj.currentimage2 = currentimage2;
		boxObj.currentimage3 = currentimage3;
		boxObj.currentimage4 = currentimage4;
		boxObj.currentimage5 = currentimage5;
		boxObj.currentimage6 = currentimage6;
		boxObj.uploadfile1 = uploadfile1;
		boxObj.uploadfile2 = uploadfile2;
		boxObj.uploadfile3 = uploadfile3;
		boxObj.uploadfile4 = uploadfile4;
		boxObj.uploadfile5 = uploadfile5;
		boxObj.uploadfile6 = uploadfile6;
		myresult = boxObj.boxprocess();
	</CFSCRIPT>


	<body class="hold-transition sidebar-mini skin-blue">

    	<div class="wrapper">

        	<CFINCLUDE TEMPLATE="../_includes/_header.cfm" />

            <CFINCLUDE TEMPLATE="../_includes/_menu.cfm" />

            <!--- Content Wrapper. Contains page content --->
      		<div class="content-wrapper">

				<CFSET locationurl = "./manage_boxes.cfm?toastcode=" & "#myresult.toastCode#" & "&toasttitle=" & "#myresult.toastTitle#" & "&toastmessage=" & "#myresult.toastMessage#" />

                <CFLOCATION URL="#locationurl#" />

            </div><!--- /.content-wrapper --->


            <CFINCLUDE TEMPLATE="../_includes/_footer.cfm" />

    	</div><!--- ./wrapper --->

        <CFINCLUDE TEMPLATE="../_includes/_head_js.cfm" />

    </body>

</html>
