
<!--- Authentication of user --->
<CFINCLUDE TEMPLATE="../authenticate.cfm">

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

  	<head>

		<CFINCLUDE TEMPLATE="../_includes/_head_meta.cfm" />

        <CFINCLUDE TEMPLATE="../_includes/_head_css.cfm" />

        <style>
			label.error {
				margin-top: 10px;
				color: #DD4B39;
			}
		</style>

        <CFINCLUDE TEMPLATE="../_includes/_head_shim.cfm" />

        <!--- Menu Bookmark Variable --->
        <CFSET menupage = 'manageboxes' />

	</head>


    <!--- Logged in user details --->
    <CFSET loginObj = CreateObject("component","birkhillincludes.login") />
	<CFSCRIPT>
        loginObj.dsn = dsn;
		loginObj.username = cookie.myadmin_username;
		loginObj.password = cookie.myadmin_password;
        logindetails = loginObj.getLoginDetails();
    </CFSCRIPT>

	<!--- Get dropdown details --->
	<CFSET pagesObj = CreateObject("component","birkhillincludes.pages") />
	<CFSCRIPT>
		pagesObj.dsn = #dsn#;
		toplevelmenus = pagesObj.getTopLevelMenus();
	</CFSCRIPT>

	<!--- Get box details --->
	<CFSET boxObj = CreateObject("component","birkhillincludes.boxes") />
	<CFSCRIPT>
		boxObj.dsn = #dsn#;
		boxObj.box_UUID = 1;
		getbox1 = boxObj.getbox();
		boxObj.box_UUID = 2;
		getbox2 = boxObj.getbox();
		boxObj.box_UUID = 3;
		getbox3 = boxObj.getbox();
		boxObj.box_UUID = 4;
		getbox4 = boxObj.getbox();
		boxObj.box_UUID = 5;
		getbox5 = boxObj.getbox();
		boxObj.box_UUID = 6;
		getbox6 = boxObj.getbox();
	</CFSCRIPT>


    <body class="hold-transition sidebar-mini skin-blue">

    	<div class="wrapper">

        	<CFINCLUDE TEMPLATE="../_includes/_header.cfm" />

            <CFINCLUDE TEMPLATE="../_includes/_menu.cfm" />

            <!--- Content Wrapper. Contains page content --->
      		<div class="content-wrapper">

            	<section class="content-header">
                  	<h1>
                    	EDIT Box
                  	</h1>
                    <ol class="breadcrumb">
                    	<li><a href="<CFOUTPUT>#request.baseurl#</CFOUTPUT>dashboard/dashboard.cfm"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                        <li class="active"><i class="fa fa-dropbox"></i> Edit Box</li>
                	</ol>
                </section><!--- /.content-header --->

                <section class="content">

                    <div class="row">

                        <form role="form" method="post" action="<CFOUTPUT>#request.baseurl#</CFOUTPUT>boxes/boxes_process.cfm" id="myform" name="myform" enctype="multipart/form-data">

                            <CFOUTPUT>
                                <input type="hidden" name="action" id="action" value="edit" />
                            </CFOUTPUT>

                            <div class="col-md-12">

              					<div class="nav-tabs-custom">

	                				<ul class="nav nav-tabs">
										<li class="active"><a href="#content1" data-toggle="tab">Box 1</a></li>
										<li><a href="#content2" data-toggle="tab">Box 2</a></li>
										<li><a href="#content3" data-toggle="tab">Box 3</a></li>
										<li><a href="#content4" data-toggle="tab">Box 4</a></li>
										<li><a href="#content5" data-toggle="tab">Box 5</a></li>
										<li><a href="#content6" data-toggle="tab">Box 6</a></li>
	                				</ul>

                					<div class="tab-content">

										<div class="active tab-pane" id="content1">

		                                    <div class="row">

			                                    <div class="col-md-5">

				                                    <div class="form-group">
			                                        	<label for="box_heading1">Box Heading</label> <span class="text-red" style="font-size: 20px;">*</span>
			                                            <input type="text" class="form-control" name="box_heading1" id="box_heading1" placeholder="Box Heading" <CFOUTPUT QUERY="getbox1">value="#box_heading#"</CFOUTPUT> />
			  										</div>

			  										<div class="form-group">
		                                            	<label for="box_link1">Box Link</label> <span style="font-size: 20px;">&nbsp;</span>
		                                                <input type="text" class="form-control" name="box_link1" id="box_link1" placeholder="Box Link" <CFOUTPUT QUERY="getbox1">value="#box_link#"</CFOUTPUT> />
									  				</div>

													<div class="row">

                                                        <div class="col-md-8">

                                                            <label>Box Image</label> <span style="font-size: 20px;">&nbsp;</span>
                                                            <div class="input-group">
                                                                <span class="input-group-btn">
                                                                    <span class="btn btn-primary btn-file">
                                                                        Browse&hellip; <input type="file" name="uploadfile1" id="uploadfile1" multiple>
                                                                    </span>
                                                                </span>
                                                                <input type="text" class="form-control" readonly>
                                                            </div>

                                                        </div><!--- /.col --->

														<CFIF #getbox1.box_uploadfile# NEQ ''>

																<br />

	                                                            <div class="col-md-4">
	                                                                <CFOUTPUT>
	                                                                    <img src="../_img/boxes/#getbox1.box_uploadfile#" width="70" style="border: 1px solid ##ccc; display: block; margin: auto;" />
	                                                                </CFOUTPUT>
	                                                            </div><!--- /.col --->
	                                                        </div><!--- /.row --->

	                                                        <br />

	                                                        <div class="row">
	                                                            <div class="col-md-8">
	                                                                <div class="form-group">
	                                                                    <label for="uploadfile1">Current Box Image</label> (delete to remove)
	                                                                    <input type="text" class="form-control" id="currentimage1" name="currentimage1" <CFOUTPUT>value="#getbox1.box_uploadfile#"</CFOUTPUT> />
	                                                                </div>
	                                                            </div><!--- /.col --->
	                                                        </div><!--- /.row --->

                                                    	<CFELSE>

																<br />

	                                                            <div class="col-md-4">
	                                                                <img src="../_img/generic_image.png" width="70" style="border: 1px solid #ccc; display: block; margin: auto;" />
	                                                                <input type="hidden" name="currentimage1" id="currentimage1" />
	                                                            </div><!--- /.col --->
	                                                        </div><!--- /.row --->

                                                    	</CFIF>

			    								</div><!--- /.col --->

			    								<div class="col-md-7">

		                                        	<div class="form-group">
		                                            	<label for="box_content1">Box Content 1</label> <span class="text-red" style="font-size: 20px;">*</span>
		                                                <textarea name="box_content1" id="box_content1"><CFOUTPUT QUERY="getbox1">#box_content#</CFOUTPUT></textarea>
		                                            </div>

		                                        </div><!--- /.col --->

		  									</div><!--- /.row --->

		  									<div class="row">

		                                        <div class="col-md-12">

		                                            <div class="box-footer">
		                                            	<button type="submit" class="btn btn-primary">Edit Box</button>
		                                            </div><!--- /.box-footer --->

		                                        </div><!--- /.col --->

		                                    </div><!--- /.row --->

										</div><!--- /.tab-pane --->

										<div class="tab-pane" id="content2">

											<div class="row">

			                                    <div class="col-md-5">

				                                    <div class="form-group">
			                                        	<label for="box_heading2">Box Heading</label> <span class="text-red" style="font-size: 20px;">*</span>
			                                            <input type="text" class="form-control" name="box_heading2" id="box_heading2" placeholder="Box Heading" <CFOUTPUT QUERY="getbox2">value="#box_heading#"</CFOUTPUT> />
			  										</div>

			  										<div class="form-group">
		                                            	<label for="box_link2">Box Link</label> <span style="font-size: 20px;">&nbsp;</span>
		                                                <input type="text" class="form-control" name="box_link2" id="box_link2" placeholder="Box Link" <CFOUTPUT QUERY="getbox2">value="#box_link#"</CFOUTPUT> />
									  				</div>

													<div class="row">

                                                        <div class="col-md-8">

                                                            <label>Box Image</label> <span style="font-size: 20px;">&nbsp;</span>
                                                            <div class="input-group">
                                                                <span class="input-group-btn">
                                                                    <span class="btn btn-primary btn-file">
                                                                        Browse&hellip; <input type="file" name="uploadfile2" id="uploadfile2" multiple>
                                                                    </span>
                                                                </span>
                                                                <input type="text" class="form-control" readonly>
                                                            </div>

                                                        </div><!--- /.col --->

														<CFIF #getbox2.box_uploadfile# NEQ ''>

																<br />

	                                                            <div class="col-md-4">
	                                                                <CFOUTPUT>
	                                                                    <img src="../_img/boxes/#getbox2.box_uploadfile#" width="70" style="border: 1px solid ##ccc; display: block; margin: auto;" />
	                                                                </CFOUTPUT>
	                                                            </div><!--- /.col --->
	                                                        </div><!--- /.row --->

	                                                        <br />

	                                                        <div class="row">
	                                                            <div class="col-md-8">
	                                                                <div class="form-group">
	                                                                    <label for="uploadfile2">Current Box Image</label> (delete to remove)
	                                                                    <input type="text" class="form-control" id="currentimage2" name="currentimage2" <CFOUTPUT>value="#getbox2.box_uploadfile#"</CFOUTPUT> />
	                                                                </div>
	                                                            </div><!--- /.col --->
	                                                        </div><!--- /.row --->

                                                    	<CFELSE>

																<br />

	                                                            <div class="col-md-4">
	                                                                <img src="../_img/generic_image.png" width="70" style="border: 1px solid #ccc; display: block; margin: auto;" />
	                                                                <input type="hidden" name="currentimage2" id="currentimage2" />
	                                                            </div><!--- /.col --->
	                                                        </div><!--- /.row --->

                                                    	</CFIF>

			    								</div><!--- /.col --->

			    								<div class="col-md-7">

		                                        	<div class="form-group">
		                                            	<label for="box_content2">Box Content 2</label> <span class="text-red" style="font-size: 20px;">*</span>
		                                                <textarea name="box_content2" id="box_content2"><CFOUTPUT QUERY="getbox2">#box_content#</CFOUTPUT></textarea>
		                                            </div>

		                                        </div><!--- /.col --->

		  									</div><!--- /.row --->

		  									<div class="row">

		                                        <div class="col-md-12">

		                                            <div class="box-footer">
		                                            	<button type="submit" class="btn btn-primary">Edit Box</button>
		                                            </div><!--- /.box-footer --->

		                                        </div><!--- /.col --->

		                                    </div><!--- /.row --->

		                                </div><!--- /.tab-pane --->

		                                <div class="tab-pane" id="content3">

											<div class="row">

			                                    <div class="col-md-5">

				                                    <div class="form-group">
			                                        	<label for="box_heading3">Box Heading</label> <span class="text-red" style="font-size: 20px;">*</span>
			                                            <input type="text" class="form-control" name="box_heading3" id="box_heading3" placeholder="Box Heading" <CFOUTPUT QUERY="getbox3">value="#box_heading#"</CFOUTPUT> />
			  										</div>

			  										<div class="form-group">
		                                            	<label for="box_link3">Box Link</label> <span style="font-size: 20px;">&nbsp;</span>
		                                                <input type="text" class="form-control" name="box_link3" id="box_link3" placeholder="Box Link" <CFOUTPUT QUERY="getbox3">value="#box_link#"</CFOUTPUT> />
									  				</div>

													<div class="row">

                                                        <div class="col-md-8">

                                                            <label>Box Image</label> <span style="font-size: 20px;">&nbsp;</span>
                                                            <div class="input-group">
                                                                <span class="input-group-btn">
                                                                    <span class="btn btn-primary btn-file">
                                                                        Browse&hellip; <input type="file" name="uploadfile3" id="uploadfile3" multiple>
                                                                    </span>
                                                                </span>
                                                                <input type="text" class="form-control" readonly>
                                                            </div>

                                                        </div><!--- /.col --->

														<CFIF #getbox3.box_uploadfile# NEQ ''>

																<br />

	                                                            <div class="col-md-4">
	                                                                <CFOUTPUT>
	                                                                    <img src="../_img/boxes/#getbox3.box_uploadfile#" width="70" style="border: 1px solid ##ccc; display: block; margin: auto;" />
	                                                                </CFOUTPUT>
	                                                            </div><!--- /.col --->
	                                                        </div><!--- /.row --->

	                                                        <br />

	                                                        <div class="row">
	                                                            <div class="col-md-8">
	                                                                <div class="form-group">
	                                                                    <label for="uploadfile3">Current Box Image</label> (delete to remove)
	                                                                    <input type="text" class="form-control" id="currentimage3" name="currentimage3" <CFOUTPUT>value="#getbox3.box_uploadfile#"</CFOUTPUT> />
	                                                                </div>
	                                                            </div><!--- /.col --->
	                                                        </div><!--- /.row --->

                                                    	<CFELSE>

																<br />

	                                                            <div class="col-md-4">
	                                                                <img src="../_img/generic_image.png" width="70" style="border: 1px solid #ccc; display: block; margin: auto;" />
	                                                                <input type="hidden" name="currentimage3" id="currentimage3" />
	                                                            </div><!--- /.col --->
	                                                        </div><!--- /.row --->

                                                    	</CFIF>

			    								</div><!--- /.col --->

			    								<div class="col-md-7">

		                                        	<div class="form-group">
		                                            	<label for="box_content1">Box Content 3</label> <span class="text-red" style="font-size: 20px;">*</span>
		                                                <textarea name="box_content3" id="box_content3"><CFOUTPUT QUERY="getbox3">#box_content#</CFOUTPUT></textarea>
		                                            </div>

		                                        </div><!--- /.col --->

		  									</div><!--- /.row --->

		  									<div class="row">

		                                        <div class="col-md-12">

		                                            <div class="box-footer">
		                                            	<button type="submit" class="btn btn-primary">Edit Box</button>
		                                            </div><!--- /.box-footer --->

		                                        </div><!--- /.col --->

		                                    </div><!--- /.row --->

		                                </div><!--- /.tab-pane --->

		                                <div class="tab-pane" id="content4">

											<div class="row">

			                                    <div class="col-md-5">

				                                    <div class="form-group">
			                                        	<label for="box_heading4">Box Heading</label> <span class="text-red" style="font-size: 20px;">*</span>
			                                            <input type="text" class="form-control" name="box_heading4" id="box_heading4" placeholder="Box Heading" <CFOUTPUT QUERY="getbox4">value="#box_heading#"</CFOUTPUT> />
			  										</div>

			  										<div class="form-group">
		                                            	<label for="box_link4">Box Link</label> <span style="font-size: 20px;">&nbsp;</span>
		                                                <input type="text" class="form-control" name="box_link4" id="box_link4" placeholder="Box Link" <CFOUTPUT QUERY="getbox4">value="#box_link#"</CFOUTPUT> />
									  				</div>

													<div class="row">

                                                        <div class="col-md-8">

                                                            <label>Box Image</label> <span style="font-size: 20px;">&nbsp;</span>
                                                            <div class="input-group">
                                                                <span class="input-group-btn">
                                                                    <span class="btn btn-primary btn-file">
                                                                        Browse&hellip; <input type="file" name="uploadfile4" id="uploadfile4" multiple>
                                                                    </span>
                                                                </span>
                                                                <input type="text" class="form-control" readonly>
                                                            </div>

                                                        </div><!--- /.col --->

														<CFIF #getbox4.box_uploadfile# NEQ ''>

																<br />

	                                                            <div class="col-md-4">
	                                                                <CFOUTPUT>
	                                                                    <img src="../_img/boxes/#getbox4.box_uploadfile#" width="70" style="border: 1px solid ##ccc; display: block; margin: auto;" />
	                                                                </CFOUTPUT>
	                                                            </div><!--- /.col --->
	                                                        </div><!--- /.row --->

	                                                        <br />

	                                                        <div class="row">
	                                                            <div class="col-md-8">
	                                                                <div class="form-group">
	                                                                    <label for="uploadfile1">Current Box Image</label> (delete to remove)
	                                                                    <input type="text" class="form-control" id="currentimage4" name="currentimage4" <CFOUTPUT>value="#getbox4.box_uploadfile#"</CFOUTPUT> />
	                                                                </div>
	                                                            </div><!--- /.col --->
	                                                        </div><!--- /.row --->

                                                    	<CFELSE>

																<br />

	                                                            <div class="col-md-4">
	                                                                <img src="../_img/generic_image.png" width="70" style="border: 1px solid #ccc; display: block; margin: auto;" />
	                                                                <input type="hidden" name="currentimage4" id="currentimage4" />
	                                                            </div><!--- /.col --->
	                                                        </div><!--- /.row --->

                                                    	</CFIF>

			    								</div><!--- /.col --->

			    								<div class="col-md-7">

		                                        	<div class="form-group">
		                                            	<label for="box_content4">Box Content 4</label> <span class="text-red" style="font-size: 20px;">*</span>
		                                                <textarea name="box_content4" id="box_content4"><CFOUTPUT QUERY="getbox4">#box_content#</CFOUTPUT></textarea>
		                                            </div>

		                                        </div><!--- /.col --->

		  									</div><!--- /.row --->

		  									<div class="row">

		                                        <div class="col-md-12">

		                                            <div class="box-footer">
		                                            	<button type="submit" class="btn btn-primary">Edit Box</button>
		                                            </div><!--- /.box-footer --->

		                                        </div><!--- /.col --->

		                                    </div><!--- /.row --->

		                                </div><!--- /.tab-pane --->

		                                <div class="tab-pane" id="content5">

											<div class="row">

			                                    <div class="col-md-5">

				                                    <div class="form-group">
			                                        	<label for="box_heading5">Box Heading</label> <span class="text-red" style="font-size: 20px;">*</span>
			                                            <input type="text" class="form-control" name="box_heading5" id="box_heading5" placeholder="Box Heading" <CFOUTPUT QUERY="getbox5">value="#box_heading#"</CFOUTPUT> />
			  										</div>

			  										<div class="form-group">
		                                            	<label for="box_link5">Box Link</label> <span style="font-size: 20px;">&nbsp;</span>
		                                                <input type="text" class="form-control" name="box_link5" id="box_link5" placeholder="Box Link" <CFOUTPUT QUERY="getbox5">value="#box_link#"</CFOUTPUT> />
									  				</div>

													<div class="row">

                                                        <div class="col-md-8">

                                                            <label>Box Image</label> <span style="font-size: 20px;">&nbsp;</span>
                                                            <div class="input-group">
                                                                <span class="input-group-btn">
                                                                    <span class="btn btn-primary btn-file">
                                                                        Browse&hellip; <input type="file" name="uploadfile5" id="uploadfile5" multiple>
                                                                    </span>
                                                                </span>
                                                                <input type="text" class="form-control" readonly>
                                                            </div>

                                                        </div><!--- /.col --->

														<CFIF #getbox5.box_uploadfile# NEQ ''>

																<br />

	                                                            <div class="col-md-4">
	                                                                <CFOUTPUT>
	                                                                    <img src="../_img/boxes/#getbox5.box_uploadfile#" width="70" style="border: 1px solid ##ccc; display: block; margin: auto;" />
	                                                                </CFOUTPUT>
	                                                            </div><!--- /.col --->
	                                                        </div><!--- /.row --->

	                                                        <br />

	                                                        <div class="row">
	                                                            <div class="col-md-8">
	                                                                <div class="form-group">
	                                                                    <label for="uploadfile5">Current Box Image</label> (delete to remove)
	                                                                    <input type="text" class="form-control" id="currentimage5" name="currentimage5" <CFOUTPUT>value="#getbox5.box_uploadfile#"</CFOUTPUT> />
	                                                                </div>
	                                                            </div><!--- /.col --->
	                                                        </div><!--- /.row --->

                                                    	<CFELSE>

																<br />

	                                                            <div class="col-md-4">
	                                                                <img src="../_img/generic_image.png" width="70" style="border: 1px solid #ccc; display: block; margin: auto;" />
	                                                                <input type="hidden" name="currentimage5" id="currentimage5" />
	                                                            </div><!--- /.col --->
	                                                        </div><!--- /.row --->

                                                    	</CFIF>

			    								</div><!--- /.col --->

			    								<div class="col-md-7">

		                                        	<div class="form-group">
		                                            	<label for="box_content5">Box Content 5</label> <span class="text-red" style="font-size: 20px;">*</span>
		                                                <textarea name="box_content5" id="box_content5"><CFOUTPUT QUERY="getbox5">#box_content#</CFOUTPUT></textarea>
		                                            </div>

		                                        </div><!--- /.col --->

		  									</div><!--- /.row --->

		  									<div class="row">

		                                        <div class="col-md-12">

		                                            <div class="box-footer">
		                                            	<button type="submit" class="btn btn-primary">Edit Box</button>
		                                            </div><!--- /.box-footer --->

		                                        </div><!--- /.col --->

		                                    </div><!--- /.row --->

		                                </div><!--- /.tab-pane --->

		                                <div class="tab-pane" id="content6">

											<div class="row">

			                                    <div class="col-md-5">

				                                    <div class="form-group">
			                                        	<label for="box_heading6">Box Heading</label> <span class="text-red" style="font-size: 20px;">*</span>
			                                            <input type="text" class="form-control" name="box_heading6" id="box_heading6" placeholder="Box Heading" <CFOUTPUT QUERY="getbox6">value="#box_heading#"</CFOUTPUT> />
			  										</div>

			  										<div class="form-group">
		                                            	<label for="box_link6">Box Link</label> <span style="font-size: 20px;">&nbsp;</span>
		                                                <input type="text" class="form-control" name="box_link6" id="box_link6" placeholder="Box Link" <CFOUTPUT QUERY="getbox6">value="#box_link#"</CFOUTPUT> />
									  				</div>

													<div class="row">

                                                        <div class="col-md-8">

                                                            <label>Box Image</label> <span style="font-size: 20px;">&nbsp;</span>
                                                            <div class="input-group">
                                                                <span class="input-group-btn">
                                                                    <span class="btn btn-primary btn-file">
                                                                        Browse&hellip; <input type="file" name="uploadfile6" id="uploadfile6" multiple>
                                                                    </span>
                                                                </span>
                                                                <input type="text" class="form-control" readonly>
                                                            </div>

                                                        </div><!--- /.col --->

														<CFIF #getbox6.box_uploadfile# NEQ ''>

																<br />

	                                                            <div class="col-md-4">
	                                                                <CFOUTPUT>
	                                                                    <img src="../_img/boxes/#getbox6.box_uploadfile#" width="70" style="border: 1px solid ##ccc; display: block; margin: auto;" />
	                                                                </CFOUTPUT>
	                                                            </div><!--- /.col --->
	                                                        </div><!--- /.row --->

	                                                        <br />

	                                                        <div class="row">
	                                                            <div class="col-md-8">
	                                                                <div class="form-group">
	                                                                    <label for="uploadfile6">Current Box Image</label> (delete to remove)
	                                                                    <input type="text" class="form-control" id="currentimage6" name="currentimage6" <CFOUTPUT>value="#getbox6.box_uploadfile#"</CFOUTPUT> />
	                                                                </div>
	                                                            </div><!--- /.col --->
	                                                        </div><!--- /.row --->

                                                    	<CFELSE>

																<br />

	                                                            <div class="col-md-4">
	                                                                <img src="../_img/generic_image.png" width="70" style="border: 1px solid #ccc; display: block; margin: auto;" />
	                                                                <input type="hidden" name="currentimage6" id="currentimage6" />
	                                                            </div><!--- /.col --->
	                                                        </div><!--- /.row --->

                                                    	</CFIF>

			    								</div><!--- /.col --->

			    								<div class="col-md-7">

		                                        	<div class="form-group">
		                                            	<label for="box_content6">Box Content 6</label> <span class="text-red" style="font-size: 20px;">*</span>
		                                                <textarea name="box_content6" id="box_content6"><CFOUTPUT QUERY="getbox6">#box_content#</CFOUTPUT></textarea>
		                                            </div>

		                                        </div><!--- /.col --->

		  									</div><!--- /.row --->

		  									<div class="row">

		                                        <div class="col-md-12">

		                                            <div class="box-footer">
		                                            	<button type="submit" class="btn btn-primary">Edit Box</button>
		                                            </div><!--- /.box-footer --->

		                                        </div><!--- /.col --->

		                                    </div><!--- /.row --->

		                                </div><!--- /.tab-pane --->

                            		</div><!--- /.tab-content --->

                        		</div><!--- /.nav-tabs-custom --->

                         	</div><!--- /.col --->

						</form>

                    </div><!--- /.row --->

                </section><!--- /.content --->

            </div><!--- /.content-wrapper --->

            <CFINCLUDE TEMPLATE="../_includes/_footer.cfm" />

    	</div><!--- ./wrapper --->

        <CFINCLUDE TEMPLATE="../_includes/_head_js.cfm" />

        <script type="text/javascript" language="javascript">
			$(document).ready(function() {
				/* Form validation */
				$("#myform").validate({
					rules: {
						box_heading1: "required",
						box_heading2: "required",
						box_heading3: "required",
						box_heading4: "required",
						box_heading5: "required",
						box_heading6: "required",
						box_content1: "required",
						box_content2: "required",
						box_content3: "required",
						box_content4: "required",
						box_content5: "required",
						box_content6: "required"
					},
					messages: {
						box_heading1: "Please enter Box Heading 1",
						box_heading2: "Please enter Box Heading 2",
						box_heading3: "Please enter Box Heading 3",
						box_heading4: "Please enter Box Heading 4",
						box_heading5: "Please enter Box Heading 5",
						box_heading6: "Please enter Box Heading 6",
						box_content1: "Please enter Page Content 1",
						box_content2: "Please enter Page Content 2",
						box_content3: "Please enter Page Content 3",
						box_content4: "Please enter Page Content 4",
						box_content5: "Please enter Page Content 5",
						box_content6: "Please enter Page Content 6"
					}
				});

				/* CKEditor initilizations */
		        CKEDITOR.replace('box_content1');
		        CKEDITOR.replace('box_content2');
		        CKEDITOR.replace('box_content3');
		        CKEDITOR.replace('box_content4');
		        CKEDITOR.replace('box_content5');
		        CKEDITOR.replace('box_content6');

				/* Upload image files */
				$('.btn-file :file').on('fileselect', function(event, numFiles, label) {
        			var input = $(this).parents('.input-group').find(':text'),
            		log = numFiles > 1 ? numFiles + ' files selected' : label;
        			if( input.length ) {
            			input.val(log);
        			} else {
            			if( log ) alert(log);
        			}
   				});
			});

			/* Upload image files */
			$(document).on('change', '.btn-file :file', function() {
  				var input = $(this),
      			numFiles = input.get(0).files ? input.get(0).files.length : 1,
      			label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
  				input.trigger('fileselect', [numFiles, label]);
			});
		</script>

		<CFINCLUDE TEMPLATE="../_includes/_toast.cfm" />

    </body>

</html>
