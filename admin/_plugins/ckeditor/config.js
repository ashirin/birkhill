/**
 * @license Copyright (c) 2003-2016, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here. For example:
	// config.language = 'fr';
	// config.uiColor = '#AADC6E';


config.toolbar = 'MyToolbar';



    config.toolbar_MyToolbar =
    [
        { name: 'clipboard', items : [ 'Cut','Copy','Paste','PasteText','PasteFromWord','-','Undo','Redo' ] },
        { name: 'editing', items : [ 'Find','Replace'] },
        { name: 'insert', items : [ 'Image','Youtube','Table', 'HorizontalRule','SpecialChar' ] },
                '/',
        { name: 'styles', items : [ 'Styles','Format' ] },
        { name: 'basicstyles', items : [ 'Bold','Italic','-','RemoveFormat' ] },
        { name: 'paragraph', items : [ 'NumberedList','BulletedList','-','Outdent','Indent','-','Blockquote' ] },
        { name: 'links', items : [ 'Link','Unlink','-','Source' ] }

    ];

config.pasteFromWordRemoveFontStyles = true;
config.toolbarCanCollapse = false;
config.extraPlugins = 'stylesheetparser,youtube';
config.contentsCss = '../editor.css';
config.stylesSet = [];

config.youtube_width = '320';
config.youtube_height = '240';
config.youtube_related = false;

config.height = '200px'; // 500 pixels.

// the 'currentfolder' is relative to the path 'request.uploadWebRoot' which is set in
// /filemanaer/connectors/cfm/filemanager.config.cfm. So, if your uploadWebroot is '/uploads/',
// and the 'currentFolder is '/Image/', then the files will be uploaded to /uploads/Image/.
    config.filebrowserBrowseUrl = '/birkhill/admin/_plugins/ckeditor/filemanager/index.html';
    config.filebrowserImageBrowseUrl = '/birkhill/admin/_plugins/ckeditor/filemanager/index.html?type=Images&currentFolder=/Image/';
    config.filebrowserFlashBrowseUrl = '/birkhill/admin/_plugins/ckeditor/filemanager/index.html?type=Flash&currentFolder=/Flash/';
    config.filebrowserUploadUrl = '/birkhill/admin/_plugins/ckeditor/filemanager/connectors/cfm/filemanager.cfm?mode=add&type=Files&currentFolder=/birkhill/uploads/File/';
    config.filebrowserImageUploadUrl = '/birkhill/admin/_plugins/ckeditor/filemanager/connectors/cfm/filemanager.cfm?mode=add&type=Images&currentFolder=/Image/';
    config.filebrowserFlashUploadUrl = '/birkhill/admin/_plugins/ckeditor/filemanager/connectors/cfm/filemanager.cfm?mode=add&type=Flash&currentFolder=/Flash/';


//config.skin = 'v2';
config.startupFocus = false;

};
