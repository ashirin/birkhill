
<!--- Authentication of user --->
<CFINCLUDE TEMPLATE="../authenticate.cfm">

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

  	<head>

		<CFINCLUDE TEMPLATE="../_includes/_head_meta.cfm" />

        <CFINCLUDE TEMPLATE="../_includes/_head_css.cfm" />

        <style>
			label.error {
				margin-top: 10px;
				color: #DD4B39;
			}
		</style>

        <CFINCLUDE TEMPLATE="../_includes/_head_shim.cfm" />

        <!--- Menu Bookmark Variable --->
        <CFSET menupage = 'manageslideshow' />

	</head>


    <!--- Logged in user details --->
    <CFSET loginObj = CreateObject("component","birkhillincludes.login") />
	<CFSCRIPT>
        loginObj.dsn = dsn;
		loginObj.username = cookie.myadmin_username;
		loginObj.password = cookie.myadmin_password;
        logindetails = loginObj.getLoginDetails();
    </CFSCRIPT>

	<!--- Get Dropdown details --->
	<CFSET slideshowObj = CreateObject("component","birkhillincludes.slideshow") />
	<CFSCRIPT>
		slideshowObj.dsn = #dsn#;
		slideshowdetails = slideshowObj.getSlideshowDetails();
	</CFSCRIPT>


    <body class="hold-transition sidebar-mini skin-blue">

    	<div class="wrapper">

        	<CFINCLUDE TEMPLATE="../_includes/_header.cfm" />

            <CFINCLUDE TEMPLATE="../_includes/_menu.cfm" />


            <!--- Content Wrapper. Contains page content --->
      		<div class="content-wrapper">

            	<section class="content-header">
                  	<h1>
                    	MANAGE Slideshow
                  	</h1>
                    <ol class="breadcrumb">
                    	<li><a href="<CFOUTPUT>#request.baseurl#</CFOUTPUT>dashboard/dashboard.cfm"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                        <li><i class="fa fa-camera"></i> Manage Slideshow</li>
                	</ol>
                </section>

                <section class="content">

                    <div class="row">

                        <form role="form" method="post" action="<CFOUTPUT>#request.baseurl#</CFOUTPUT>slideshow/slideshow_process.cfm" id="myform" name="myform">

                            <CFOUTPUT QUERY="slideshowdetails">
                                <input type="hidden" name="slideshow_UUID" id="slideshow_UUID" value="#slideshow_UUID#" />

	                            <div class="col-md-12">

	                                <div class="box box-primary">

	                                    <div class="box-body">

	                                        <div class="row">

	                                            <div class="col-md-4">

	                                                <div class="form-group">
	                                                    <label for="slide1_large">Slide 1 <small>(large text)</small></label> <span class="text-red" style="font-size: 20px;">*</span>
	                                                    <input type="text" class="form-control" name="slide1_large" id="slide1_large" placeholder="Slide 1 Large Text" value="#slide1_large#" />
	                                                </div>

	                                                <div class="form-group">
	                                                    <label for="slide2_large">Slide 2 <small>(large text)</small></label> <span class="text-red" style="font-size: 20px;">*</span>
	                                                    <input type="text" class="form-control" name="slide2_large" id="slide2_large" placeholder="Slide 2 Large Text" value="#slide2_large#" />
	                                                </div>

	                                                <div class="form-group">
	                                                    <label for="slide3_large">Slide 3 <small>(large text)</small></label> <span class="text-red" style="font-size: 20px;">*</span>
	                                                    <input type="text" class="form-control" name="slide3_large" id="slide3_large" placeholder="Slide 3 Large Text" value="#slide3_large#" />
	                                                </div>

	                                                <div class="form-group">
	                                                    <label for="slide4_large">Slide 4 <small>(large text)</small></label> <span class="text-red" style="font-size: 20px;">*</span>
	                                                    <input type="text" class="form-control" name="slide4_large" id="slide4_large" placeholder="Slide 4 Large Text" value="#slide4_large#" />
	                                                </div>

	                                                <div class="form-group">
	                                                    <label for="slide5_large">Slide 5 <small>(large text)</small></label> <span class="text-red" style="font-size: 20px;">*</span>
	                                                    <input type="text" class="form-control" name="slide5_large" id="slide5_large" placeholder="Slide 5 Large Text" value="#slide5_large#" />
	                                                </div>

												</div><!--- /.col --->

												<div class="col-md-4">

	                                                <div class="form-group">
	                                                    <label for="slide1_medium">Slide 1 <small>(medium text)</small></label> <span class="text-red" style="font-size: 20px;">*</span>
	                                                    <input type="text" class="form-control" name="slide1_medium" id="slide1_medium" placeholder="Slide 1 Medium Text" value="#slide1_medium#" />
	                                                </div>

	                                                <div class="form-group">
	                                                    <label for="slide2_medium">Slide 2 <small>(medium text)</small></label> <span class="text-red" style="font-size: 20px;">*</span>
	                                                    <input type="text" class="form-control" name="slide2_medium" id="slide2_medium" placeholder="Slide 2 Medium Text" value="#slide2_medium#" />
	                                                </div>

	                                                <div class="form-group">
	                                                    <label for="slide3_medium">Slide 3 <small>(medium text)</small></label> <span class="text-red" style="font-size: 20px;">*</span>
	                                                    <input type="text" class="form-control" name="slide3_medium" id="slide3_medium" placeholder="Slide 3 Medium Text" value="#slide3_medium#" />
	                                                </div>

	                                                <div class="form-group">
	                                                    <label for="slide4_medium">Slide 4 <small>(medium text)</small></label> <span class="text-red" style="font-size: 20px;">*</span>
	                                                    <input type="text" class="form-control" name="slide4_medium" id="slide4_medium" placeholder="Slide 4 Medium Text" value="#slide4_medium#" />
	                                                </div>

	                                                <div class="form-group">
	                                                    <label for="slide5_medium">Slide 5 <small>(medium text)</small></label> <span class="text-red" style="font-size: 20px;">*</span>
	                                                    <input type="text" class="form-control" name="slide5_medium" id="slide5_medium" placeholder="Slide 5 Medium Text" value="#slide5_medium#" />
	                                                </div>

												</div><!--- /.col --->

												<div class="col-md-4">

	                                                <div class="form-group">
	                                                    <label for="slide1_link">Slide 1 <small>(button link)</small></label> <span class="text-red" style="font-size: 20px;">*</span>
	                                                    <input type="text" class="form-control" name="slide1_link" id="slide1_link" placeholder="Slide 1 Button Link" value="#slide1_link#" />
	                                                </div>

	                                                <div class="form-group">
	                                                    <label for="slide2_link">Slide 2 <small>(button link)</small></label> <span class="text-red" style="font-size: 20px;">*</span>
	                                                    <input type="text" class="form-control" name="slide2_link" id="slide2_link" placeholder="Slide 2 Button Link" value="#slide2_link#" />
	                                                </div>

	                                                <div class="form-group">
	                                                    <label for="slide3_link">Slide 3 <small>(button link)</small></label> <span class="text-red" style="font-size: 20px;">*</span>
	                                                    <input type="text" class="form-control" name="slide3_link" id="slide3_link" placeholder="Slide 3 Button Link" value="#slide3_link#" />
	                                                </div>

	                                                <div class="form-group">
	                                                    <label for="slide4_link">Slide 4 <small>(button link)</small></label> <span class="text-red" style="font-size: 20px;">*</span>
	                                                    <input type="text" class="form-control" name="slide4_link" id="slide4_link" placeholder="Slide 4 Button Link" value="#slide4_link#" />
	                                                </div>

	                                                <div class="form-group">
	                                                    <label for="slide5_link">Slide 5 <small>(button link)</small></label> <span class="text-red" style="font-size: 20px;">*</span>
	                                                    <input type="text" class="form-control" name="slide5_link" id="slide5_link" placeholder="Slide 5 Button Link" value="#slide5_link#" />
	                                                </div>

												</div><!--- /.col --->

	                                        </div><!--- /.row --->

	                                    </div><!--- /.box-body --->

	                                    <div class="box-footer">
	                                        <button type="submit" class="btn btn-primary">UPDATE Slideshow</button>
	                                    </div><!--- /.box-footer --->

	                             	</div><!--- /.box --->

	                            </div><!--- /.col --->

							</CFOUTPUT>

                         </form>

                    </div><!--- /.row --->

                </section><!--- /.content --->

            </div><!--- /.content-wrapper --->


            <CFINCLUDE TEMPLATE="../_includes/_footer.cfm" />

            <CFINCLUDE TEMPLATE="../_includes/_settings.cfm" />

    	</div><!--- ./wrapper --->

        <CFINCLUDE TEMPLATE="../_includes/_head_js.cfm" />

		<script type="text/javascript" language="javascript">
			$(document).ready(function() {
				/* Form validation */
				$("#myform").validate({
					rules: {
						slide1_large: {maxlength: 30},
						slide2_large: {maxlength: 30},
						slide3_large: {maxlength: 30},
						slide4_large: {maxlength: 30},
						slide5_large: {maxlength: 30},
						slide1_medium: {maxlength: 40},
						slide2_medium: {maxlength: 40},
						slide3_medium: {maxlength: 40},
						slide4_medium: {maxlength: 40},
						slide5_medium: {maxlength: 40}
					},
					messages: {
						slide1_large: "Only 30 Characters Allowed",
						slide2_large: "Only 30 Characters Allowed",
						slide3_large: "Only 30 Characters Allowed",
						slide4_large: "Only 30 Characters Allowed",
						slide5_large: "Only 30 Characters Allowed",
						slide1_medium: "Only 40 Characters Allowed",
						slide2_medium: "Only 40 Characters Allowed",
						slide3_medium: "Only 40 Characters Allowed",
						slide4_medium: "Only 40 Characters Allowed",
						slide5_medium: "Only 40 Characters Allowed"
					}
				});
			});
		</script>

		<CFINCLUDE TEMPLATE="../_includes/_toast.cfm" />

    </body>

</html>
