
<!--- Authentication of user --->
<CFINCLUDE TEMPLATE="../authenticate.cfm">

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

  	<head>

		<CFINCLUDE TEMPLATE="../_includes/_head_meta.cfm" />

        <CFINCLUDE TEMPLATE="../_includes/_head_css.cfm" />

        <CFINCLUDE TEMPLATE="../_includes/_head_shim.cfm" />

        <!--- Menu Bookmark Variable --->
        <CFSET menupage = 'manageslideshow' />

	</head>


     <!--- Logged in user details --->
    <CFSET loginObj = CreateObject("component","birkhillincludes.login") />
	<CFSCRIPT>
        loginObj.dsn = dsn;
		loginObj.username = cookie.myadmin_username;
		loginObj.password = cookie.myadmin_password;
        logindetails = loginObj.getLoginDetails();
    </CFSCRIPT>

    <!--- Send form details to the database --->
    <CFSET slideshowObj = CreateObject("component","birkhillincludes.slideshow") />
	<CFSCRIPT>
        slideshowObj.dsn = dsn;
        slideshowObj.logged_user = cookie.myadmin_username;
        slideshowObj.slideshow_UUID = form.slideshow_UUID;
        slideshowObj.slide1_large = form.slide1_large;
        slideshowObj.slide1_medium = form.slide1_medium;
        slideshowObj.slide1_link = form.slide1_link;
        slideshowObj.slide2_large = form.slide2_large;
        slideshowObj.slide2_medium = form.slide2_medium;
        slideshowObj.slide2_link = form.slide2_link;
        slideshowObj.slide3_large = form.slide3_large;
        slideshowObj.slide3_medium = form.slide3_medium;
        slideshowObj.slide3_link = form.slide3_link;
        slideshowObj.slide4_large = form.slide4_large;
        slideshowObj.slide4_medium = form.slide4_medium;
        slideshowObj.slide4_link = form.slide4_link;
        slideshowObj.slide5_large = form.slide5_large;
        slideshowObj.slide5_medium = form.slide5_medium;
        slideshowObj.slide5_link = form.slide5_link;
        myresult = slideshowObj.slideshowProcess();
    </CFSCRIPT>


	<body class="hold-transition sidebar-mini skin-blue">

    	<div class="wrapper">

        	<CFINCLUDE TEMPLATE="../_includes/_header.cfm" />

            <CFINCLUDE TEMPLATE="../_includes/_menu.cfm" />

            <!--- Content Wrapper. Contains page content --->
      		<div class="content-wrapper">

				<CFSET locationurl = "./slideshow.cfm?toastcode=" & "#myresult.toastCode#" & "&toasttitle=" & "#myresult.toastTitle#" & "&toastmessage=" & "#myresult.toastMessage#" />

                <CFLOCATION URL="#locationurl#" />

            </div><!--- /.content-wrapper --->


            <CFINCLUDE TEMPLATE="../_includes/_footer.cfm" />

            <CFINCLUDE TEMPLATE="../_includes/_settings.cfm" />

    	</div><!--- ./wrapper --->

        <CFINCLUDE TEMPLATE="../_includes/_head_js.cfm" />

    </body>

</html>
