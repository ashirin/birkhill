
<!--- Authentication of user --->
<CFIF #IsDefined('cookie.remember_username')# AND #IsDefined('cookie.remember_password')#>

    <CFLOCATION URL="./dashboard/dashboard.cfm" ADDTOKEN="No">

<CFELSE>

	<!--- Wipe cookies to begin application with a clean slate --->
	<CFCOOKIE NAME="remember_username" VALUE="-99" EXPIRES="NOW">
  <CFCOOKIE NAME="remember_password" VALUE="-99" EXPIRES="NOW">
  <CFCOOKIE NAME="myadmin_username" VALUE="-99" EXPIRES="NOW">
  <CFCOOKIE NAME="myadmin_password" VALUE="-99" EXPIRES="NOW">

</CFIF>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

  	<head>

  		<CFINCLUDE TEMPLATE="./_includes/_head_meta.cfm" />

      <CFINCLUDE TEMPLATE="./_includes/_head_css.cfm" />

      <CFINCLUDE TEMPLATE="./_includes/_head_shim.cfm" />

	</head>


  	<body class="hold-transition login-page">

        <div class="login-box">

          	<div class="login-logo">

              <img src="<CFOUTPUT>#request.baseurl#</CFOUTPUT>_img/logo.png" width="300px"/>

            </div><!--- /.login-logo --->

            <div class="login-box-body">

                <p class="login-box-msg">Sign in to start your session<br />&nbsp;</p>

                <form action="<CFOUTPUT>#request.baseurl#</CFOUTPUT>dashboard/dashboard.cfm" method="post">

                	<div class="form-group has-feedback">
                    	<input type="email" class="form-control" placeholder="Email" name="admin_username">
                    	<i class="fa fa-envelope form-control-feedback"></i>
                  	</div>

                  	<div class="form-group has-feedback">
                    	<input type="password" class="form-control" placeholder="Password" name="admin_password">
                    	<i class="fa fa-lock form-control-feedback"></i>
                  	</div>

                    <!--- Remember me checkbox --->
                  	<div class="row">
                    	<div class="col-xs-8">
                      		<div class="checkbox icheck">
                        		<label>
                          			<input type="checkbox" name="remember"> Remember Me
                        		</label>
                      		</div>
                    	</div><!--- /.col --->
                        <div class="col-xs-4">
                            <button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
                        </div><!--- /.col --->
                    </div><!--- /.row --->

                </form>

                <br />

                <!--- Forgot password link --->
                <a href="<CFOUTPUT>#request.baseurl#</CFOUTPUT>forgot/forgot_password.cfm">I forgot my password</a><br />

          	</div><!--- /.login-box-body --->

        </div><!--- /.login-box --->

        <CFINCLUDE TEMPLATE="./_includes/_head_js.cfm" />

    	<!--- Required for the remember me checkbox styling --->
		<script type="text/javascript" language="javascript">
            $(document).ready(function() {
                /* Checkbox initialization */
                $('input').iCheck({
                    checkboxClass: 'icheckbox_square-blue',
                    radioClass: 'iradio_square-blue',
                    increaseArea: '20%' // optional
                });
            });
        </script>

        <CFINCLUDE TEMPLATE="./_includes/_toast.cfm" />

  	</body>

</html>
