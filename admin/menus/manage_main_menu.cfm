
<!--- Authentication of user --->
<CFINCLUDE TEMPLATE="../authenticate.cfm">

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

  	<head>

		<CFINCLUDE TEMPLATE="../_includes/_head_meta.cfm" />

        <CFINCLUDE TEMPLATE="../_includes/_head_css.cfm" />

		<!--- Slightly changing appearance of Select2 dropdowns used inside of a datatable --->
		<link type="text/css" rel="stylesheet" href="<CFOUTPUT>#request.baseurl#</CFOUTPUT>_css/table_select2.css">

        <style>
        	.pagination > .active > a, .pagination > .active > a:focus, .pagination > .active > a:hover, .pagination > .active > span, .pagination > .active > span:focus, .pagination > .active > span:hover {
				z-index: 2;
				color: #FFF;
				cursor: default;
				background-color: #337AB7;
				border-color: #337AB7;
			}
        </style>

        <CFINCLUDE TEMPLATE="../_includes/_head_shim.cfm" />

        <!--- Menu Bookmark Variable --->
        <CFSET menupage = 'mainmenu' />

	</head>


    <!--- Logged in user details --->
    <CFSET loginObj = CreateObject("component","birkhillincludes.login") />
	<CFSCRIPT>
        loginObj.dsn = dsn;
		loginObj.username = cookie.myadmin_username;
		loginObj.password = cookie.myadmin_password;
        logindetails = loginObj.getLoginDetails();
    </CFSCRIPT>

    <!--- Menu Details --->
    <CFSET menusObj = CreateObject("component","birkhillincludes.menus") />
	<CFSCRIPT>
        menusObj.dsn = dsn;
        menusObj.parent_menu_UUID = url.parent_menu_UUID;
        menudetails = menusObj.getMenuDetails();
        menuorder = menusObj.getMenuDetails();
        menulevel = menusObj.getMenuLevel();
    </CFSCRIPT>


    <body class="hold-transition sidebar-mini skin-blue">

    	<div class="wrapper">

        	<CFINCLUDE TEMPLATE="../_includes/_header.cfm" />

            <CFINCLUDE TEMPLATE="../_includes/_menu.cfm" />


            <!--- Content Wrapper. Contains page content --->
      		<div class="content-wrapper">

            	<section class="content-header">
                  	<h1>
                    	MANAGE Main Menu
                        &nbsp;&nbsp;
                        <span class="label label-primary">
							<CFIF #url.parent_menu_UUID# EQ 'top'>
								Top Level
							<CFELSE>
								Parent Level: <CFOUTPUT>#menulevel.menu_name#</CFOUTPUT>
							</CFIF>
						</span>
                    </h1>
                	<ol class="breadcrumb">
                    	<li><a href="<CFOUTPUT>#request.baseurl#</CFOUTPUT>dashboard/dashboard.cfm"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                        <li class="active"><i class="fa fa-reorder"></i> Manage Main Menu</li>
                	</ol>
                </section><!--- /.content-header --->

                <section class="content">

                    <div class="row">

                        <div class="col-md-12">

                            <div class="box box-primary">

                                <div class="box-body">

                                	<table id="myTable" class="table table-bordered table-hover">
                                        <thead>
                                            <tr>
                                                <th>Menu Name</th>
                                                <th>Linked Page</th>
                                                <th>Order</th>
                                                <th>Last Modified</th>
                                                <th>Actions</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <CFOUTPUT QUERY="menudetails">
                                            <tr>
												<input type="hidden" name="menu_UUID" id="menu_UUID" value="#menu_UUID#" />
                                                <td>#menu_name#</td>
                                                <td>#page_name#</td>
                                                <td>
													<select class="select2" name="menu_order" id="menu_order" style="width: 100%;">
                                                        <CFLOOP QUERY="menuorder">
															<option value="#menu_order#" <CFIF menudetails.menu_order EQ menu_order>selected</CFIF>>#menu_order#</option>
                                                    	</CFLOOP>
													</select>
												</td>
                                                <td>#DateFormat(last_modified, 'dd/mm/yyyy')#</td>
                                                <td>
													<a class="btn btn-xs btn-primary" href="#request.baseurl#menus/main_menu.cfm?parent_menu_UUID=#parent_menu_UUID#&menu_UUID=#menu_UUID#&action=edit">Edit</a>
													&nbsp;
													<a class="btn btn-xs btn-danger" href="#request.baseurl#menus/main_menu.cfm?parent_menu_UUID=#parent_menu_UUID#&menu_UUID=#menu_UUID#&action=delete">Delete</a>
													&nbsp;
													<a class="btn btn-xs btn-warning" href="#request.baseurl#menus/manage_main_menu.cfm?parent_menu_UUID=#menu_UUID#">Manage Sub-Menus (#sub_menus#)</a>
                                                </td>
                                            </tr>
                                            </CFOUTPUT>
                                        </tbody>
                                    </table>

                            	</div><!--- /.box-body --->

								<div class="box-footer">
									<CFOUTPUT>
                                    	<a class="btn btn-success" href="#request.baseurl#menus/main_menu.cfm?action=add&parent_menu_UUID=<CFIF #IsDefined('menulevel.menu_UUID')# AND Len(menulevel.menu_UUID) GT 0>#menulevel.menu_UUID#<CFELSE>top</CFIF>"><strong>ADD Page</strong></a>
                                		<CFIF #url.parent_menu_UUID# NEQ 'top'>
											&nbsp;
											<a class="btn btn-primary" href="#request.baseurl#menus/manage_main_menu.cfm?action=add&parent_menu_UUID=<CFIF #IsDefined('menulevel.previous_UUID')# AND Len(menulevel.previous_UUID) GT 0>#menulevel.previous_UUID#<CFELSE>top</CFIF>"><strong>Go to #menulevel.previous_name#</strong></a>
										</CFIF>
									</CFOUTPUT>
								</div><!--- /.box-footer --->

                            </div><!--- /.box --->

                        </div><!--- /.col --->

                    </div><!--- /.row --->

                </section><!--- /.content --->

            </div> <!--- /.content-wrapper --->


            <CFINCLUDE TEMPLATE="../_includes/_footer.cfm" />

            <CFINCLUDE TEMPLATE="../_includes/_settings.cfm" />

    	</div><!--- ./wrapper --->

        <CFINCLUDE TEMPLATE="../_includes/_head_js.cfm" />

        <script type="text/javascript" language="javascript">
			$(document).ready(function() {
    			/* Initializing datatables */
				$('#myTable').dataTable( {
					"bLengthChange": false,
					"info": false,
					"aaSorting": [[ 2, "desc" ]]
				});

				/* Select2 initilizations */
				$(".select2").select2( {
					minimumResultsForSearch: Infinity
				});

				$('.select2').change(function () {
					$.ajax({
						url: "../_cfcs/menus.cfc?method=updateMenus",
						data: {
							dsn: "<CFOUTPUT>#dsn#</CFOUTPUT>",
							menu_UUID: $(this).parent().parent().find('#menu_UUID').val(),
							menu_order: $(this).val(),
							logged_user: "<CFOUTPUT>#cookie.myadmin_username#</CFOUTPUT>"
						},
						success: function(result) {
							location.reload(true);
						}
					});
				});
			});
		</script>

        <CFINCLUDE TEMPLATE="../_includes/_toast.cfm" />

    </body>

</html>
