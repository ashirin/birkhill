
<!--- Authentication of user --->
<CFINCLUDE TEMPLATE="../authenticate.cfm">

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

  	<head>

		<CFINCLUDE TEMPLATE="../_includes/_head_meta.cfm" />

        <CFINCLUDE TEMPLATE="../_includes/_head_css.cfm" />

        <CFINCLUDE TEMPLATE="../_includes/_head_shim.cfm" />

        <!--- Menu Bookmark Variable --->
        <CFSET menupage = 'mainmenu' />

	</head>


     <!--- Logged in user details --->
    <CFSET loginObj = CreateObject("component","birkhillincludes.login") />
	<CFSCRIPT>
        loginObj.dsn = dsn;
		loginObj.username = cookie.myadmin_username;
		loginObj.password = cookie.myadmin_password;
        logindetails = loginObj.getLoginDetails();
    </CFSCRIPT>

    <!--- Send form details to the database --->
    <CFSET menusObj = CreateObject("component","birkhillincludes.menus") />
	<CFSCRIPT>
        menusObj.dsn = dsn;
        menusObj.action = form.action;
        menusObj.menu_UUID = form.menu_UUID;
        menusObj.parent_menu_UUID = form.parent_menu_UUID;
        menusObj.logged_user = cookie.myadmin_username;
		if (form.action != 'delete') {
            menusObj.menu_name = form.menu_name;
            menusObj.menu_type = 'main';
            menusObj.menu_order = form.menu_order;
            menusObj.page_UUID = form.page_UUID;
        }
        myresult = menusObj.menuProcess();
    </CFSCRIPT>


	<body class="hold-transition sidebar-mini skin-blue">

    	<div class="wrapper">

        	<CFINCLUDE TEMPLATE="../_includes/_header.cfm" />

            <CFINCLUDE TEMPLATE="../_includes/_menu.cfm" />

            <!--- Content Wrapper. Contains page content --->
      		<div class="content-wrapper">

				<CFSET locationurl = "./manage_main_menu.cfm?parent_menu_UUID=#myresult.parent_menu_UUID#&toastcode=#myresult.toastCode#&toasttitle=#myresult.toastTitle#&toastmessage=#myresult.toastMessage#" />

                <CFLOCATION URL="#locationurl#" />

            </div><!--- /.content-wrapper --->


            <CFINCLUDE TEMPLATE="../_includes/_footer.cfm" />

            <CFINCLUDE TEMPLATE="../_includes/_settings.cfm" />

    	</div><!--- ./wrapper --->

        <CFINCLUDE TEMPLATE="../_includes/_head_js.cfm" />

    </body>

</html>
