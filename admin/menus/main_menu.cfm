
<!--- Authentication of user --->
<CFINCLUDE TEMPLATE="../authenticate.cfm">

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

  	<head>

		<CFINCLUDE TEMPLATE="../_includes/_head_meta.cfm" />

        <CFINCLUDE TEMPLATE="../_includes/_head_css.cfm" />

        <style>
			label.error {
				margin-top: 10px;
				color: #DD4B39;
			}
		</style>

        <CFINCLUDE TEMPLATE="../_includes/_head_shim.cfm" />

        <!--- Menu Bookmark Variable --->
        <CFIF #url.action# EQ 'undelete'>
        	<CFSET menupage = 'managedeletedusers' />
       	<CFELSE>
        	<CFSET menupage = 'mainmenu' />
		</CFIF>

	</head>


    <!--- Logged in user details --->
    <CFSET loginObj = CreateObject("component","birkhillincludes.login") />
	<CFSCRIPT>
        loginObj.dsn = dsn;
		loginObj.username = cookie.myadmin_username;
		loginObj.password = cookie.myadmin_password;
        logindetails = loginObj.getLoginDetails();
    </CFSCRIPT>

	<CFSET menusObj = CreateObject("component","birkhillincludes.menus") />
	<CFSCRIPT>
		menusObj.dsn = dsn;
	</CFSCRIPT>

    <!--- Get menu details if this is not an add --->
	<CFIF #url.action# NEQ 'add'>
		<CFSCRIPT>
			menusObj.menu_UUID = url.menu_UUID;
			getmenu = menusObj.getMenuDetails();
		</CFSCRIPT>

		<!--- Get parent menu details if they exist --->
		<CFIF #getmenu.parent_menu_UUID# NEQ 'top'>
			<CFSCRIPT>
				menusObj.check_parent = 'yes';
				menusObj.parent_menu_UUID = getmenu.parent_menu_UUID;
				getparentmenu = menusObj.getMenuDetails();
			</CFSCRIPT>
		</CFIF>
	<CFELSE>
		<CFSCRIPT>
			menusObj.parent_menu_UUID = url.parent_menu_UUID;
		</CFSCRIPT>

		<!--- Get parent menu details if they are passed in --->
		<CFIF #IsDefined('url.parent_menu_UUID')# AND url.parent_menu_UUID NEQ 'top'>
			<CFSCRIPT>
				menusObj.check_parent = 'yes';
				getparentmenu = menusObj.getMenuDetails();
			</CFSCRIPT>
		</CFIF>
	</CFIF>

	<!--- Get menu order --->
	<CFSCRIPT>
		menusObj.check_parent = 'no';
		menusObj.menu_UUID = '';
		menusObj.parent_menu_UUID = url.parent_menu_UUID;
		menuorder = menusObj.getMenuDetails();
	</CFSCRIPT>

	<!--- Get linked pages --->
	<CFIF #IsDefined('url.menu_UUID')#>
		<CFSCRIPT>
			menusObj.menu_UUID = url.menu_UUID;
			linkedpages = menusObj.getLinkedPages();
		</CFSCRIPT>
	<CFELSE>
		<CFSCRIPT>
			linkedpages = menusObj.getLinkedPages();
		</CFSCRIPT>
	</CFIF>


    <body class="hold-transition sidebar-mini skin-blue">

    	<div class="wrapper">

        	<CFINCLUDE TEMPLATE="../_includes/_header.cfm" />

            <CFINCLUDE TEMPLATE="../_includes/_menu.cfm" />


            <!--- Content Wrapper. Contains page content --->
      		<div class="content-wrapper">

            	<section class="content-header">
                  	<h1>
                    	<CFOUTPUT>#UCASE(url.action)#</CFOUTPUT> Menu Item
                  	</h1>
                    <ol class="breadcrumb">
                    	<li><a href="<CFOUTPUT>#request.baseurl#</CFOUTPUT>dashboard/dashboard.cfm"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                        <li><a href="<CFOUTPUT>#request.baseurl#</CFOUTPUT>menus/manage_main_menu.cfm"><i class="fa fa-reorder"></i> Manage Menus</a></li>
						<CFIF #url.action# EQ 'add'>
                            <li class="active"><i class="fa fa-reorder"></i> Add Menu</li>
                        <CFELSEIF #url.action# EQ 'edit'>
                            <li class="active"><i class="fa fa-reorder"></i> Edit Menu</li>
                        <CFELSEIF #url.action# EQ 'delete'>
                            <li class="active"><i class="fa fa-reorder"></i> Delete Menu</li>
                        </CFIF>
                	</ol>
                </section>

                <section class="content">

                    <div class="row">

                        <form role="form" method="post" action="<CFOUTPUT>#request.baseurl#</CFOUTPUT>menus/main_menu_process.cfm" id="myform" name="myform">

                            <CFOUTPUT>
                                <input type="hidden" name="action" value="#url.action#">
                                <input type="hidden" name="menu_UUID" value="<CFIF #url.action# NEQ 'add'>#menu_UUID#</CFIF>">
								<input type="hidden" name="parent_menu_UUID" value="<CFIF #IsDefined('parent_menu_UUID')#>#parent_menu_UUID#</CFIF>">
                            </CFOUTPUT>

                            <div class="col-md-12">

                                <div class="box <CFIF #url.action# EQ "delete"><CFOUTPUT>box-danger</CFOUTPUT><CFELSE><CFOUTPUT>box-primary</CFOUTPUT></CFIF>">

                                	<div class="box-header with-border">

                                    	<p>Fields marked with an <span class="text-red" style="font-size: 20px;">*</span> must be filled in</p>

                                    </div>

                                    <div class="box-body">

                                        <div class="row">

                                            <div class="col-md-6">

                                                <div class="form-group">
                                                    <label for="menu_name">Menu Name</label> <span class="text-red" style="font-size: 20px;">*</span>
                                                    <input type="text" class="form-control" name="menu_name" id="menu_name" placeholder="Menu Name" <CFIF #url.action# NEQ "add"><CFOUTPUT QUERY="getmenu">value="#menu_name#"</CFOUTPUT></CFIF> />
                                                </div>

                                                <div class="form-group">
                                                    <label>Menu Order</label> <span style="font-size: 20px;">&nbsp;</span>
                                                    <select class="form-control select2" name="menu_order" id="menu_order" style="width: 100%;">
                                                        <option value="" <CFIF action EQ 'add'>selected</CFIF>>...End</option>
                                                        <CFOUTPUT QUERY="menuorder">
															<option value="#menu_order#" <CFIF action NEQ 'add'><CFIF getmenu.menu_order EQ menu_order>selected</CFIF></CFIF>>#menu_order#</option>
														</CFOUTPUT>
                                                    </select>
                                                </div>

                                            </div><!--- /.col --->

                                            <div class="col-md-6">

												<div class="form-group"> <span style="font-size: 20px;">&nbsp;</span>
                                                    <label for="menu_parent">Parent Level</label>
                                                    <input type="text" class="form-control" name="menu_parent" id="menu_parent" placeholder="None - Top Level" <CFOUTPUT><CFIF #IsDefined('getparentmenu.menu_name')#>value="#getparentmenu.menu_name#"</CFIF></CFOUTPUT> disabled />
                                                </div>

												<div class="form-group">
                                                    <label>Linked Page</label> <span style="font-size: 20px;">&nbsp;</span>
                                                    <select class="form-control select2-linked" name="page_UUID" id="page_UUID" style="width: 100%;">
														<option value="" <CFIF action EQ 'add'>selected</CFIF>>&nbsp;</option>
														<CFOUTPUT QUERY="linkedpages">
                                                            <option value="#page_UUID#" <CFIF action NEQ 'add'><CFIF getmenu.page_UUID EQ page_UUID>selected</CFIF></CFIF>>#page_name#</option>
                                                        </CFOUTPUT>
                                                    </select>
                                                </div>

                                            </div><!--- /.col --->

                                        </div><!--- /.row --->

                                    </div><!--- /.box-body --->

                                    <div class="box-footer">
                                        <button type="submit" class="btn <CFIF #url.action# NEQ "delete"><CFOUTPUT>btn-primary</CFOUTPUT><CFELSE><CFOUTPUT>btn-danger</CFOUTPUT></CFIF>"><CFOUTPUT>#UCase(url.action)# Menu Item</CFOUTPUT></button>
                                    </div><!--- /.box-footer --->

                             	</div><!--- /.box --->

                            </div><!--- /.col --->

                         </form>

                    </div><!--- /.row --->

                </section><!--- /.content --->

            </div><!--- /.content-wrapper --->


            <CFINCLUDE TEMPLATE="../_includes/_footer.cfm" />

            <CFINCLUDE TEMPLATE="../_includes/_settings.cfm" />

    	</div><!--- ./wrapper --->

        <CFINCLUDE TEMPLATE="../_includes/_head_js.cfm" />

        <script type="text/javascript" language="javascript">
			$(document).ready(function() {
				/* Form validation */
				$("#myform").validate({
					rules: {
						menu_name: "required"
					},
					messages: {
						menu_name: "Please enter a menu name"
					}
				});

				/* Select2 initilizations */
				$(".select2").select2( {
					minimumResultsForSearch: Infinity
				});

				$(".select2-linked").select2( {
					minimumResultsForSearch: Infinity,
					placeholder: "Select a page..."
				});
			});
		</script>

        <!--- Disabled fields if deleting content --->
        <CFIF #url.action# EQ "delete">
			<script type="text/javascript" language="javascript">
				$(document).ready(function() {
					$('#menu_name').prop('disabled', true);
					$('#menu_order').prop('disabled', true);
					$('#page_UUID').prop('disabled', true);
					$('.select2-selection--single').css('cursor', 'not-allowed');
				});
            </script>
        </CFIF>

		<CFINCLUDE TEMPLATE="../_includes/_toast.cfm" />

    </body>

</html>
