
<!--- jQuery 2.1.4 --->
<script type="text/javascript" language="javascript" src="<CFOUTPUT>#request.baseurl#</CFOUTPUT>_plugins/jQuery/jQuery-2.1.4.min.js"></script>

<!--- Bootstrap 3.3.5 --->
<script type="text/javascript" language="javascript" src="<CFOUTPUT>#request.baseurl#</CFOUTPUT>_bootstrap/js/bootstrap.min.js"></script>

<!--- jQuery UI --->
<script type="text/javascript" language="javascript" src="<CFOUTPUT>#request.baseurl#</CFOUTPUT>_plugins/jQueryUI/jquery-ui.js"></script>

<!--- FastClick --->
<script type="text/javascript" language="javascript" src="<CFOUTPUT>#request.baseurl#</CFOUTPUT>_plugins/fastclick/fastclick.min.js"></script>

<!--- AdminLTE App --->
<script type="text/javascript" language="javascript" src="<CFOUTPUT>#request.baseurl#</CFOUTPUT>_js/app.min.js"></script>

<!--- Skin Select --->
<script type="text/javascript" language="javascript" src="<CFOUTPUT>#request.baseurl#</CFOUTPUT>_js/skin.select.js"></script>


<!--- Sparkline --->
<script type="text/javascript" language="javascript" src="<CFOUTPUT>#request.baseurl#</CFOUTPUT>_plugins/sparkline/jquery.sparkline.min.js"></script>

<!--- jvectormap --->
<script type="text/javascript" language="javascript" src="<CFOUTPUT>#request.baseurl#</CFOUTPUT>_plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script type="text/javascript" language="javascript" src="<CFOUTPUT>#request.baseurl#</CFOUTPUT>_plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>

<!--- SlimScroll 1.3.0 --->
<script type="text/javascript" language="javascript" src="<CFOUTPUT>#request.baseurl#</CFOUTPUT>_plugins/slimScroll/jquery.slimscroll.min.js"></script>

<!--- ChartJS 1.0.1 --->
<script type="text/javascript" language="javascript" src="<CFOUTPUT>#request.baseurl#</CFOUTPUT>_plugins/chartjs/Chart.min.js"></script>

<!--- iCheck --->
<script type="text/javascript" language="javascript" src="<CFOUTPUT>#request.baseurl#</CFOUTPUT>_plugins/iCheck/icheck.min.js"></script>

<!--- DataTables --->
<script type="text/javascript" language="javascript" src="<CFOUTPUT>#request.baseurl#</CFOUTPUT>_plugins/datatables/jquery.dataTables.min.js"></script>
<script type="text/javascript" language="javascript" src="<CFOUTPUT>#request.baseurl#</CFOUTPUT>_plugins/datatables/dataTables.bootstrap.min.js"></script>

<!--- Select2 --->
<script type="text/javascript" language="javascript" src="<CFOUTPUT>#request.baseurl#</CFOUTPUT>_plugins/select2/select2.full.min.js"></script>

<!--- Toastr --->
<script type="text/javascript" language="javascript" src="<CFOUTPUT>#request.baseurl#</CFOUTPUT>_plugins/toastr/toastr.min.js"></script>

<!--- Validate --->
<script type="text/javascript" language="javascript" src="<CFOUTPUT>#request.baseurl#</CFOUTPUT>_plugins/validate/dist/jquery.validate.js"></script>

<!--- Google Maps Library --->
<script type="text/javascript" language="javascript" src="http://maps.google.com/maps/api/js?sensor=false&amp;language=en"></script>

<!--- gMap3 --->
<script type="text/javascript" language="javascript" src="<CFOUTPUT>#request.baseurl#</CFOUTPUT>_plugins/gmap3/gmap3.min.js"></script>

<!--- fullCalendar 2.2.5 --->
<script type="text/javascript" language="javascript" src="<CFOUTPUT>#request.baseurl#</CFOUTPUT>_js/moment.js"></script>
<script type="text/javascript" language="javascript" src="<CFOUTPUT>#request.baseurl#</CFOUTPUT>_plugins/fullcalendar/fullcalendar.min.js"></script>

<!--- InputMask --->
<script type="text/javascript" language="javascript" src="<CFOUTPUT>#request.baseurl#</CFOUTPUT>_plugins/input-mask/jquery.inputmask.js"></script>
<script type="text/javascript" language="javascript" src="<CFOUTPUT>#request.baseurl#</CFOUTPUT>_plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script type="text/javascript" language="javascript" src="<CFOUTPUT>#request.baseurl#</CFOUTPUT>_plugins/input-mask/jquery.inputmask.extensions.js"></script>

<!--- CKEditor --->
<script type="text/javascript" language="javascript" src="<CFOUTPUT>#request.baseurl#</CFOUTPUT>_plugins/ckeditor/ckeditor.js"></script>