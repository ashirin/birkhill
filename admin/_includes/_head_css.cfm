
<!--- Bootstrap 3.3.5 --->
<link type="text/css" rel="stylesheet" href="<CFOUTPUT>#request.baseurl#</CFOUTPUT>_bootstrap/css/bootstrap.min.css">

<!--- Font Awesome --->
<link type="text/css" rel="stylesheet" href="<CFOUTPUT>#request.baseurl#</CFOUTPUT>_css/font-awesome.min.css">

<!--- Ionicons --->
<link type="text/css" rel="stylesheet" href="<CFOUTPUT>#request.baseurl#</CFOUTPUT>_css/ionicons.min.css">

<!--- jvectormap --->
<link type="text/css" rel="stylesheet" href="<CFOUTPUT>#request.baseurl#</CFOUTPUT>_plugins/jvectormap/jquery-jvectormap-1.2.2.css">

<!--- iCheck --->
<link type="text/css" rel="stylesheet" href="<CFOUTPUT>#request.baseurl#</CFOUTPUT>_plugins/iCheck/all.css">

<!--- DataTables --->
<link type="text/css" rel="stylesheet" href="<CFOUTPUT>#request.baseurl#</CFOUTPUT>_plugins/datatables/dataTables.bootstrap.css">

<!--- Select2 --->
<link type="text/css" rel="stylesheet" href="<CFOUTPUT>#request.baseurl#</CFOUTPUT>_plugins/select2/select2.min.css">

<!--- Toastr --->
<link type="text/css" rel="stylesheet" href="<CFOUTPUT>#request.baseurl#</CFOUTPUT>_plugins/toastr/toastr.min.css">

<!--- fullCalendar 2.2.5 --->
<link type="text/css" rel="stylesheet" href="<CFOUTPUT>#request.baseurl#</CFOUTPUT>_plugins/fullcalendar/fullcalendar.min.css">
<link type="text/css" rel="stylesheet" href="<CFOUTPUT>#request.baseurl#</CFOUTPUT>_plugins/fullcalendar/fullcalendar.print.css" media="print">


<!--- Theme style --->
<link type="text/css" rel="stylesheet" href="<CFOUTPUT>#request.baseurl#</CFOUTPUT>_css/AdminLTE.min.css">

<!--- AdminLTE Skins. Choose a skin from the css/skins folder instead of downloading all of them to reduce the load. --->
<link type="text/css" rel="stylesheet" href="<CFOUTPUT>#request.baseurl#</CFOUTPUT>_css/skins/_all-skins.min.css">

<!--- AdminLTE Skins. Choose a skin from the css/skins folder instead of downloading all of them to reduce the load. --->
<link type="text/css" rel="stylesheet" href="<CFOUTPUT>#request.baseurl#</CFOUTPUT>_css/search.css">

