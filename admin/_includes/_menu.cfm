
<!--- User Numbers --->
<CFSET usersObj = CreateObject("component","birkhillincludes.users") />
<CFIF #logindetails.user_role_UUID# NEQ 'a88aaf23-0d3f-11e5-a315-38b1dbe2bc0e'>
	<CFSCRIPT>
        usersObj.username_hash = logindetails.username_hash;
    </CFSCRIPT>
</CFIF>
<CFSCRIPT>
	usersObj.dsn = dsn;
	usersnumber = usersObj.getUsersNumber();
	deletedusersnumber = usersObj.getDeletedUsersNumber();
</CFSCRIPT>


<!--- Left side column. contains the logo and sidebar --->
<aside class="main-sidebar">

	<!--- sidebar: style can be found in sidebar.less --->
	<section class="sidebar">

  		<!--- Sidebar user panel --->
  		<div class="user-panel">

    		<div class="pull-left image">
      			<!--- Insert logged in user avatar or default to generic avatar --->
				<CFIF #logindetails.uploadfile1# NEQ ''>
                    <CFOUTPUT QUERY = 'logindetails'>
                        <img src="<CFOUTPUT>#request.baseurl#</CFOUTPUT>_img/avatars/#user_details_UUID#/#uploadfile1#" class="img-circle" alt="User Image" />
                    </CFOUTPUT>
                <CFELSE>
                    <img src="<CFOUTPUT>#request.baseurl#</CFOUTPUT>_img/generic_avatar.png" class="img-circle" alt="User Image" />
                </CFIF>
    		</div>

    		<div class="pull-left info">
      			<p><CFOUTPUT QUERY="logindetails"><CFIF #IsDefined('logindetails.user_firstname')#>#user_firstname# #user_lastname#<CFELSE>#email_name#</CFIF></CFOUTPUT></p>
      			<CFOUTPUT QUERY="logindetails">#user_role#</CFOUTPUT>
    		</div>

  		</div>

  		<!--- sidebar menu: : style can be found in sidebar.less --->
  		<ul class="sidebar-menu">

			<CFOUTPUT>

	    		<li class="header">MAIN NAVIGATION</li>

	            <li <CFIF #menupage# EQ 'dashboard'>class="active"</CFIF>><a href="#request.baseurl#dashboard/dashboard.cfm"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a></li>

				<li <CFIF #menupage# EQ 'managepages'>class="active"</CFIF>><a href="#request.baseurl#pages/manage_pages.cfm"><i class="fa fa-file-text"></i> <span>Manage Pages</span></a></li>

				<li <CFIF #menupage# EQ 'mainmenu'>class="treeview active"<CFELSE>class="treeview"</CFIF>>
	                <a href="##">
	                    <i class="fa fa-reorder"></i> <span>Manage Menus</span> <i class="fa fa-angle-left pull-right"></i>
	                </a>
	                <ul class="treeview-menu">
	                    <li <CFIF #menupage# EQ 'mainmenu'>class="active"</CFIF>><a href="#request.baseurl#menus/manage_main_menu.cfm?parent_menu_UUID=top"><i class="fa fa-circle-o"></i> <span>Main Menu</span></a></li>
	                </ul>
	            </li>

				<li <CFIF #menupage# EQ 'manageboxes'>class="active"</CFIF>><a href="#request.baseurl#boxes/manage_boxes.cfm"><i class="fa fa-dropbox"></i> <span>Manage Boxes</span></a></li>


				<li <CFIF #menupage# EQ 'managecarousel'>class="active"</CFIF>><a href="<CFOUTPUT>#request.baseurl#</CFOUTPUT>pages/carousel.cfm"><i class="fa fa-camera"></i> <span>Manage Carousel</span></a></li>

        <li <CFIF #menupage# EQ 'managecircles'>class="active"</CFIF>><a href="<CFOUTPUT>#request.baseurl#</CFOUTPUT>pages/circles.cfm"><i class="fa fa-circle-o"></i> <span>Manage Circles</span></a></li>

        <li <CFIF #menupage# EQ 'managetestimonials'>class="active"</CFIF>><a href="<CFOUTPUT>#request.baseurl#</CFOUTPUT>pages/testimonials.cfm"><i class="fa fa-check"></i> <span>Manage Testimonials</span></a></li>



				<!--- Users Menu Item --->
	            <!--- Only viewable if you are not a user --->
	            <CFIF #logindetails.user_role_UUID# NEQ '87fc3819-0d3f-11e5-a315-38b1dbe2bc0e'>
	                <li <CFIF (#menupage# EQ 'manageusers') OR (#menupage# EQ 'managedeletedusers')>class="treeview active"<CFELSE>class="treeview"</CFIF>>
	                    <a href="##">
	                        <i class="fa fa-user"></i> <span>System Users</span> <i class="fa fa-angle-left pull-right"></i>
	                    </a>
	                    <ul class="treeview-menu">
	                        <li <CFIF #menupage# EQ 'manageusers'>class="active"</CFIF>><a href="#request.baseurl#users/manage_users.cfm"><i class="fa fa-circle-o"></i> <span>Manage Users</span> <span class="label label-info pull-right">#usersnumber.userscount#</span></a></li>
	                        <!--- Only viewable as Super User --->
	                        <CFIF #logindetails.user_role_UUID# EQ 'a88aaf23-0d3f-11e5-a315-38b1dbe2bc0e'>
	                        	<li <CFIF #menupage# EQ 'managedeletedusers'>class="active"</CFIF>><a href="#request.baseurl#users/manage_deleted_users.cfm"><i class="fa fa-circle-o text-danger"></i> <span>Deleted Users</span> <span class="label label-danger pull-right">#deletedusersnumber.deleteduserscount#</span></a></li>
	                    	</CFIF>
	                    </ul>
	                </li>

	             </CFIF>

			</CFOUTPUT>

  		</ul>

	</section><!--- /.sidebar --->

</aside>
