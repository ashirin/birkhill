<CFCOMPONENT HINT="dynamic component">

	<!--- Set THIS scope constructor variables --->
	<CFSET THIS.dsn = "" />
	<CFSET THIS.action = "" />

	<CFSET THIS.menu_UUID = "" />
    <CFSET THIS.parent_menu_UUID = "" />
	<CFSET THIS.menu_name = "" />
	<CFSET THIS.menu_type = "" />
	<CFSET THIS.menu_order = "" />
	<CFSET THIS.page_UUID = "" />
	<CFSET THIS.check_parent = "" />

	<CFSET THIS.logged_user = "" />


	<!--- Get Menu --->
	<CFFUNCTION NAME="getMenu" ACCESS="public" RETURNTYPE="query">

		<CFQUERY NAME="getmenu" DATASOURCE="#THIS.dsn#">

			SELECT *

				FROM web_menus

				WHERE deleted IS NULL

				<CFIF THIS.parent_menu_UUID NEQ "">
					AND parent_menu_UUID = <CFQUERYPARAM VALUE="#THIS.parent_menu_UUID#" CFSQLTYPE="cf_sql_varchar" />
				</CFIF>

				<CFIF THIS.menu_type NEQ "">
					AND menu_type = <CFQUERYPARAM VALUE="#THIS.menu_type#" CFSQLTYPE="cf_sql_varchar" />
				</CFIF>

				ORDER BY menu_order ASC

		</CFQUERY>

		<CFRETURN getmenu />

	</CFFUNCTION>
	<!--- End Get Menu --->

	<!--- Get Menu Details --->
	<CFFUNCTION NAME="getMenuDetails" ACCESS="public" RETURNTYPE="query">

		<CFQUERY NAME="getmenudetails" DATASOURCE="#THIS.dsn#">

			SELECT a.*,
				   CASE WHEN a.modified_date IS NULL THEN a.added_date ELSE a.modified_date END AS last_modified,
				   ISNULL(b.sub_menus, 0) sub_menus

				FROM web_menus a
						LEFT JOIN (SELECT parent_menu_UUID, COUNT(*) AS sub_menus FROM web_menus GROUP BY parent_menu_UUID) b ON a.menu_UUID = b.parent_menu_UUID

				WHERE deleted IS NULL

				<CFIF THIS.menu_UUID NEQ "" AND THIS.check_parent NEQ "yes">
					AND a.menu_UUID = <CFQUERYPARAM VALUE="#THIS.menu_UUID#" CFSQLTYPE="cf_sql_varchar" />
				<CFELSE>
					<CFIF THIS.parent_menu_UUID NEQ "" AND THIS.check_parent NEQ 'yes'>
						AND a.parent_menu_UUID = <CFQUERYPARAM VALUE="#THIS.parent_menu_UUID#" CFSQLTYPE="cf_sql_varchar" />
					<CFELSE>
						AND a.menu_UUID = <CFQUERYPARAM VALUE="#THIS.parent_menu_UUID#" CFSQLTYPE="cf_sql_varchar" />
					</CFIF>
				</CFIF>

				ORDER BY a.menu_order ASC

		</CFQUERY>

		<CFRETURN getmenudetails />

	</CFFUNCTION>
	<!--- End Get Menu Details --->

	<!--- Get Menu Level --->
	<CFFUNCTION NAME="getMenuLevel" ACCESS="public" RETURNTYPE="query">

		<CFQUERY NAME="getmenudetails" DATASOURCE="#THIS.dsn#">

			SELECT a.*,
				   ISNULL(b.previous_UUID, 'top') AS previous_UUID,
				   ISNULL(b.previous_name, 'Top Level') AS previous_name

				FROM web_menus a
						LEFT JOIN (SELECT menu_UUID AS previous_UUID, menu_name AS previous_name FROM web_menus) b ON a.parent_menu_UUID = b.previous_UUID

				WHERE a.deleted IS NULL

				AND a.menu_UUID = <CFQUERYPARAM VALUE="#THIS.parent_menu_UUID#" CFSQLTYPE="cf_sql_varchar" />

		</CFQUERY>

		<CFRETURN getmenudetails />

	</CFFUNCTION>
	<!--- End Get Menu Level --->

	<!--- Get Last Menu --->
	<CFFUNCTION NAME="getLastMenu" ACCESS="public" RETURNTYPE="numeric">

		<CFQUERY NAME="getmenus" DATASOURCE="#THIS.dsn#">

			SELECT COUNT(a.menu_UUID) AS menucount

				FROM (SELECT menu_UUID

						FROM web_menus

						WHERE deleted IS NULL

						AND parent_menu_UUID = <CFQUERYPARAM VALUE="#THIS.parent_menu_UUID#" CFSQLTYPE="cf_sql_varchar" />) a

		</CFQUERY>

		<CFRETURN getmenus.menucount />

	</CFFUNCTION>
	<!--- End Get Last Menu --->

	<!--- Re-Order Menus --->
	<CFFUNCTION NAME="reorderMenus" ACCESS="public">

		<CFQUERY NAME="reorder1" DATASOURCE="#THIS.dsn#">

			SELECT *

				FROM web_menus

				WHERE parent_menu_UUID = <CFQUERYPARAM VALUE="#THIS.parent_menu_UUID#" CFSQLTYPE="cf_sql_varchar" />

				ORDER BY menu_order ASC, modified_date DESC

		</CFQUERY>

		<CFSET my_counter = 1>

		<CFLOOP QUERY="reorder1">

			<CFQUERY NAME="reorder2" DATASOURCE="#THIS.dsn#">

				UPDATE web_menus

					SET menu_order = <CFQUERYPARAM VALUE="#my_counter#" CFSQLTYPE="cf_sql_integer" />

					WHERE menu_UUID = <CFQUERYPARAM VALUE="#reorder1.menu_UUID#" CFSQLTYPE="cf_sql_varchar" />

			</CFQUERY>

			<CFSET my_counter = #my_counter# + 1>

		</CFLOOP>

	</CFFUNCTION>
	<!--- End Re-Order Menus --->

	<!--- Update Menus --->
	<CFFUNCTION NAME="updateMenus" ACCESS="remote">

		<CFARGUMENT NAME="dsn" />
		<CFARGUMENT NAME="menu_UUID" />
		<CFARGUMENT NAME="menu_order" />
		<CFARGUMENT NAME="logged_user" />

		<CFQUERY NAME="update1" DATASOURCE="#dsn#">

			UPDATE web_menus

				SET menu_order = <CFQUERYPARAM VALUE="#menu_order#" CFSQLTYPE="cf_sql_integer" />,
					modified_by = <CFQUERYPARAM VALUE="#logged_user#" CFSQLTYPE="cf_sql_varchar" />,
					modified_date = <CFQUERYPARAM VALUE="#NOW()#" CFSQLTYPE="cf_sql_timestamp" />

				WHERE menu_UUID = <CFQUERYPARAM VALUE="#menu_UUID#" CFSQLTYPE="cf_sql_varchar" />

		</CFQUERY>

		<CFQUERY NAME="update2" DATASOURCE="#dsn#">

			SELECT *

				FROM web_menus

				WHERE menu_UUID = <CFQUERYPARAM VALUE="#menu_UUID#" CFSQLTYPE="cf_sql_varchar" />

		</CFQUERY>

		<CFSET THIS.dsn = dsn />
		<CFSET THIS.parent_menu_UUID = update2.parent_menu_UUID />

		<CFSCRIPT>
			#THIS.reorderMenus()#;
		</CFSCRIPT>

	</CFFUNCTION>
	<!--- End Update Menus --->

	<!--- Menu Process --->
	<CFFUNCTION NAME="menuProcess" ACCESS="public" RETURNTYPE="struct">


		<CFIF THIS.menu_order EQ "">
			<CFSET my_menu_order = getLastMenu() + 1>
		<CFELSE>
			<CFSET my_menu_order = THIS.menu_order>
		</CFIF>

		<CFSET my_page_name = "none" />

		<CFIF #THIS.page_UUID# NEQ "">

			<CFQUERY NAME="menu1" DATASOURCE="#THIS.dsn#">

				SELECT *

					FROM web_pages

					WHERE page_UUID = <CFQUERYPARAM VALUE="#THIS.page_UUID#" CFSQLTYPE="cf_sql_varchar" />

			</CFQUERY>

			<CFSET my_page_name = #menu1.page_name# />

		</CFIF>


		<!--- Add Menu --->
		<CFIF #THIS.action# EQ "add">

			<CFTRY>

				<CFLOCK TIMEOUT="20">

					<CFTRANSACTION>

						<CFSET menu_UUID = #CreateUUID()# />

	 					<CFQUERY NAME="additem1" DATASOURCE="#THIS.dsn#">

							INSERT INTO web_menus (menu_UUID, menu_name, menu_type, menu_order, parent_menu_UUID, page_UUID, page_name, menu_children, added_by, added_date)

								VALUES(
										<CFQUERYPARAM VALUE="#menu_UUID#" CFSQLTYPE="cf_sql_varchar" />,
										<CFQUERYPARAM VALUE="#THIS.menu_name#" CFSQLTYPE="cf_sql_varchar" />,
										<CFQUERYPARAM VALUE="#THIS.menu_type#" CFSQLTYPE="cf_sql_varchar" />,
										<CFQUERYPARAM VALUE="#my_menu_order#" CFSQLTYPE="cf_sql_integer" />,
										<CFQUERYPARAM VALUE="#THIS.parent_menu_UUID#" CFSQLTYPE="cf_sql_varchar" />,
										<CFIF #THIS.page_UUID# NEQ "">
											<CFQUERYPARAM VALUE="#THIS.page_UUID#" CFSQLTYPE="cf_sql_varchar" />
										<CFELSE>
											<CFQUERYPARAM VALUE="" CFSQLTYPE="cf_sql_varchar" NULL="yes"/>
										</CFIF>,
										<CFQUERYPARAM VALUE="#my_page_name#" CFSQLTYPE="cf_sql_varchar" />,
										<CFQUERYPARAM VALUE="0" CFSQLTYPE="cf_sql_integer" />,
										<CFQUERYPARAM VALUE="#THIS.logged_user#" CFSQLTYPE="cf_sql_varchar" />,
										<CFQUERYPARAM VALUE="#NOW()#" CFSQLTYPE="cf_sql_timestamp" />
										)
						</CFQUERY>

						<CFSCRIPT>
							#THIS.reorderMenus()#;
						</CFSCRIPT>

					</CFTRANSACTION>

				</CFLOCK>

				<CFSET resultStruct.parent_menu_UUID = #THIS.parent_menu_UUID# />
				<CFSET resultStruct.toastCode = 1 />
	            <CFSET resultStruct.toastTitle = "Success" />
	            <CFSET resultStruct.toastMessage = "#THIS.menu_name#" & " has been created!" />
	            <CFRETURN resultStruct />

	            <CFCATCH>

					<CFSET resultStruct.parent_menu_UUID = #THIS.parent_menu_UUID# />
	            	<CFSET resultStruct.toastCode = -1 />
	                <CFSET resultStruct.toastTitle = "Error" />
	                <CFSET resultStruct.toastMessage = "Sorry, there was an error creating " & "#THIS.menu_name#" & "!" />
	                <CFRETURN resultStruct />

	            </CFCATCH>

			</CFTRY>

		</CFIF>
		<!--- End Add Menu --->


		<!--- Edit Menu --->
		<CFIF #THIS.action# EQ "edit">

			<CFTRY>

				<CFLOCK TIMEOUT="20">

					<CFTRANSACTION>

		 				<CFQUERY NAME="edititem" DATASOURCE="#THIS.dsn#">

							UPDATE web_menus

								SET menu_name = <CFQUERYPARAM VALUE="#THIS.menu_name#" CFSQLTYPE="cf_sql_varchar" />,
									menu_order = <CFQUERYPARAM VALUE="#my_menu_order#" CFSQLTYPE="cf_sql_integer" />,
									parent_menu_UUID = <CFQUERYPARAM VALUE="#THIS.parent_menu_UUID#" CFSQLTYPE="cf_sql_varchar" />,
									page_UUID = <CFIF #THIS.page_UUID# NEQ "">
													<CFQUERYPARAM VALUE="#THIS.page_UUID#" CFSQLTYPE="cf_sql_varchar" />
												<CFELSE>
													<CFQUERYPARAM VALUE="" CFSQLTYPE="cf_sql_varchar" NULL="yes"/>
												</CFIF>,
									page_name = <CFQUERYPARAM VALUE="#my_page_name#" CFSQLTYPE="cf_sql_varchar" />,
									modified_by = <CFQUERYPARAM VALUE="#THIS.logged_user#" CFSQLTYPE="cf_sql_varchar" />,
									modified_date = <CFQUERYPARAM VALUE="#NOW()#" CFSQLTYPE="cf_sql_timestamp" />

								WHERE menu_UUID = <CFQUERYPARAM VALUE="#THIS.menu_UUID#" CFSQLTYPE="cf_sql_varchar" />

						</CFQUERY>

						<CFSCRIPT>
							#THIS.reorderMenus()#;
						</CFSCRIPT>

					</CFTRANSACTION>

				</CFLOCK>

				<CFSET resultStruct.parent_menu_UUID = #THIS.parent_menu_UUID# />
				<CFSET resultStruct.toastCode = 1 />
	            <CFSET resultStruct.toastTitle = "Success" />
	            <CFSET resultStruct.toastMessage = "#THIS.menu_name#" & " has been edited!" />
	            <CFRETURN resultStruct />

	            <CFCATCH>

					<CFSET resultStruct.parent_menu_UUID = #THIS.parent_menu_UUID# />
	                <CFSET resultStruct.toastCode = -1 />
	                <CFSET resultStruct.toastTitle = "Error" />
	                <CFSET resultStruct.toastMessage = "Sorry, there was an error editing " & "#THIS.menu_name#" & "!" />
	                <CFRETURN resultStruct />

	            </CFCATCH>

			</CFTRY>

		</CFIF>
		<!--- End Edit Menu --->


		<!--- Delete Menu --->
		<CFIF #THIS.action# EQ "delete">

			<CFTRY>

				<CFLOCK TIMEOUT="20">

					<CFTRANSACTION>

		 				<CFQUERY NAME="deleteitem1" DATASOURCE="#THIS.dsn#">

							UPDATE web_menus

								SET deleted = <CFQUERYPARAM VALUE="1" CFSQLTYPE="cf_sql_bit" />

								WHERE menu_UUID = <CFQUERYPARAM VALUE="#THIS.menu_UUID#" CFSQLTYPE="cf_sql_varchar" />

						</CFQUERY>

						<CFSCRIPT>
							#THIS.reorderMenus()#;
						</CFSCRIPT>

					</CFTRANSACTION>

				</CFLOCK>

				<CFSET resultStruct.parent_menu_UUID = #THIS.parent_menu_UUID# />
				<CFSET resultStruct.toastCode = 1 />
	            <CFSET resultStruct.toastTitle = "Success" />
	            <CFSET resultStruct.toastMessage = "#THIS.menu_name#" & " has been deleted!" />
	            <CFRETURN resultStruct />

	            <CFCATCH>

					<CFSET resultStruct.parent_menu_UUID = #THIS.parent_menu_UUID# />
	                <CFSET resultStruct.toastCode = -1 />
	                <CFSET resultStruct.toastTitle = "Error" />
	                <CFSET resultStruct.toastMessage = "Sorry, there was an error deleting " & "#THIS.menu_name#" & "!" />
	                <CFRETURN resultStruct />

	            </CFCATCH>

			</CFTRY>

		</CFIF>
		<!--- End Delete Menu --->

	</CFFUNCTION>
	<!--- End Menu Process --->

	<!--- Get Linked Pages --->
	<CFFUNCTION NAME="getLinkedPages" ACCESS="public" RETURNTYPE="query">

		<CFQUERY NAME="getlinkedpages" DATASOURCE="#THIS.dsn#">

			SELECT *

				FROM web_pages

				WHERE deleted IS NULL

					AND menu_UUID = <CFQUERYPARAM VALUE="#THIS.menu_UUID#" CFSQLTYPE="cf_sql_varchar" />

				ORDER BY page_name ASC

		</CFQUERY>

		<CFRETURN getlinkedpages />

	</CFFUNCTION>
	<!--- End Get Linked Pages --->


</CFCOMPONENT>
