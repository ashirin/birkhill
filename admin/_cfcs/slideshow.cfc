<CFCOMPONENT>

	<!--- Set THIS scope constructor variables --->
	<CFSET THIS.dsn = "" />

	<CFSET THIS.slideshow_UUID = "" />
	<CFSET THIS.slide1_large = "" />
    <CFSET THIS.slide1_medium = "" />
    <CFSET THIS.slide1_link = "" />
    <CFSET THIS.slide2_large = "" />
    <CFSET THIS.slide2_medium = "" />
    <CFSET THIS.slide2_link = "" />
    <CFSET THIS.slide3_large = "" />
    <CFSET THIS.slide3_medium = "" />
    <CFSET THIS.slide3_link = "" />
    <CFSET THIS.slide4_large = "" />
    <CFSET THIS.slide4_medium = "" />
    <CFSET THIS.slide4_link = "" />
    <CFSET THIS.slide5_large = "" />
    <CFSET THIS.slide5_medium = "" />
    <CFSET THIS.slide5_link = "" />

	<CFSET THIS.logged_user = "" />


	<!--- Get Slideshow Details --->
	<CFFUNCTION NAME="getSlideshowDetails" ACCESS="public" RETURNTYPE="query">

		<CFQUERY NAME="slideshowdetails" DATASOURCE="#THIS.dsn#">
			SELECT *

            	FROM web_slideshow

		</CFQUERY>

		<CFRETURN slideshowdetails />

	</CFFUNCTION>
	<!--- End Get Slideshow Details --->


	<!--- Slideshow Process --->
	<CFFUNCTION NAME="slideshowProcess" ACCESS="public" RETURNTYPE="struct">

		<CFTRY>

			<CFLOCK TIMEOUT="20">

				<CFTRANSACTION>

	 				<CFQUERY NAME="edititem1" DATASOURCE="#THIS.dsn#">

						UPDATE web_slideshow

							SET slide1_large = <CFIF #THIS.slide1_large# NEQ "">
												   <CFQUERYPARAM VALUE="#THIS.slide1_large#" CFSQLTYPE="cf_sql_varchar" />
											   <CFELSE>
												   <CFQUERYPARAM VALUE="" CFSQLTYPE="cf_sql_varchar" NULL="yes"/>
											   </CFIF>,
								slide1_medium = <CFIF #THIS.slide1_medium# NEQ "">
													<CFQUERYPARAM VALUE="#THIS.slide1_medium#" CFSQLTYPE="cf_sql_varchar" />
												<CFELSE>
													<CFQUERYPARAM VALUE="" CFSQLTYPE="cf_sql_varchar" NULL="yes"/>
												</CFIF>,
								slide1_link = <CFIF #THIS.slide1_link# NEQ "">
												  <CFQUERYPARAM VALUE="#THIS.slide1_link#" CFSQLTYPE="cf_sql_varchar" />
											  <CFELSE>
												  <CFQUERYPARAM VALUE="" CFSQLTYPE="cf_sql_varchar" NULL="yes"/>
											  </CFIF>,
								slide2_large = <CFIF #THIS.slide2_large# NEQ "">
												   <CFQUERYPARAM VALUE="#THIS.slide2_large#" CFSQLTYPE="cf_sql_varchar" />
											   <CFELSE>
												   <CFQUERYPARAM VALUE="" CFSQLTYPE="cf_sql_varchar" NULL="yes"/>
											   </CFIF>,
								slide2_medium = <CFIF #THIS.slide2_medium# NEQ "">
													<CFQUERYPARAM VALUE="#THIS.slide2_medium#" CFSQLTYPE="cf_sql_varchar" />
												<CFELSE>
													<CFQUERYPARAM VALUE="" CFSQLTYPE="cf_sql_varchar" NULL="yes"/>
												</CFIF>,
								slide2_link = <CFIF #THIS.slide2_link# NEQ "">
												  <CFQUERYPARAM VALUE="#THIS.slide2_link#" CFSQLTYPE="cf_sql_varchar" />
											  <CFELSE>
												  <CFQUERYPARAM VALUE="" CFSQLTYPE="cf_sql_varchar" NULL="yes"/>
											  </CFIF>,
								slide3_large = <CFIF #THIS.slide3_large# NEQ "">
												   <CFQUERYPARAM VALUE="#THIS.slide3_large#" CFSQLTYPE="cf_sql_varchar" />
											   <CFELSE>
												   <CFQUERYPARAM VALUE="" CFSQLTYPE="cf_sql_varchar" NULL="yes"/>
											   </CFIF>,
								slide3_medium = <CFIF #THIS.slide3_medium# NEQ "">
													<CFQUERYPARAM VALUE="#THIS.slide3_medium#" CFSQLTYPE="cf_sql_varchar" />
												<CFELSE>
													<CFQUERYPARAM VALUE="" CFSQLTYPE="cf_sql_varchar" NULL="yes"/>
												</CFIF>,
								slide3_link = <CFIF #THIS.slide3_link# NEQ "">
												  <CFQUERYPARAM VALUE="#THIS.slide3_link#" CFSQLTYPE="cf_sql_varchar" />
											  <CFELSE>
												  <CFQUERYPARAM VALUE="" CFSQLTYPE="cf_sql_varchar" NULL="yes"/>
											  </CFIF>,
								slide4_large = <CFIF #THIS.slide4_large# NEQ "">
												   <CFQUERYPARAM VALUE="#THIS.slide4_large#" CFSQLTYPE="cf_sql_varchar" />
											   <CFELSE>
												   <CFQUERYPARAM VALUE="" CFSQLTYPE="cf_sql_varchar" NULL="yes"/>
											   </CFIF>,
								slide4_medium = <CFIF #THIS.slide4_medium# NEQ "">
													<CFQUERYPARAM VALUE="#THIS.slide4_medium#" CFSQLTYPE="cf_sql_varchar" />
												<CFELSE>
													<CFQUERYPARAM VALUE="" CFSQLTYPE="cf_sql_varchar" NULL="yes"/>
												</CFIF>,
								slide4_link = <CFIF #THIS.slide4_link# NEQ "">
												  <CFQUERYPARAM VALUE="#THIS.slide4_link#" CFSQLTYPE="cf_sql_varchar" />
											  <CFELSE>
												  <CFQUERYPARAM VALUE="" CFSQLTYPE="cf_sql_varchar" NULL="yes"/>
											  </CFIF>,
								slide5_large = <CFIF #THIS.slide5_large# NEQ "">
												   <CFQUERYPARAM VALUE="#THIS.slide5_large#" CFSQLTYPE="cf_sql_varchar" />
											   <CFELSE>
												   <CFQUERYPARAM VALUE="" CFSQLTYPE="cf_sql_varchar" NULL="yes"/>
											   </CFIF>,
								slide5_medium = <CFIF #THIS.slide5_medium# NEQ "">
													<CFQUERYPARAM VALUE="#THIS.slide5_medium#" CFSQLTYPE="cf_sql_varchar" />
												<CFELSE>
													<CFQUERYPARAM VALUE="" CFSQLTYPE="cf_sql_varchar" NULL="yes"/>
												</CFIF>,
								slide5_link = <CFIF #THIS.slide5_link# NEQ "">
												  <CFQUERYPARAM VALUE="#THIS.slide5_link#" CFSQLTYPE="cf_sql_varchar" />
											  <CFELSE>
												  <CFQUERYPARAM VALUE="" CFSQLTYPE="cf_sql_varchar" NULL="yes"/>
											  </CFIF>,
								modified_by = <CFQUERYPARAM VALUE="#THIS.logged_user#" CFSQLTYPE="cf_sql_varchar" />,
								modified_date = <CFQUERYPARAM VALUE="#NOW()#" CFSQLTYPE="cf_sql_timestamp" />

							WHERE slideshow_UUID = <CFQUERYPARAM VALUE="#THIS.slideshow_UUID#" CFSQLTYPE="cf_sql_varchar" />

					</CFQUERY>

				</CFTRANSACTION>

			</CFLOCK>

			<CFSET resultStruct.toastCode = 1 />
            <CFSET resultStruct.toastTitle = "Success" />
            <CFSET resultStruct.toastMessage = "The slideshow has been edited!" />
            <CFRETURN resultStruct />

            <CFCATCH>

                <CFSET resultStruct.toastCode = -1 />
                <CFSET resultStruct.toastTitle = "Error" />
                <CFSET resultStruct.toastMessage = "Sorry, there was an error editing the slideshow!" />
                <CFRETURN resultStruct />

            </CFCATCH>

		</CFTRY>

	</CFFUNCTION>
	<!--- End Slideshow Process --->


</CFCOMPONENT>
