<CFCOMPONENT HINT="dynamic component">

	<!--- Set THIS scope constructor variables --->
	<CFSET THIS.dsn = "" />
	<CFSET THIS.action = "" />

	<CFSET THIS.page_UUID = "" />
	<CFSET THIS.page_reference = "" />
	<CFSET THIS.page_name = "" />
	<CFSET THIS.page_title = "" />
	<CFSET THIS.page_template = "" />
	<CFSET THIS.page_description = "" />
	<CFSET THIS.page_keywords = "" />
	<CFSET THIS.page_content1= "" />
	<CFSET THIS.page_content2= "" />
	<CFSET THIS.page_content3= "" />
	<CFSET THIS.logged_user = "" />
	<CFSET THIS.image_url = "" />
	<CFSET THIS.image_id = "" />
  <CFSET THIS.span = "" />
  <CFSET THIS.span2 = "" />
  <CFSET THIS.currentimage = "" />
  <CFSET THIS.uploadfile = "" />
  <CFSET THIS.currentimage2 = "" />
  <CFSET THIS.uploadfile2 = "" />
  <CFSET THIS.ribbon = "" />

	<!--- Get Page Details --->
	<CFFUNCTION NAME="getPageDetails" ACCESS="public" RETURNTYPE="query">

		<CFQUERY NAME="pagesdetails" DATASOURCE="#THIS.dsn#">
			SELECT *,
            	   CASE WHEN a.modified_date IS NULL THEN a.added_date ELSE a.modified_date END AS last_modified

            	FROM web_pages a
						LEFT JOIN web_menus b ON a.menu_UUID = b.menu_UUID

                WHERE a.deleted IS NULL

				<CFIF THIS.page_UUID NEQ "">
					AND a.page_UUID = <CFQUERYPARAM VALUE="#THIS.page_UUID#" CFSQLTYPE="cf_sql_varchar" />
				</CFIF>

				<CFIF THIS.page_name NEQ "">
					AND a.page_name = <CFQUERYPARAM VALUE="#THIS.page_name#" CFSQLTYPE="cf_sql_varchar" />
				</CFIF>

		</CFQUERY>

		<CFRETURN pagesdetails />

	</CFFUNCTION>
	<!--- End Get Page Details --->

	<!--- Get Top Level Menus --->
	<CFFUNCTION NAME="getTopLevelMenus" ACCESS="public" RETURNTYPE="query">

		<CFQUERY NAME="topmenusdetails" DATASOURCE="#THIS.dsn#">
			SELECT *

            	FROM web_menus

                WHERE deleted IS NULL

				AND parent_menu_UUID = 'top'

		</CFQUERY>

		<CFRETURN topmenusdetails />

	</CFFUNCTION>
	<!--- End Get Top Level Menus --->

	<cffunction name="getimages" access="public" returntype="query">
		<cfquery name="qimages" datasource="#this.dsn#">
			SELECT image_url, image_id
			FROM gallery
			WHERE page_uuid = <cfqueryparam value="#this.page_uuid#" cfsqltype="cf_sql_varchar"/>
		</cfquery>
		<cfreturn qimages />
	</cffunction>

	<cffunction name="getimagedetails" access="public" returntype="query">
		<cfquery name="qimages" datasource="#this.dsn#">
			SELECT image_url, image_id, page_uuid
			FROM gallery
			WHERE image_id = <cfqueryparam value="#this.image_id#" cfsqltype="cf_sql_int"/>
		</cfquery>
		<cfreturn qimages />
	</cffunction>

	<!--- Page Process --->
	<CFFUNCTION NAME="pageProcess" ACCESS="public" RETURNTYPE="struct">
    <CFIF #THIS.action# NEQ "delete">
      <CFSET myfile1 = #THIS.currentimage#>
      <CFIF #THIS.UploadFile# is not "">
          <CFFILE ACTION= "UPLOAD"
              FILEFIELD = "UploadFile1"
              DESTINATION = "#request.maindirectory#/_img/banners/"
              NAMECONFLICT = "Overwrite" >
          <CFSET myfile1 = #FILE.ServerFile#>
      </CFIF>
      <CFSET myfile2 = #THIS.currentimage2#>
      <CFIF #THIS.UploadFile2# is not "">
          <CFFILE ACTION= "UPLOAD"
              FILEFIELD = "UploadFile2"
              DESTINATION = "#request.maindirectory#/_img/banners/"
              NAMECONFLICT = "Overwrite" >
          <CFSET myfile2 = #FILE.ServerFile#>
      </CFIF>
    </CFIF>
		<!--- Add Page --->
		<CFIF #THIS.action# EQ "add">

			<CFTRY>

				<CFLOCK TIMEOUT="20">

					<CFTRANSACTION>

						<CFSET page_UUID = #CreateUUID()# />

	 					<CFQUERY NAME="additem1" DATASOURCE="#THIS.dsn#">

							INSERT INTO web_pages (page_UUID, page_reference, page_name,
                page_title, page_template, page_description, page_keywords, page_content1,
                page_content2, page_content3, menu_UUID,
                added_by, added_date, banner_image, banner_text, ribbon,
                bottom_image, bottom_text
                )

								VALUES (
										<CFQUERYPARAM VALUE="#page_UUID#" CFSQLTYPE="cf_sql_varchar" />,
										<CFQUERYPARAM VALUE="#THIS.page_reference#" CFSQLTYPE="cf_sql_varchar" />,
										<CFQUERYPARAM VALUE="#THIS.page_name#" CFSQLTYPE="cf_sql_varchar" />,
										<CFIF #THIS.page_title# NEQ "">
											<CFQUERYPARAM VALUE="#THIS.page_title#" CFSQLTYPE="cf_sql_varchar" />
										<CFELSE>
											<CFQUERYPARAM VALUE="" CFSQLTYPE="cf_sql_varchar" NULL="yes"/>
										</CFIF>,
										<CFQUERYPARAM VALUE="#THIS.page_template#" CFSQLTYPE="cf_sql_varchar" />,
										<CFIF #THIS.page_description# NEQ "">
											<CFQUERYPARAM VALUE="#THIS.page_description#" CFSQLTYPE="cf_sql_varchar" />
										<CFELSE>
											<CFQUERYPARAM VALUE="" CFSQLTYPE="cf_sql_varchar" NULL="yes"/>
										</CFIF>,
										<CFIF #THIS.page_keywords# NEQ "">
											<CFQUERYPARAM VALUE="#THIS.page_keywords#" CFSQLTYPE="cf_sql_varchar" />
										<CFELSE>
											<CFQUERYPARAM VALUE="" CFSQLTYPE="cf_sql_varchar" NULL="yes"/>
										</CFIF>,
										<CFQUERYPARAM VALUE="#THIS.page_content1#" CFSQLTYPE="cf_sql_varchar" />,
										<CFQUERYPARAM VALUE="#THIS.page_content2#" CFSQLTYPE="cf_sql_varchar" />,
										<CFQUERYPARAM VALUE="#THIS.page_content3#" CFSQLTYPE="cf_sql_varchar" />,
										<CFIF #THIS.menu_UUID# NEQ "">
											<CFQUERYPARAM VALUE="#THIS.menu_UUID#" CFSQLTYPE="cf_sql_varchar" />
										<CFELSE>
											<CFQUERYPARAM VALUE="" CFSQLTYPE="cf_sql_varchar" NULL="yes"/>
										</CFIF>,
										<CFQUERYPARAM VALUE="#THIS.logged_user#" CFSQLTYPE="cf_sql_varchar" />,
										<CFQUERYPARAM VALUE="#NOW()#" CFSQLTYPE="cf_sql_timestamp" />,
                    <CFIF myfile1 is not "">
                        <cfqueryparam value="#myfile1#" cfsqltype="cf_sql_varchar">
                    <CFELSE>
                        <cfqueryparam value="" cfsqltype="cf_sql_varchar" null="yes">
                    </CFIF>,
                    <CFIF THIS.span NEQ "">
                        <cfqueryparam value="#THIS.span#" cfsqltype="cf_sql_varchar">
                    <CFELSE>
                        <cfqueryparam value="" cfsqltype="cf_sql_varchar" null="yes">
                    </CFIF>,
                    <CFIF THIS.ribbon NEQ "">
                        <cfqueryparam value="#THIS.ribbon#" cfsqltype="cf_sql_tinyint">
                    <CFELSE>
                        <cfqueryparam value="" cfsqltype="cf_sql_varchar" null="yes">
                    </CFIF>,
                    <CFIF myfile2 is not "">
                        <cfqueryparam value="#myfile2#" cfsqltype="cf_sql_varchar">
                    <CFELSE>
                        <cfqueryparam value="" cfsqltype="cf_sql_varchar" null="yes">
                    </CFIF>,
                    <CFIF THIS.span2 NEQ "">
                        <cfqueryparam value="#THIS.span2#" cfsqltype="cf_sql_varchar">
                    <CFELSE>
                        <cfqueryparam value="" cfsqltype="cf_sql_varchar" null="yes">
                    </CFIF>
										)
						</CFQUERY>

						<!--- Create the file using _tempalte.cfm as the basis --->
						<CFFILE ACTION="copy" SOURCE="#request.rootdirectory#\#THIS.page_template#" DESTINATION="#request.rootdirectory#\#THIS.page_name#" />

					</CFTRANSACTION>

				</CFLOCK>

				<CFSET resultStruct.toastCode = 1 />
	            <CFSET resultStruct.toastTitle = "Success" />
	            <CFSET resultStruct.toastMessage = "#THIS.page_reference#" & " has been created!" />
	            <CFRETURN resultStruct />

	            <CFCATCH>

	            	<CFSET resultStruct.toastCode = -1 />
	                <CFSET resultStruct.toastTitle = "Error" />
	                <CFSET resultStruct.toastMessage = "Sorry, there was an error creating " & "#THIS.page_reference#" & "!" />
	                <CFRETURN resultStruct />

	            </CFCATCH>

			</CFTRY>

		</CFIF>
		<!--- End Add Page --->


		<!--- Edit Page --->
		<CFIF #THIS.action# EQ "edit">

			<CFTRY>

				<CFLOCK TIMEOUT="20">

					<CFTRANSACTION>

						<CFQUERY NAME="edititem1" DATASOURCE="#THIS.dsn#">

								SELECT *

									FROM web_pages

									WHERE page_UUID = <CFQUERYPARAM VALUE="#THIS.page_UUID#" CFSQLTYPE="cf_sql_varchar" />

						</CFQUERY>

		 				<CFQUERY NAME="edititem2" DATASOURCE="#THIS.dsn#">

							UPDATE web_pages

								SET page_reference = <CFQUERYPARAM VALUE="#THIS.page_reference#" CFSQLTYPE="cf_sql_varchar" />,
									page_title = <CFIF #THIS.page_title# NEQ "">
													<CFQUERYPARAM VALUE="#THIS.page_title#" CFSQLTYPE="cf_sql_varchar" />
												<CFELSE>
													<CFQUERYPARAM VALUE="" CFSQLTYPE="cf_sql_varchar" NULL="yes"/>
												</CFIF>,
									page_name = <CFQUERYPARAM VALUE="#THIS.page_name#" CFSQLTYPE="cf_sql_varchar" />,
									menu_UUID = <CFIF #THIS.menu_UUID# NEQ "">
													<CFQUERYPARAM VALUE="#THIS.menu_UUID#" CFSQLTYPE="cf_sql_varchar" />
												<CFELSE>
													<CFQUERYPARAM VALUE="" CFSQLTYPE="cf_sql_varchar" NULL="yes"/>
												</CFIF>,
									page_template = <CFQUERYPARAM VALUE="#THIS.page_template#" CFSQLTYPE="cf_sql_varchar" />,
									page_content1 = <CFQUERYPARAM VALUE="#THIS.page_content1#" CFSQLTYPE="cf_sql_varchar" />,
									page_content2 = <CFQUERYPARAM VALUE="#THIS.page_content2#" CFSQLTYPE="cf_sql_varchar" />,
									page_content3 = <CFQUERYPARAM VALUE="#THIS.page_content3#" CFSQLTYPE="cf_sql_varchar" />,
									page_description = <CFIF #THIS.page_description# NEQ "">
														   <CFQUERYPARAM VALUE="#THIS.page_description#" CFSQLTYPE="cf_sql_varchar" />
													   <CFELSE>
														   <CFQUERYPARAM VALUE="" CFSQLTYPE="cf_sql_varchar" NULL="yes"/>
													   </CFIF>,
									page_keywords = <CFIF #THIS.page_keywords# NEQ "">
														<CFQUERYPARAM VALUE="#THIS.page_keywords#" CFSQLTYPE="cf_sql_varchar" />
													<CFELSE>
														<CFQUERYPARAM VALUE="" CFSQLTYPE="cf_sql_varchar" NULL="yes"/>
													</CFIF>,
									modified_by = <CFQUERYPARAM VALUE="#THIS.logged_user#" CFSQLTYPE="cf_sql_varchar" />,
									modified_date = <CFQUERYPARAM VALUE="#NOW()#" CFSQLTYPE="cf_sql_timestamp" />,
                  banner_image = <CFIF myfile1 is not "">
                            <cfqueryparam value="#myfile1#" cfsqltype="cf_sql_varchar">
                        <CFELSE>
                            <cfqueryparam value="" cfsqltype="cf_sql_varchar" null="yes">
                        </CFIF>,
                  banner_text =  <CFIF THIS.span NEQ "">
                          <cfqueryparam value="#THIS.span#" cfsqltype="cf_sql_varchar">
                      <CFELSE>
                          <cfqueryparam value="" cfsqltype="cf_sql_varchar" null="yes">
                      </CFIF>,
                  ribbon = <CFIF THIS.ribbon NEQ "">
                        <cfqueryparam value="#THIS.ribbon#" cfsqltype="cf_sql_tinyint">
                    <CFELSE>
                        <cfqueryparam value="" cfsqltype="cf_sql_varchar" null="yes">
                    </CFIF>,
                  bottom_image = <CFIF myfile2 is not "">
                            <cfqueryparam value="#myfile2#" cfsqltype="cf_sql_varchar">
                        <CFELSE>
                            <cfqueryparam value="" cfsqltype="cf_sql_varchar" null="yes">
                        </CFIF>,
                  bottom_text =  <CFIF THIS.span2 NEQ "">
                          <cfqueryparam value="#THIS.span2#" cfsqltype="cf_sql_varchar">
                      <CFELSE>
                          <cfqueryparam value="" cfsqltype="cf_sql_varchar" null="yes">
                      </CFIF>

								WHERE page_UUID = <CFQUERYPARAM VALUE="#THIS.page_UUID#" CFSQLTYPE="cf_sql_varchar" />

						</CFQUERY>

						<CFIF #THIS.page_template# NEQ #edititem1.page_template#>
							<CFFILE ACTION="copy" SOURCE="#request.rootdirectory#\#THIS.page_template#" DESTINATION="#request.rootdirectory#\#THIS.page_name#" NAMECONFLICT="overwrite"/>
						</CFIF>

					</CFTRANSACTION>

				</CFLOCK>

				<CFSET resultStruct.toastCode = 1 />
	            <CFSET resultStruct.toastTitle = "Success" />
	            <CFSET resultStruct.toastMessage = "#THIS.page_reference#" & " has been edited!" />
	            <CFRETURN resultStruct />

	            <CFCATCH>

	                <CFSET resultStruct.toastCode = -1 />
	                <CFSET resultStruct.toastTitle = "Error" />
	                <CFSET resultStruct.toastMessage = "Sorry, there was an error editing " & "#THIS.page_reference#" & "!" />
	                <CFRETURN resultStruct />

	            </CFCATCH>

			</CFTRY>

		</CFIF>
		<!--- End Edit Page --->


		<!--- Delete Page --->
		<CFIF #THIS.action# EQ "delete">

			<CFTRY>

				<CFLOCK TIMEOUT="20">

					<CFTRANSACTION>

		 				<CFQUERY NAME="deleteitem1" DATASOURCE="#THIS.dsn#">

							UPDATE web_pages

								SET deleted = <CFQUERYPARAM VALUE="1" CFSQLTYPE="cf_sql_bit" />

								WHERE page_UUID = <CFQUERYPARAM VALUE="#THIS.page_UUID#" CFSQLTYPE="cf_sql_varchar" />

						</CFQUERY>

					</CFTRANSACTION>

				</CFLOCK>

				<CFSET resultStruct.toastCode = 1 />
	            <CFSET resultStruct.toastTitle = "Success" />
	            <CFSET resultStruct.toastMessage = "#THIS.page_reference#" & " has been deleted!" />
	            <CFRETURN resultStruct />

	            <CFCATCH>

	                <CFSET resultStruct.toastCode = -1 />
	                <CFSET resultStruct.toastTitle = "Error" />
	                <CFSET resultStruct.toastMessage = "Sorry, there was an error deleting " & "#THIS.page_reference#" & "!" />
	                <CFRETURN resultStruct />

	            </CFCATCH>

			</CFTRY>

		</CFIF>
		<!--- End Delete Page --->

	</CFFUNCTION>
	<!--- End Page Process --->

	<CFFUNCTION NAME="imageProcess" ACCESS="public" RETURNTYPE="struct">
        <CFIF #THIS.action# EQ "add">
            <CFIF THIS.image_url is "">
                <cfset resultStruct.toastCode = -2 />
                <cfset resultStruct.toastTitle = "error" />
                <cfset resultStruct.toastMessage = "Sorry, you must upload image!" />
                <cfreturn resultStruct />
            </CFIF>
            <cfif not DirectoryExists("#request.rootdirectory#images/uploads/#this.page_uuid#")>
                <cfdirectory directory="#request.rootdirectory#images/uploads/#this.page_uuid#" action="create"/>
            </cfif>
            <CFTRY>
                <cfscript>
                    imgObj=CreateObject("component", "birkhillincludes.tmt_img");
                    imgPath = "#request.rootdirectory#images/uploads/#this.page_uuid#/#THIS.image_url#";
                    picHeight = imgObj.getHeight(imgPath);
                    picWidth = imgObj.getWidth(imgPath);

                    resizeObj=CreateObject("component", "birkhillincludes.image_resize");
                    resizeObj.maxWidth = 50;
                    resizeObj.maxHeight = 50;
                    resizeObj.picHeight = picHeight;
                    resizeObj.picWidth = picWidth;
                    mynewsizes = resizeObj.resize_image();
                    imgObj.resize(imgPath, "#request.rootdirectory#images/uploads/#this.page_uuid#/thumb_#THIS.image_url#", #mynewsizes.newwidth#);
                </cfscript>
                <CFLOCK TIMEOUT="20">
                    <CFTRANSACTION>
                        <CFSET image_UUID = #CreateUUID()# />
                        <CFQUERY NAME="additem1" DATASOURCE="#THIS.dsn#">
                            INSERT INTO gallery (image_url, page_uuid)
                            VALUES (
                                    <CFQUERYPARAM VALUE="#THIS.image_url#" CFSQLTYPE="cf_sql_varchar" />,
                                    <CFQUERYPARAM VALUE="#THIS.page_UUID#" CFSQLTYPE="cf_sql_varchar" />
                                   )
                        </CFQUERY>
                    </CFTRANSACTION>
                </CFLOCK>

                <CFSET resultStruct.toastCode = 1 />
                <CFSET resultStruct.toastTitle = "Success" />
                <CFSET resultStruct.toastMessage = "Image has been created!" />
                <CFRETURN resultStruct />

                <CFCATCH>
                    <CFSET resultStruct.toastCode = -1 />
                    <CFSET resultStruct.toastTitle = "Error" />
                    <CFSET resultStruct.toastMessage = "Sorry, there was an error creating " & "#THIS.image_url#" & "!" />
                    <CFSET resultStruct.toastMessage = "#cfcatch.Message# #cfcatch.Detail#" />
                    <CFRETURN resultStruct />
                </CFCATCH>
            </CFTRY>
        </CFIF>

        <CFIF #THIS.action# EQ "edit">
            <CFIF #THIS.image_url# is not "">
            	<cfif not DirectoryExists("#request.rootdirectory#images/uploads/#this.page_uuid#")>
                    <cfdirectory directory="#request.rootdirectory#images/uploads/#this.page_uuid#" action="create"/>
                </cfif>
                <CFFILE ACTION= "UPLOAD"
                        FILEFIELD = "image_url"
                        DESTINATION = "#request.rootdirectory#images/uploads/#this.page_uuid#"
                        NAMECONFLICT = "Overwrite" >
                <CFSET myfile1 = #FILE.ServerFile#>
                <cfscript>
                    imgObj=CreateObject("component", "birkhillincludes.tmt_img");
                    imgPath = "#request.rootdirectory#images/uploads/#this.page_uuid#/#myfile1#";
                    picHeight = imgObj.getHeight(imgPath);
                    picWidth = imgObj.getWidth(imgPath);

                    resizeObj=CreateObject("component", "birkhillincludes.image_resize");
                    resizeObj.maxWidth = 50;
                    resizeObj.maxHeight = 50;
                    resizeObj.picHeight = picHeight;
                    resizeObj.picWidth = picWidth;
                    mynewsizes = resizeObj.resize_image();
                    imgObj.resize(imgPath, "#request.rootdirectory#images/uploads/#this.page_uuid#/thumb_#myfile1#", #mynewsizes.newwidth#);
                </cfscript>
            </CFIF>
            <CFTRY>
            	<CFIF #THIS.image_url# is not "">
	                <CFLOCK TIMEOUT="20">
	                    <CFTRANSACTION>
	                        <CFQUERY NAME="additem1" DATASOURCE="#THIS.dsn#">
	                            UPDATE gallery
	                            SET image_url = <CFQUERYPARAM VALUE="#myfile1#" CFSQLTYPE="cf_sql_varchar" />
	                            WHERE image_id = <CFQUERYPARAM VALUE="#THIS.image_id#" CFSQLTYPE="cf_sql_int" />
	                        </CFQUERY>
	                    </CFTRANSACTION>
	                </CFLOCK>
                </CFIF>

                <CFSET resultStruct.toastCode = 1 />
                <CFSET resultStruct.toastTitle = "Success" />
                <CFSET resultStruct.toastMessage = "Image has been edited!" />
                <CFRETURN resultStruct />

                <CFCATCH>
                    <CFSET resultStruct.toastCode = -1 />
                    <CFSET resultStruct.toastTitle = "Error" />
                    <CFSET resultStruct.toastMessage = "Sorry, there was an error editing " & "#THIS.image_url#" & "!" />
                    <CFSET resultStruct.toastMessage = "#cfcatch.Message# #cfcatch.Detail#" />
                    <CFRETURN resultStruct />
                </CFCATCH>
            </CFTRY>
        </CFIF>

        <CFIF #THIS.action# EQ "delete">
            <CFTRY>
                <CFLOCK TIMEOUT="20">
                    <CFTRANSACTION>
                        <CFQUERY NAME="deleteitem1" DATASOURCE="#THIS.dsn#">
                            DELETE FROM gallery
                            WHERE image_id = <CFQUERYPARAM VALUE="#THIS.image_id#" CFSQLTYPE="cf_sql_int" />
                        </CFQUERY>
                    </CFTRANSACTION>
                </CFLOCK>

                <CFSET resultStruct.toastCode = 1 />
                <CFSET resultStruct.toastTitle = "Success" />
                <CFSET resultStruct.toastMessage = "Image has been deleted!" />
                <CFRETURN resultStruct />

                <CFCATCH>
                    <CFSET resultStruct.toastCode = -1 />
                    <CFSET resultStruct.toastTitle = "Error" />
                    <CFSET resultStruct.toastMessage = "Sorry, there was an error deleting " & "#THIS.image_url#" & "!" />
                    <CFRETURN resultStruct />
                </CFCATCH>
            </CFTRY>
        </CFIF>
    </CFFUNCTION>
</CFCOMPONENT>
