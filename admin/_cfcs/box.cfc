<cfcomponent hint="news component">
    <!--- set THIS scope constructor variables --->

    <cfset THIS.id = "" />
    <cfset THIS.dsn = "" />
    <cfset THIS.link = "" />
    <cfset THIS.line1 = "" />
    <cfset THIS.line2 = "" />
    <cfset THIS.line3 = "" />
    <cfset THIS.line4 = "" />
    <cfset THIS.line5 = "" />
    <cfset THIS.line6 = "" />
    <cfset THIS.span1 = "" />
    <cfset THIS.span2 = "" />
    <cfset THIS.span3 = "" />
    <cfset THIS.span4 = "" />
    <cfset THIS.column1 = "" />
    <cfset THIS.column2 = "" />
    <cfset THIS.column3 = "" />
    <cfset THIS.column4 = "" />
    <cfset THIS.column5 = "" />
    <cfset THIS.header1 = "" />
    <cfset THIS.header2 = "" />
    <cfset THIS.header3 = "" />
    <cfset THIS.header4 = "" />
    <cfset THIS.header5 = "" />
    <cfset THIS.header6 = "" />
    <cfset THIS.box_text1 = "" />
    <cfset THIS.box_text2 = "" />
    <cfset THIS.link_text = "" />
    <cfset THIS.subheader1 = "" />
    <cfset THIS.subheader2 = "" />
    <cfset THIS.subheader3 = "" />
    <cfset THIS.subheader4 = "" />
    <cfset THIS.subheader5 = "" />
    <cfset THIS.subheader6 = "" />
    <cfset THIS.uploadfile1 = "" />
    <cfset THIS.uploadfile2 = "" />
    <cfset THIS.uploadfile3 = "" />
    <cfset THIS.uploadfile4 = "" />
    <cfset THIS.uploadfile5 = "" />
    <cfset THIS.uploadfile6 = "" />
    <cfset THIS.uploadimage1 = "" />
    <cfset THIS.box1_heading1 = "" />
    <cfset THIS.box1_heading2 = "" />
    <cfset THIS.box1_heading3 = "" />
    <cfset THIS.box2_heading1 = "" />
    <cfset THIS.box2_heading2 = "" />
    <cfset THIS.box2_heading3 = "" />
    <cfset THIS.currentimage1 = "" />
    <cfset THIS.currentimage2 = "" />
    <cfset THIS.currentimage3 = "" />
    <cfset THIS.currentimage4 = "" />
    <cfset THIS.currentimage5 = "" />
    <cfset THIS.currentimage6 = "" />
    <cfset THIS.column1_quote = "" />
    <cfset THIS.column2_quote = "" />
    <cfset THIS.column3_quote = "" />
    <cfset THIS.column4_quote = "" />
    <cfset THIS.column5_quote = "" />
    <cfset THIS.relativepath_prefix = "..\.." />

    <!--- default init() method returns instance of the object.
          Behaves in similair way to Java init() method and is here for consistency --->

    <cffunction name="init" access="public" returntype="Project">
        <cfreturn this />
    </cffunction>

    <cffunction name="postcard_process"
        access="public"
        displayname="Create a box"
        hint="Sets box ID in THIS scope"
        returntype="struct">
        <!--- <cftry> --->
            <CFSET myfile1 = #THIS.currentimage1#>
            <CFSET myfile2 = #THIS.currentimage2#>

            <CFIF #THIS.UploadFile1# is not "">
                <CFFILE ACTION= "UPLOAD"
                    FILEFIELD = "UploadFile1"
                    DESTINATION = "#request.rootdirectory#/_img/postcards/"
                    NAMECONFLICT = "Overwrite" >
                <CFSET myfile1 = #FILE.ServerFile#>
            </CFIF>

            <CFIF #THIS.UploadFile2# is not "">
                <CFFILE ACTION= "UPLOAD"
                    FILEFIELD = "UploadFile2"
                    DESTINATION = "#request.rootdirectory#/_img/postcards/"
                    NAMECONFLICT = "Overwrite" >
                <CFSET myfile2 = #FILE.ServerFile#>
            </CFIF>

            <!--- <CFIF #THIS.UploadFile1# NEQ "">
                <cfscript>
                    imgObj=CreateObject("component", "birkhillincludes.tmt_img");
                    picHeight = imgObj.getHeight("#request.rootdirectory#\_img\postcards\#myfile1#");
                    picWidth = imgObj.getWidth("#request.rootdirectory#\_img\postcards\#myfile1#");

                    resizeObj=CreateObject("component", "birkhillincludes.image_resize");
                    resizeObj.maxWidth = request.homethumbnail_width;
                    resizeObj.maxHeight = 300;
                    resizeObj.picHeight = picHeight;
                    resizeObj.picWidth = picWidth;
                    mynewsizes = resizeObj.resize_image();
                    if ((picHeight GT 300) OR (picWidth GT request.homethumbnail_width)) {
                        imgObj.resize("#request.rootdirectory#\_img\postcards\#myfile1#", "#request.rootdirectory#\_img\postcards\#myfile1#", #mynewsizes.newwidth#);
                    }

                </cfscript>
            </CFIF>

            <CFIF #THIS.UploadFile2# NEQ "">
                <cfscript>
                    imgObj=CreateObject("component", "birkhillincludes.tmt_img");
                    picHeight = imgObj.getHeight("#request.rootdirectory#\_img\postcards\#myfile2#");
                    picWidth = imgObj.getWidth("#request.rootdirectory#\_img\postcards\#myfile2#");

                    resizeObj=CreateObject("component", "birkhillincludes.image_resize");
                    resizeObj.maxWidth = request.homethumbnail_width;
                    resizeObj.maxHeight = 300;
                    resizeObj.picHeight = picHeight;
                    resizeObj.picWidth = picWidth;
                    mynewsizes = resizeObj.resize_image();
                    if ((picHeight GT 300) OR (picWidth GT request.homethumbnail_width)) {
                        imgObj.resize("#request.rootdirectory#\_img\postcards\#myfile2#", "#request.rootdirectory#\_img\postcards\#myfile2#", #mynewsizes.newwidth#);
                    }

                </cfscript>
            </CFIF> --->

            <CFQUERY NAME="edititem1" DATASOURCE="#THIS.dsn#">
                UPDATE postcards SET
                box_heading1 =  <CFIF THIS.box1_heading1 NEQ "">
                                    <cfqueryparam value="#THIS.box1_heading1#" cfsqltype="cf_sql_varchar">
                                <CFELSE>
                                    <cfqueryparam value="" cfsqltype="cf_sql_varchar" null="yes">
                                </CFIF>,
                box_heading2 =  <CFIF THIS.box1_heading2 NEQ "">
                                    <cfqueryparam value="#THIS.box1_heading2#" cfsqltype="cf_sql_varchar">
                                <CFELSE>
                                    <cfqueryparam value="" cfsqltype="cf_sql_varchar" null="yes">
                                </CFIF>,
                box_heading3 =  <CFIF THIS.box1_heading3 NEQ "">
                                    <cfqueryparam value="#THIS.box1_heading3#" cfsqltype="cf_sql_varchar">
                                <CFELSE>
                                    <cfqueryparam value="" cfsqltype="cf_sql_varchar" null="yes">
                                </CFIF>,
                box_image = <CFIF myfile1 is not "">
                                <cfqueryparam value="#myfile1#" cfsqltype="cf_sql_varchar">
                            <CFELSE>
                                <cfqueryparam value="" cfsqltype="cf_sql_varchar" null="yes">
                            </CFIF>,
                box_text =  <CFIF THIS.box_text1 NEQ "">
                                <cfqueryparam value="#THIS.box_text1#" cfsqltype="cf_sql_varchar">
                            <CFELSE>
                                <cfqueryparam value="" cfsqltype="cf_sql_varchar" null="yes">
                            </CFIF>
                WHERE id = <cfqueryparam value="1" cfsqltype="cf_sql_integer">
            </cfquery>

            <CFQUERY NAME="edititem2" DATASOURCE="#THIS.dsn#">
                UPDATE postcards SET
                box_heading1 =  <CFIF THIS.box2_heading1 NEQ "">
                                    <cfqueryparam value="#THIS.box2_heading1#" cfsqltype="cf_sql_varchar">
                                <CFELSE>
                                    <cfqueryparam value="" cfsqltype="cf_sql_varchar" null="yes">
                                </CFIF>,
                box_heading2 =  <CFIF THIS.box2_heading2 NEQ "">
                                    <cfqueryparam value="#THIS.box2_heading2#" cfsqltype="cf_sql_varchar">
                                <CFELSE>
                                    <cfqueryparam value="" cfsqltype="cf_sql_varchar" null="yes">
                                </CFIF>,
                box_heading3 =  <CFIF THIS.box2_heading3 NEQ "">
                                    <cfqueryparam value="#THIS.box2_heading3#" cfsqltype="cf_sql_varchar">
                                <CFELSE>
                                    <cfqueryparam value="" cfsqltype="cf_sql_varchar" null="yes">
                                </CFIF>,
                box_image = <CFIF myfile2 is not "">
                                <cfqueryparam value="#myfile2#" cfsqltype="cf_sql_varchar">
                            <CFELSE>
                                <cfqueryparam value="" cfsqltype="cf_sql_varchar" null="yes">
                            </CFIF>,
                box_text =  <CFIF THIS.box_text2 NEQ "">
                                <cfqueryparam value="#THIS.box_text2#" cfsqltype="cf_sql_varchar">
                            <CFELSE>
                                <cfqueryparam value="" cfsqltype="cf_sql_varchar" null="yes">
                            </CFIF>
                WHERE id = <cfqueryparam value="2" cfsqltype="cf_sql_integer">
            </cfquery>

            <cfset resultStruct.toastCode = 1 />
            <cfset resultStruct.toastTitle = "success" />
            <cfset resultStruct.toastMessage = "The boxes have been updated!" />
            <cfreturn resultStruct />
            <!--- <cfcatch> --->
            <cfset resultStruct.toastCode = -1 />
            <cfset resultStruct.toastTitle = error />
            <cfset resultStruct.toastMessage = "Sorry, there was an error updating the boxes!" />
            <cfset resultStruct.toastMessage = cfcatch.detail/>
            <cfreturn resultStruct />
            <!--- </cfcatch> --->
        <!--- </cftry> --->
    </cffunction>

    <cffunction name="getpostcard"
        access="public"
        displayname="Gets box details"
        hint="Returns box details"
        returntype="query">

        <cfquery name="qGetBoxDetails" datasource="#THIS.dsn#">
            SELECT *
            FROM postcards
            WHERE id = <cfqueryparam value="#THIS.id#" cfsqltype="cf_sql_integer" />
        </cfquery>
        <cfreturn qGetBoxDetails />
    </cffunction>

    <cffunction name="banner_process"
        access="public"
        displayname="Create a box"
        hint="Sets box ID in THIS scope"
        returntype="struct">
        <cftry>
            <CFIF THIS.column1 is "">
                <cfset resultStruct.toastcode = -2 />
                <cfset resultStruct.toastTitle = "error" />
                <cfset resultStruct.toastMessage = "Sorry, you must enter column 1!" />
                <cfreturn resultStruct />
            </CFIF>

            <CFIF THIS.column2 is "">
                <cfset resultStruct.toastcode = -2 />
                <cfset resultStruct.toastTitle = "error" />
                <cfset resultStruct.toastMessage = "Sorry, you must enter column 2!" />
                <cfreturn resultStruct />
            </CFIF>

            <CFIF THIS.column3 is "">
                <cfset resultStruct.toastcode = -2 />
                <cfset resultStruct.toastTitle = "error" />
                <cfset resultStruct.toastMessage = "Sorry, you must enter column 3!" />
                <cfreturn resultStruct />
            </CFIF>

            <CFIF THIS.column4 is "">
                <cfset resultStruct.toastcode = -2 />
                <cfset resultStruct.toastTitle = "error" />
                <cfset resultStruct.toastMessage = "Sorry, you must enter column 4!" />
                <cfreturn resultStruct />
            </CFIF>

            <CFIF THIS.column5 is "">
                <cfset resultStruct.toastcode = -2 />
                <cfset resultStruct.toastTitle = "error" />
                <cfset resultStruct.toastMessage = "Sorry, you must enter column 5!" />
                <cfreturn resultStruct />
            </CFIF>

            <CFIF THIS.column1_quote is "">
                <cfset resultStruct.toastcode = -2 />
                <cfset resultStruct.toastTitle = "error" />
                <cfset resultStruct.toastMessage = "Sorry, you must enter quote for column 1!" />
                <cfreturn resultStruct />
            </CFIF>

            <CFIF THIS.column2_quote is "">
                <cfset resultStruct.toastcode = -2 />
                <cfset resultStruct.toastTitle = "error" />
                <cfset resultStruct.toastMessage = "Sorry, you must enter quote for column 2!" />
                <cfreturn resultStruct />
            </CFIF>

            <CFIF THIS.column3_quote is "">
                <cfset resultStruct.toastcode = -2 />
                <cfset resultStruct.toastTitle = "error" />
                <cfset resultStruct.toastMessage = "Sorry, you must enter quote for column 3!" />
                <cfreturn resultStruct />
            </CFIF>

            <CFIF THIS.column4_quote is "">
                <cfset resultStruct.toastcode = -2 />
                <cfset resultStruct.toastTitle = "error" />
                <cfset resultStruct.toastMessage = "Sorry, you must enter quote for column 4!" />
                <cfreturn resultStruct />
            </CFIF>

            <CFIF THIS.column5_quote is "">
                <cfset resultStruct.toastcode = -2 />
                <cfset resultStruct.toastTitle = "error" />
                <cfset resultStruct.toastMessage = "Sorry, you must enter quote for column 5!" />
                <cfreturn resultStruct />
            </CFIF>

            <CFSET myfile1 = #THIS.currentimage1#>

            <CFIF #THIS.uploadimage1# is not "">
                <CFFILE ACTION= "UPLOAD"
                    FILEFIELD = "uploadimage1"
                    DESTINATION = "#request.rootdirectory#/images/banner/banner.jpg"
                    NAMECONFLICT = "Overwrite" >
                <CFSET myfile1 = #FILE.ServerFile#>
            </CFIF>

            <CFIF #THIS.uploadimage1# NEQ "">
                <!--- <cfscript>
                    imgObj=CreateObject("component", "birkhillincludes.tmt_img");
                    picHeight = imgObj.getHeight("#THIS.relativepath_prefix#\images\banner\#myfile1#");
                    picWidth = imgObj.getWidth("#THIS.relativepath_prefix#\images\banner\#myfile1#");

                    resizeObj=CreateObject("component", "birkhillincludes.image_resize");
                    resizeObj.maxWidth = request.homethumbnail_width;
                    resizeObj.maxHeight = 300;
                    resizeObj.picHeight = picHeight;
                    resizeObj.picWidth = picWidth;
                    mynewsizes = resizeObj.resize_image();
                    if ((picHeight GT 300) OR (picWidth GT request.homethumbnail_width)) {
                        imgObj.resize("#THIS.relativepath_prefix#\images\banner\#myfile1#", "#THIS.relativepath_prefix#\images\boxes\#myfile1#", #mynewsizes.newwidth#);
                    }
                </cfscript> --->
                <cffile
                    action = "rename"
                    destination = "#request.rootdirectory#/images/banner/#myfile1#"
                    source = "#request.rootdirectory#/images/banner/banner.jpg"
                    >
            </CFIF>

            <CFQUERY NAME="edititem1" DATASOURCE="#THIS.dsn#">
                UPDATE banner SET
                column1 =   <CFIF THIS.column1 NEQ "">
                                <cfqueryparam value="#THIS.column1#" cfsqltype="cf_sql_varchar">
                            <CFELSE>
                                <cfqueryparam value="" cfsqltype="cf_sql_varchar" null="yes">
                            </CFIF>,
                column2 =   <CFIF THIS.column2 NEQ "">
                                <cfqueryparam value="#THIS.column2#" cfsqltype="cf_sql_varchar">
                            <CFELSE>
                                <cfqueryparam value="" cfsqltype="cf_sql_varchar" null="yes">
                            </CFIF>,
                column3 =   <CFIF THIS.column3 NEQ "">
                                <cfqueryparam value="#THIS.column3#" cfsqltype="cf_sql_varchar">
                            <CFELSE>
                                <cfqueryparam value="" cfsqltype="cf_sql_varchar" null="yes">
                            </CFIF>,
                column4 =   <CFIF THIS.column4 NEQ "">
                                <cfqueryparam value="#THIS.column4#" cfsqltype="cf_sql_varchar">
                            <CFELSE>
                                <cfqueryparam value="" cfsqltype="cf_sql_varchar" null="yes">
                            </CFIF>,
                column5 =   <CFIF THIS.column5 NEQ "">
                                <cfqueryparam value="#THIS.column5#" cfsqltype="cf_sql_varchar">
                            <CFELSE>
                                <cfqueryparam value="" cfsqltype="cf_sql_varchar" null="yes">
                            </CFIF>,
                column1_quote =     <CFIF THIS.column1_quote NEQ "">
                                        <cfqueryparam value="#THIS.column1_quote#" cfsqltype="cf_sql_varchar">
                                    <CFELSE>
                                        <cfqueryparam value="" cfsqltype="cf_sql_varchar" null="yes">
                                    </CFIF>,
                column2_quote =     <CFIF THIS.column2_quote NEQ "">
                                        <cfqueryparam value="#THIS.column2_quote#" cfsqltype="cf_sql_varchar">
                                    <CFELSE>
                                        <cfqueryparam value="" cfsqltype="cf_sql_varchar" null="yes">
                                    </CFIF>,
                column3_quote =     <CFIF THIS.column3_quote NEQ "">
                                        <cfqueryparam value="#THIS.column3_quote#" cfsqltype="cf_sql_varchar">
                                    <CFELSE>
                                        <cfqueryparam value="" cfsqltype="cf_sql_varchar" null="yes">
                                    </CFIF>,
                column4_quote =     <CFIF THIS.column4_quote NEQ "">
                                        <cfqueryparam value="#THIS.column4_quote#" cfsqltype="cf_sql_varchar">
                                    <CFELSE>
                                        <cfqueryparam value="" cfsqltype="cf_sql_varchar" null="yes">
                                    </CFIF>,
                column5_quote =     <CFIF THIS.column5_quote NEQ "">
                                        <cfqueryparam value="#THIS.column5_quote#" cfsqltype="cf_sql_varchar">
                                    <CFELSE>
                                        <cfqueryparam value="" cfsqltype="cf_sql_varchar" null="yes">
                                    </CFIF>,
                line1 = <CFIF THIS.line1 NEQ "">
                            <cfqueryparam value="#THIS.line1#" cfsqltype="cf_sql_varchar">
                        <CFELSE>
                            <cfqueryparam value="" cfsqltype="cf_sql_varchar" null="yes">
                        </CFIF>,
                line2 = <CFIF THIS.line2 NEQ "">
                            <cfqueryparam value="#THIS.line2#" cfsqltype="cf_sql_varchar">
                        <CFELSE>
                            <cfqueryparam value="" cfsqltype="cf_sql_varchar" null="yes">
                        </CFIF>,
                line3 = <CFIF THIS.line3 NEQ "">
                            <cfqueryparam value="#THIS.line3#" cfsqltype="cf_sql_varchar">
                        <CFELSE>
                            <cfqueryparam value="" cfsqltype="cf_sql_varchar" null="yes">
                        </CFIF>,
                image = <CFIF myfile1 is not "">
                            <cfqueryparam value="#myfile1#" cfsqltype="cf_sql_varchar">
                        <CFELSE>
                            <cfqueryparam value="" cfsqltype="cf_sql_varchar" null="yes">
                        </CFIF>
                WHERE id = <cfqueryparam value="1" cfsqltype="cf_sql_integer">
            </cfquery>

            <cfset resultStruct.toastCode = 1 />
            <cfset resultStruct.toastTitle = "success" />
            <cfset resultStruct.toastMessage = "The banner have been updated!" />
            <cfreturn resultStruct />
            <cfcatch>
                <cfset resultStruct.toastCode = -1 />
                <cfset resultStruct.toastTitle = "error" />
                <cfset resultStruct.toastMessage = "Sorry, there was an error updating the banner!" />
                <cfset resultStruct.toastMessage = cfcatch.detail/>
                <cfreturn resultStruct />
            </cfcatch>
        </cftry>
    </cffunction>

    <cffunction name="getbanner"
        access="public"
        displayname="Gets banner details"
        hint="Returns banner details"
        returntype="query">

        <cfquery name="qGetBannerDetails" datasource="#THIS.dsn#">
            SELECT *
            FROM banner
            WHERE id = 1
        </cfquery>
        <cfreturn qGetBannerDetails />
    </cffunction>

    <cffunction name="tryus_process"
        access="public"
        displayname="Create a box"
        hint="Sets box ID in THIS scope"
        returntype="struct">
        <cftry>
            <CFQUERY NAME="edititem1" DATASOURCE="#THIS.dsn#">
                UPDATE tryus SET
                line1 = <CFIF THIS.line1 NEQ "">
                            <cfqueryparam value="#THIS.line1#" cfsqltype="cf_sql_varchar">
                        <CFELSE>
                            <cfqueryparam value="" cfsqltype="cf_sql_varchar" null="yes">
                        </CFIF>,
                line2 = <CFIF THIS.line2 NEQ "">
                            <cfqueryparam value="#THIS.line2#" cfsqltype="cf_sql_varchar">
                        <CFELSE>
                            <cfqueryparam value="" cfsqltype="cf_sql_varchar" null="yes">
                        </CFIF>,
                line3 = <CFIF THIS.line3 NEQ "">
                            <cfqueryparam value="#THIS.line3#" cfsqltype="cf_sql_varchar">
                        <CFELSE>
                            <cfqueryparam value="" cfsqltype="cf_sql_varchar" null="yes">
                        </CFIF>,
                link = <CFIF THIS.link NEQ "">
                            <cfqueryparam value="#THIS.link#" cfsqltype="cf_sql_varchar">
                        <CFELSE>
                            <cfqueryparam value="" cfsqltype="cf_sql_varchar" null="yes">
                        </CFIF>,
                link_text = <CFIF THIS.link_text NEQ "">
                            <cfqueryparam value="#THIS.link_text#" cfsqltype="cf_sql_varchar">
                        <CFELSE>
                            <cfqueryparam value="" cfsqltype="cf_sql_varchar" null="yes">
                        </CFIF>
                WHERE id = <cfqueryparam value="1" cfsqltype="cf_sql_integer">
            </cfquery>

            <cfset resultStruct.toastCode = 1 />
            <cfset resultStruct.toastTitle = "success" />
            <cfset resultStruct.toastMessage = "The try us have been updated!" />
            <cfreturn resultStruct />
            <cfcatch>
                <cfset resultStruct.toastCode = -1 />
                <cfset resultStruct.toastTitle = "error" />
                <cfset resultStruct.toastMessage = "Sorry, there was an error updating the try us!" />
                <cfset resultStruct.toastMessage = cfcatch.detail/>
                <cfreturn resultStruct />
            </cfcatch>
        </cftry>
    </cffunction>

    <cffunction name="gettryus"
        access="public"
        displayname="Gets tryus details"
        hint="Returns tryus details"
        returntype="query">

        <cfquery name="qGettryusDetails" datasource="#THIS.dsn#">
            SELECT *
            FROM tryus
            WHERE id = 1
        </cfquery>
        <cfreturn qGettryusDetails />
    </cffunction>

    <cffunction name="circles_process"
        access="public"
        displayname="Create a box"
        hint="Sets box ID in THIS scope"
        returntype="struct">

         <cftry>
            <CFSET myfile1 = #THIS.currentimage1#>
            <CFSET myfile2 = #THIS.currentimage2#>
            <CFSET myfile3 = #THIS.currentimage3#>
            <CFSET myfile4 = #THIS.currentimage4#>
            <CFSET myfile5 = #THIS.currentimage5#>
            <CFSET myfile6 = #THIS.currentimage6#>

            <CFIF #THIS.uploadfile1# is not "">
                <CFFILE ACTION= "UPLOAD"
                    FILEFIELD = "icon1"
                    DESTINATION = "#request.rootdirectory#/_img/circles/"
                    NAMECONFLICT = "Overwrite" >
                <CFSET myfile1 = #FILE.ServerFile#>
            </CFIF>

            <CFIF #THIS.uploadfile2# is not "">
                <CFFILE ACTION= "UPLOAD"
                    FILEFIELD = "icon2"
                    DESTINATION = "#request.rootdirectory#/_img/circles/"
                    NAMECONFLICT = "Overwrite" >
                <CFSET myfile2 = #FILE.ServerFile#>
            </CFIF>

            <CFIF #THIS.uploadfile3# is not "">
                <CFFILE ACTION= "UPLOAD"
                    FILEFIELD = "icon3"
                    DESTINATION = "#request.maindirectory#/_img/circles/"
                    NAMECONFLICT = "Overwrite" >
                <CFSET myfile3 = #FILE.ServerFile#>
            </CFIF>

            <CFIF #THIS.uploadfile4# is not "">
                <CFFILE ACTION= "UPLOAD"
                    FILEFIELD = "icon4"
                    DESTINATION = "#request.maindirectory#/_img/circles/"
                    NAMECONFLICT = "Overwrite" >
                <CFSET myfile4 = #FILE.ServerFile#>
            </CFIF>
            <CFIF #THIS.uploadfile5# is not "">
                <CFFILE ACTION= "UPLOAD"
                    FILEFIELD = "icon5"
                    DESTINATION = "#request.maindirectory#/_img/circles/"
                    NAMECONFLICT = "Overwrite" >
                <CFSET myfile5 = #FILE.ServerFile#>
            </CFIF>

            <CFIF #THIS.uploadfile6# is not "">
                <CFFILE ACTION= "UPLOAD"
                    FILEFIELD = "icon6"
                    DESTINATION = "#request.maindirectory#/_img/circles/"
                    NAMECONFLICT = "Overwrite" >
                <CFSET myfile6 = #FILE.ServerFile#>
            </CFIF>

            <CFQUERY NAME="edititem1" DATASOURCE="#THIS.dsn#">
                UPDATE circles SET
                line1 = <CFIF THIS.line1 NEQ "">
                            <cfqueryparam value="#THIS.line1#" cfsqltype="cf_sql_varchar">
                        <CFELSE>
                            <cfqueryparam value="" cfsqltype="cf_sql_varchar" null="yes">
                        </CFIF>,
                line2 = <CFIF THIS.line2 NEQ "">
                            <cfqueryparam value="#THIS.line2#" cfsqltype="cf_sql_varchar">
                        <CFELSE>
                            <cfqueryparam value="" cfsqltype="cf_sql_varchar" null="yes">
                        </CFIF>,
                line3 = <CFIF THIS.line3 NEQ "">
                            <cfqueryparam value="#THIS.line3#" cfsqltype="cf_sql_varchar">
                        <CFELSE>
                            <cfqueryparam value="" cfsqltype="cf_sql_varchar" null="yes">
                        </CFIF>,
                line4 = <CFIF THIS.line4 NEQ "">
                            <cfqueryparam value="#THIS.line4#" cfsqltype="cf_sql_varchar">
                        <CFELSE>
                            <cfqueryparam value="" cfsqltype="cf_sql_varchar" null="yes">
                        </CFIF>,
                line5 = <CFIF THIS.line5 NEQ "">
                            <cfqueryparam value="#THIS.line5#" cfsqltype="cf_sql_varchar">
                        <CFELSE>
                            <cfqueryparam value="" cfsqltype="cf_sql_varchar" null="yes">
                        </CFIF>,
                line6 = <CFIF THIS.line6 NEQ "">
                            <cfqueryparam value="#THIS.line6#" cfsqltype="cf_sql_varchar">
                        <CFELSE>
                            <cfqueryparam value="" cfsqltype="cf_sql_varchar" null="yes">
                        </CFIF>,
                picture1 = <CFIF myfile1 NEQ "">
                            <cfqueryparam value="#myfile1#" cfsqltype="cf_sql_varchar">
                        <CFELSE>
                            <cfqueryparam value="" cfsqltype="cf_sql_varchar" null="yes">
                        </CFIF>,
                picture2 = <CFIF myfile2 NEQ "">
                            <cfqueryparam value="#myfile2#" cfsqltype="cf_sql_varchar">
                        <CFELSE>
                            <cfqueryparam value="" cfsqltype="cf_sql_varchar" null="yes">
                        </CFIF>,
                picture3 = <CFIF myfile3 NEQ "">
                            <cfqueryparam value="#myfile3#" cfsqltype="cf_sql_varchar">
                        <CFELSE>
                            <cfqueryparam value="" cfsqltype="cf_sql_varchar" null="yes">
                        </CFIF>,
                picture4 = <CFIF myfile4 NEQ "">
                            <cfqueryparam value="#myfile4#" cfsqltype="cf_sql_varchar">
                        <CFELSE>
                            <cfqueryparam value="" cfsqltype="cf_sql_varchar" null="yes">
                        </CFIF>,
                picture5 = <CFIF myfile5 NEQ "">
                            <cfqueryparam value="#myfile5#" cfsqltype="cf_sql_varchar">
                        <CFELSE>
                            <cfqueryparam value="" cfsqltype="cf_sql_varchar" null="yes">
                        </CFIF>,
                picture6 = <CFIF myfile6 NEQ "">
                            <cfqueryparam value="#myfile6#" cfsqltype="cf_sql_varchar">
                        <CFELSE>
                            <cfqueryparam value="" cfsqltype="cf_sql_varchar" null="yes">
                        </CFIF>
                WHERE id = <cfqueryparam value="1" cfsqltype="cf_sql_integer">
            </cfquery>

            <cfset resultStruct.toastCode = 1 />
            <cfset resultStruct.toastTitle = "success" />
            <cfset resultStruct.toastMessage = "The circles have been updated!" />
            <cfreturn resultStruct />
            <cfcatch>
                <cfset resultStruct.toastCode = -1 />
                <cfset resultStruct.toastTitle = "error" />
                <cfset resultStruct.toastMessage = "Sorry, there was an error updating the circles!" />
                <cfset resultStruct.toastMessage = cfcatch.detail/>
                <cfreturn resultStruct />
            </cfcatch>
        </cftry>
    </cffunction>

    <cffunction name="getcircles"
        access="public"
        displayname="Gets tryus details"
        hint="Returns tryus details"
        returntype="query">

        <cfquery name="qGettryusDetails" datasource="#THIS.dsn#">
            SELECT *
            FROM circles
            WHERE id = 1
        </cfquery>
        <cfreturn qGettryusDetails />
    </cffunction>

 <cffunction name="testimonials_process"
        access="public"
        returntype="struct">
        <cftry>
            <CFQUERY NAME="edititem1" DATASOURCE="#THIS.dsn#">
                UPDATE testimonials SET
                line1 = <CFIF THIS.line1 NEQ "">
                            <cfqueryparam value="#THIS.line1#" cfsqltype="cf_sql_varchar">
                        <CFELSE>
                            <cfqueryparam value="" cfsqltype="cf_sql_varchar" null="yes">
                        </CFIF>,
                line2 = <CFIF THIS.line2 NEQ "">
                            <cfqueryparam value="#THIS.line2#" cfsqltype="cf_sql_varchar">
                        <CFELSE>
                            <cfqueryparam value="" cfsqltype="cf_sql_varchar" null="yes">
                        </CFIF>,
                line3 = <CFIF THIS.line3 NEQ "">
                            <cfqueryparam value="#THIS.line3#" cfsqltype="cf_sql_varchar">
                        <CFELSE>
                            <cfqueryparam value="" cfsqltype="cf_sql_varchar" null="yes">
                        </CFIF>,
                line4 = <CFIF THIS.line4 NEQ "">
                            <cfqueryparam value="#THIS.line4#" cfsqltype="cf_sql_varchar">
                        <CFELSE>
                            <cfqueryparam value="" cfsqltype="cf_sql_varchar" null="yes">
                        </CFIF>,
                line5 = <CFIF THIS.line5 NEQ "">
                            <cfqueryparam value="#THIS.line5#" cfsqltype="cf_sql_varchar">
                        <CFELSE>
                            <cfqueryparam value="" cfsqltype="cf_sql_varchar" null="yes">
                        </CFIF>,
                line6 = <CFIF THIS.line6 NEQ "">
                            <cfqueryparam value="#THIS.line6#" cfsqltype="cf_sql_varchar">
                        <CFELSE>
                            <cfqueryparam value="" cfsqltype="cf_sql_varchar" null="yes">
                        </CFIF>
                WHERE id = <cfqueryparam value="1" cfsqltype="cf_sql_integer">
            </cfquery>

            <cfset resultStruct.toastCode = 1 />
            <cfset resultStruct.toastTitle = "success" />
            <cfset resultStruct.toastMessage = "The testimonials have been updated!" />
            <cfreturn resultStruct />
            <cfcatch>
                <cfset resultStruct.toastCode = -1 />
                <cfset resultStruct.toastTitle = "error" />
                <cfset resultStruct.toastMessage = "Sorry, there was an error updating the testimonials!" />
                <cfset resultStruct.toastMessage = cfcatch.detail/>
                <cfreturn resultStruct />
            </cfcatch>
        </cftry>
    </cffunction>


    <cffunction name="gettestimonials"
        access="public"
        returntype="query">

        <cfquery name="qGetTestimonials" datasource="#THIS.dsn#">
            SELECT *
            FROM testimonials
            WHERE id = 1
        </cfquery>
        <cfreturn qGetTestimonials />
    </cffunction>

    <cffunction name="icons_process"
        access="public"
        displayname="Create a box"
        hint="Sets box ID in THIS scope"
        returntype="struct">
        <cftry>
            <CFQUERY NAME="edititem1" DATASOURCE="#THIS.dsn#">
                UPDATE icons SET
                header1 = <CFIF THIS.header1 NEQ "">
                            <cfqueryparam value="#THIS.header1#" cfsqltype="cf_sql_varchar">
                        <CFELSE>
                            <cfqueryparam value="" cfsqltype="cf_sql_varchar" null="yes">
                        </CFIF>,
                header2 = <CFIF THIS.header2 NEQ "">
                            <cfqueryparam value="#THIS.header2#" cfsqltype="cf_sql_varchar">
                        <CFELSE>
                            <cfqueryparam value="" cfsqltype="cf_sql_varchar" null="yes">
                        </CFIF>,
                header3 = <CFIF THIS.header3 NEQ "">
                            <cfqueryparam value="#THIS.header3#" cfsqltype="cf_sql_varchar">
                        <CFELSE>
                            <cfqueryparam value="" cfsqltype="cf_sql_varchar" null="yes">
                        </CFIF>,
                header4 = <CFIF THIS.header4 NEQ "">
                            <cfqueryparam value="#THIS.header4#" cfsqltype="cf_sql_varchar">
                        <CFELSE>
                            <cfqueryparam value="" cfsqltype="cf_sql_varchar" null="yes">
                        </CFIF>,
                header5 = <CFIF THIS.header5 NEQ "">
                            <cfqueryparam value="#THIS.header5#" cfsqltype="cf_sql_varchar">
                        <CFELSE>
                            <cfqueryparam value="" cfsqltype="cf_sql_varchar" null="yes">
                        </CFIF>,
                header6 = <CFIF THIS.header6 NEQ "">
                            <cfqueryparam value="#THIS.header6#" cfsqltype="cf_sql_varchar">
                        <CFELSE>
                            <cfqueryparam value="" cfsqltype="cf_sql_varchar" null="yes">
                        </CFIF>,
                subheader1 = <CFIF THIS.subheader1 NEQ "">
                            <cfqueryparam value="#THIS.subheader1#" cfsqltype="cf_sql_varchar">
                        <CFELSE>
                            <cfqueryparam value="" cfsqltype="cf_sql_varchar" null="yes">
                        </CFIF>,
                subheader2 = <CFIF THIS.subheader2 NEQ "">
                            <cfqueryparam value="#THIS.subheader2#" cfsqltype="cf_sql_varchar">
                        <CFELSE>
                            <cfqueryparam value="" cfsqltype="cf_sql_varchar" null="yes">
                        </CFIF>,
                subheader3 = <CFIF THIS.subheader3 NEQ "">
                            <cfqueryparam value="#THIS.subheader3#" cfsqltype="cf_sql_varchar">
                        <CFELSE>
                            <cfqueryparam value="" cfsqltype="cf_sql_varchar" null="yes">
                        </CFIF>,
                subheader4 = <CFIF THIS.subheader4 NEQ "">
                            <cfqueryparam value="#THIS.subheader4#" cfsqltype="cf_sql_varchar">
                        <CFELSE>
                            <cfqueryparam value="" cfsqltype="cf_sql_varchar" null="yes">
                        </CFIF>,
                subheader5 = <CFIF THIS.subheader5 NEQ "">
                            <cfqueryparam value="#THIS.subheader5#" cfsqltype="cf_sql_varchar">
                        <CFELSE>
                            <cfqueryparam value="" cfsqltype="cf_sql_varchar" null="yes">
                        </CFIF>,
                subheader6 = <CFIF THIS.subheader6 NEQ "">
                            <cfqueryparam value="#THIS.subheader6#" cfsqltype="cf_sql_varchar">
                        <CFELSE>
                            <cfqueryparam value="" cfsqltype="cf_sql_varchar" null="yes">
                        </CFIF>
                WHERE id = <cfqueryparam value="1" cfsqltype="cf_sql_integer">
            </cfquery>

            <cfset resultStruct.toastCode = 1 />
            <cfset resultStruct.toastTitle = "success" />
            <cfset resultStruct.toastMessage = "The icons have been updated!" />
            <cfreturn resultStruct />
            <cfcatch>
                <cfset resultStruct.toastCode = -1 />
                <cfset resultStruct.toastTitle = "error" />
                <cfset resultStruct.toastMessage = "Sorry, there was an error updating the icons!" />
                <cfset resultStruct.toastMessage = cfcatch.detail/>
                <cfreturn resultStruct />
            </cfcatch>
        </cftry>
    </cffunction>

    <cffunction name="geticons"
        access="public"
        displayname="Gets tryus details"
        hint="Returns tryus details"
        returntype="query">

        <cfquery name="qGettryusDetails" datasource="#THIS.dsn#">
            SELECT *
            FROM icons
            WHERE id = 1
        </cfquery>
        <cfreturn qGettryusDetails />
    </cffunction>

    <cffunction name="carousel_process"
        access="public"
        displayname="Create a box"
        hint="Sets box ID in THIS scope"
        returntype="struct">
        <cftry>
            <CFSET myfile1 = #THIS.currentimage1#>
            <CFSET myfile2 = #THIS.currentimage2#>
            <CFSET myfile3 = #THIS.currentimage3#>
            <CFSET myfile4 = #THIS.currentimage4#>

            <CFIF #THIS.UploadFile1# is not "">
                <CFFILE ACTION= "UPLOAD"
                    FILEFIELD = "UploadFile1"
                    DESTINATION = "#request.maindirectory#/_img/slideshow/"
                    NAMECONFLICT = "Overwrite" >
                <CFSET myfile1 = #FILE.ServerFile#>
            </CFIF>

            <CFIF #THIS.UploadFile2# is not "">
                <CFFILE ACTION= "UPLOAD"
                    FILEFIELD = "UploadFile2"
                    DESTINATION = "#request.maindirectory#/_img/slideshow/"
                    NAMECONFLICT = "Overwrite" >
                <CFSET myfile2 = #FILE.ServerFile#>
            </CFIF>

            <CFIF #THIS.UploadFile3# is not "">
                <CFFILE ACTION= "UPLOAD"
                    FILEFIELD = "UploadFile3"
                    DESTINATION = "#request.maindirectory#/_img/slideshow/"
                    NAMECONFLICT = "Overwrite" >
                <CFSET myfile3 = #FILE.ServerFile#>
            </CFIF>

            <CFIF #THIS.UploadFile4# is not "">
                <CFFILE ACTION= "UPLOAD"
                    FILEFIELD = "UploadFile4"
                    DESTINATION = "#request.maindirectory#/_img/slideshow/"
                    NAMECONFLICT = "Overwrite" >
                <CFSET myfile4 = #FILE.ServerFile#>
            </CFIF>

            <!--- <CFIF #THIS.UploadFile1# NEQ "">
                <cfscript>
                    imgObj=CreateObject("component", "birkhillincludes.tmt_img");
                    picHeight = imgObj.getHeight("#THIS.relativepath_prefix#\_img\boxes\#myfile1#");
                    picWidth = imgObj.getWidth("#THIS.relativepath_prefix#\images\boxes\#myfile1#");

                    resizeObj=CreateObject("component", "birkhillincludes.image_resize");
                    resizeObj.maxWidth = request.homethumbnail_width;
                    resizeObj.maxHeight = 300;
                    resizeObj.picHeight = picHeight;
                    resizeObj.picWidth = picWidth;
                    mynewsizes = resizeObj.resize_image();
                    if ((picHeight GT 300) OR (picWidth GT request.homethumbnail_width)) {
                        imgObj.resize("#THIS.relativepath_prefix#\images\boxes\#myfile1#", "#THIS.relativepath_prefix#\images\boxes\#myfile1#", #mynewsizes.newwidth#);
                    }

                </cfscript>
            </CFIF>

            <CFIF #THIS.UploadFile2# NEQ "">
                <cfscript>
                    imgObj=CreateObject("component", "birkhillincludes.tmt_img");
                    picHeight = imgObj.getHeight("#THIS.relativepath_prefix#\images\boxes\#myfile2#");
                    picWidth = imgObj.getWidth("#THIS.relativepath_prefix#\images\boxes\#myfile2#");

                    resizeObj=CreateObject("component", "birkhillincludes.image_resize");
                    resizeObj.maxWidth = request.homethumbnail_width;
                    resizeObj.maxHeight = 300;
                    resizeObj.picHeight = picHeight;
                    resizeObj.picWidth = picWidth;
                    mynewsizes = resizeObj.resize_image();
                    if ((picHeight GT 300) OR (picWidth GT request.homethumbnail_width)) {
                        imgObj.resize("#THIS.relativepath_prefix#\images\boxes\#myfile2#", "#THIS.relativepath_prefix#\images\boxes\#myfile2#", #mynewsizes.newwidth#);
                    }

                </cfscript>
            </CFIF> --->

            <CFQUERY NAME="edititem1" DATASOURCE="#THIS.dsn#">
                UPDATE carousel SET
                header =    <CFIF THIS.header1 NEQ "">
                                <cfqueryparam value="#THIS.header1#" cfsqltype="cf_sql_varchar">
                            <CFELSE>
                                <cfqueryparam value="" cfsqltype="cf_sql_varchar" null="yes">
                            </CFIF>,
                span =  <CFIF THIS.span1 NEQ "">
                            <cfqueryparam value="#THIS.span1#" cfsqltype="cf_sql_varchar">
                        <CFELSE>
                            <cfqueryparam value="" cfsqltype="cf_sql_varchar" null="yes">
                        </CFIF>,
                image = <CFIF myfile1 is not "">
                            <cfqueryparam value="#myfile1#" cfsqltype="cf_sql_varchar">
                        <CFELSE>
                            <cfqueryparam value="" cfsqltype="cf_sql_varchar" null="yes">
                        </CFIF>
                WHERE id = <cfqueryparam value="1" cfsqltype="cf_sql_integer">
            </cfquery>

            <CFQUERY NAME="edititem2" DATASOURCE="#THIS.dsn#">
                UPDATE carousel SET
                header =    <CFIF THIS.header2 NEQ "">
                                <cfqueryparam value="#THIS.header2#" cfsqltype="cf_sql_varchar">
                            <CFELSE>
                                <cfqueryparam value="" cfsqltype="cf_sql_varchar" null="yes">
                            </CFIF>,
                span =  <CFIF THIS.span2 NEQ "">
                            <cfqueryparam value="#THIS.span2#" cfsqltype="cf_sql_varchar">
                        <CFELSE>
                            <cfqueryparam value="" cfsqltype="cf_sql_varchar" null="yes">
                        </CFIF>,
                image = <CFIF myfile2 is not "">
                            <cfqueryparam value="#myfile2#" cfsqltype="cf_sql_varchar">
                        <CFELSE>
                            <cfqueryparam value="" cfsqltype="cf_sql_varchar" null="yes">
                        </CFIF>
                WHERE id = <cfqueryparam value="2" cfsqltype="cf_sql_integer">
            </cfquery>

            <CFQUERY NAME="edititem3" DATASOURCE="#THIS.dsn#">
                UPDATE carousel SET
                header =    <CFIF THIS.header3 NEQ "">
                                <cfqueryparam value="#THIS.header3#" cfsqltype="cf_sql_varchar">
                            <CFELSE>
                                <cfqueryparam value="" cfsqltype="cf_sql_varchar" null="yes">
                            </CFIF>,
                span =  <CFIF THIS.span3 NEQ "">
                            <cfqueryparam value="#THIS.span3#" cfsqltype="cf_sql_varchar">
                        <CFELSE>
                            <cfqueryparam value="" cfsqltype="cf_sql_varchar" null="yes">
                        </CFIF>,
                image = <CFIF myfile3 is not "">
                            <cfqueryparam value="#myfile3#" cfsqltype="cf_sql_varchar">
                        <CFELSE>
                            <cfqueryparam value="" cfsqltype="cf_sql_varchar" null="yes">
                        </CFIF>
                WHERE id = <cfqueryparam value="3" cfsqltype="cf_sql_integer">
            </cfquery>

            <CFQUERY NAME="edititem4" DATASOURCE="#THIS.dsn#">
                UPDATE carousel SET
                header =    <CFIF THIS.header4 NEQ "">
                                <cfqueryparam value="#THIS.header4#" cfsqltype="cf_sql_varchar">
                            <CFELSE>
                                <cfqueryparam value="" cfsqltype="cf_sql_varchar" null="yes">
                            </CFIF>,
                span =  <CFIF THIS.span4 NEQ "">
                            <cfqueryparam value="#THIS.span4#" cfsqltype="cf_sql_varchar">
                        <CFELSE>
                            <cfqueryparam value="" cfsqltype="cf_sql_varchar" null="yes">
                        </CFIF>,
                image = <CFIF myfile4 is not "">
                            <cfqueryparam value="#myfile4#" cfsqltype="cf_sql_varchar">
                        <CFELSE>
                            <cfqueryparam value="" cfsqltype="cf_sql_varchar" null="yes">
                        </CFIF>
                WHERE id = <cfqueryparam value="4" cfsqltype="cf_sql_integer">
            </cfquery>

            <cfset resultStruct.toastCode = 1 />
            <cfset resultStruct.toastTitle = "success" />
            <cfset resultStruct.toastMessage = "The boxes have been updated!" />
            <cfreturn resultStruct />
            <cfcatch>
                <cfset resultStruct.toastCode = -1 />
                <cfset resultStruct.toastTitle = "error" />
                <cfset resultStruct.toastMessage = "Sorry, there was an error updating the boxes!" />
                <cfset resultStruct.toastMessage = cfcatch.detail/>
                <cfreturn resultStruct />
            </cfcatch>
        </cftry>
    </cffunction>

    <cffunction name="getcarousel"
        access="public"
        displayname="Gets box details"
        hint="Returns box details"
        returntype="query">

        <cfquery name="qGetBoxDetails" datasource="#THIS.dsn#">
            SELECT *
            FROM carousel
            WHERE id = <cfqueryparam value="#THIS.id#" cfsqltype="cf_sql_integer" />
        </cfquery>
        <cfreturn qGetBoxDetails />
    </cffunction>


    <cffunction name="getcarouselFull"
        access="public"
        returntype="query">

        <cfquery name="qGetBoxDetails" datasource="#THIS.dsn#">
            SELECT *
            FROM carousel
            WHERE image <> ''
        </cfquery>
        <cfreturn qGetBoxDetails />
    </cffunction>

</cfcomponent>