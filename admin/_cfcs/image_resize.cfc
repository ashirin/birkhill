<cfcomponent hint="image resize component">
	<!--- set THIS scope constructor variables --->
	
	<cfset THIS.maxHeight = "">
	<cfset THIS.maxWidth = "">

	
	<cfset THIS.picHeight = "">
	<cfset THIS.picWidth = "">
	
		
	<!--- default init() method returns instance of the object.
	Behaves in similair way to Java init() method and is here for consistency --->
	<cffunction name="init" access="public" returntype="Project">
		<cfreturn this />
	</cffunction>

<cffunction name="resize_image" 
		access="public"
		displayname="Set Image Sizes" 
		hint="Returns sizes" 
		returntype="struct">



	
				
<CFSET myflag = 0>
				
				<cfset newwidth = THIS.maxWidth>
				<cfset newheight = THIS.maxHeight>
				
				
				 <!--- set final image dimensions --->
                <!--- if it's taller --->
				<CFIF myflag EQ 0>
                <cfif THIS.picHeight GT THIS.picWidth>
					<cfset newheight = THIS.maxHeight>
					<cfset newwidth = Fix((THIS.maxHeight*THIS.picWidth)/THIS.picHeight)>
               	</cfif>

				
					
				<!--- check to see if the width is  GT max--->
				<cfif newwidth GT THIS.maxWidth>
					
					<cfset newheight = Fix((THIS.maxHeight*THIS.maxWidth)/newwidth)>
					<cfset newwidth = THIS.maxWidth>
					
                </cfif>	
				<CFSET myflag = 1>
				</CFIF>
				
				
				
              
<CFIF myflag EQ 0>
                <!--- if it's wider --->
                <cfif THIS.picWidth GT THIS.picHeight>
					<cfset newwidth = THIS.maxWidth>
					<cfset newheight = Fix((THIS.maxWidth*THIS.picHeight)/THIS.picWidth)>
					
                </cfif>
				
			
				
								<!--- check to see if the width is  GT max--->
				<cfif newheight GT THIS.maxHeight>
				
					
					<cfset newwidth = Fix((THIS.maxWidth*THIS.maxHeight)/newheight)>
					<cfset newheight = THIS.maxHeight>
                </cfif>	
				<CFSET myflag = 1>
		</CFIF>
	
	

				
		
		<cfset resultStruct.newwidth = #newwidth# />
		<cfset resultStruct.newheight = #newheight# />
            <cfreturn resultStruct />
			
	</cffunction>
	
		
	
	
	
	
	
	
</cfcomponent>