
<CFCOMPONENT>

	<!--- set THIS scope constructor variables --->
	<CFSET THIS.dsn = "" />
	<CFSET THIS.action = "" />

	<CFSET THIS.box_UUID = "" />
	<CFSET THIS.box_heading1 = "" />
    <CFSET THIS.box_heading2 = "" />
    <CFSET THIS.box_heading3 = "" />
	<CFSET THIS.box_heading4 = "" />
    <CFSET THIS.box_heading5 = "" />
    <CFSET THIS.box_heading6 = "" />
    <CFSET THIS.box_content1 = "" />
    <CFSET THIS.box_content2 = "" />
    <CFSET THIS.box_content3 = "" />
	<CFSET THIS.box_content4 = "" />
    <CFSET THIS.box_content5 = "" />
    <CFSET THIS.box_content6 = "" />
    <CFSET THIS.box_link1 = "" />
    <CFSET THIS.box_link2 = "" />
    <CFSET THIS.box_link3 = "" />
	<CFSET THIS.box_link4 = "" />
    <CFSET THIS.box_link5 = "" />
    <CFSET THIS.box_link6 = "" />
    <CFSET THIS.button_text1 = "" />
    <CFSET THIS.button_text2 = "" />
    <CFSET THIS.button_text3 = "" />
	<CFSET THIS.button_text4 = "" />
    <CFSET THIS.button_text5 = "" />
    <CFSET THIS.button_text6 = "" />
	<CFSET THIS.currentimage1 = "" />
    <CFSET THIS.currentimage2 = "" />
    <CFSET THIS.currentimage3 = "" />
	<CFSET THIS.currentimage4 = "" />
    <CFSET THIS.currentimage5 = "" />
    <CFSET THIS.currentimage6 = "" />
	<CFSET THIS.uploadfile1 = "" />
    <CFSET THIS.uploadfile2 = "" />
    <CFSET THIS.uploadfile3 = "" />
	<CFSET THIS.uploadfile4 = "" />
    <CFSET THIS.uploadfile5 = "" />
    <CFSET THIS.uploadfile6 = "" />

	<CFSET THIS.relativepath_prefix = "..\.." />


	<!--- Box Process --->
	<CFFUNCTION NAME="boxprocess" ACCESS="public" RETURNTYPE="struct">

		<CFSET myfile1 = #THIS.currentimage1# />
		<CFSET myfile2 = #THIS.currentimage2# />
		<CFSET myfile3 = #THIS.currentimage3# />
		<CFSET myfile4 = #THIS.currentimage4# />
		<CFSET myfile5 = #THIS.currentimage5# />
		<CFSET myfile6 = #THIS.currentimage6# />

		<!--- Upload the image to the server --->
		<CFSET mydirectory = #THIS.box_UUID#>
        <CFSET currentDirectory = '#request.maindirectory#/_img/boxes/#mydirectory#' />

        <CFIF NOT DirectoryExists(currentDirectory)>
            <CFDIRECTORY ACTION="create" DIRECTORY="#currentDirectory#" >
        </CFIF>

        <CFIF #THIS.uploadfile1# NEQ ''>
            <CFFILE ACTION="upload" FILEFIELD="uploadfile1" DESTINATION="#currentDirectory#/" NAMECONFLICT="Overwrite" />
            <CFSET myfile1 = #FILE.ServerFile#>
        </CFIF>

		<CFIF #THIS.uploadfile2# NEQ ''>
            <CFFILE ACTION="upload" FILEFIELD="uploadfile2" DESTINATION="#currentDirectory#/" NAMECONFLICT="Overwrite" />
            <CFSET myfile2 = #FILE.ServerFile#>
        </CFIF>

		<CFIF #THIS.uploadfile3# NEQ ''>
            <CFFILE ACTION="upload" FILEFIELD="uploadfile3" DESTINATION="#currentDirectory#/" NAMECONFLICT="Overwrite" />
            <CFSET myfile3 = #FILE.ServerFile#>
        </CFIF>

		<CFIF #THIS.uploadfile4# NEQ ''>
            <CFFILE ACTION="upload" FILEFIELD="uploadfile4" DESTINATION="#currentDirectory#/" NAMECONFLICT="Overwrite" />
            <CFSET myfile4 = #FILE.ServerFile#>
        </CFIF>

		<CFIF #THIS.uploadfile5# NEQ ''>
            <CFFILE ACTION="upload" FILEFIELD="uploadfile5" DESTINATION="#currentDirectory#/" NAMECONFLICT="Overwrite" />
            <CFSET myfile5 = #FILE.ServerFile#>
        </CFIF>

		<CFIF #THIS.uploadfile6# NEQ ''>
            <CFFILE ACTION="upload" FILEFIELD="uploadfile6" DESTINATION="#currentDirectory#/" NAMECONFLICT="Overwrite" />
            <CFSET myfile6 = #FILE.ServerFile#>
        </CFIF>

		<!---
		<CFIF #THIS.uploadfile1# NEQ "">
			<CFSCRIPT>
				imgObj = CreateObject("component", "birkhillincludes.tmt_img");
				picHeight = imgObj.getHeight("#THIS.relativepath_prefix#\admin\_img\boxes\#myfile1#");
			    picWidth = imgObj.getWidth("#THIS.relativepath_prefix#\admin\_img\boxes\#myfile1#");
				resizeObj = CreateObject("component", "birkhillincludes.image_resize");
				resizeObj.maxWidth = request.homethumbnail_width;
				resizeObj.maxHeight = 300;
				resizeObj.picHeight = picHeight;
				resizeObj.picWidth = picWidth;
				mynewsizes = resizeObj.resize_image();
				if ((picHeight GT 300) OR (picWidth GT request.homethumbnail_width)) {
					imgObj.resize("#THIS.relativepath_prefix#\admin\_img\boxes\#myfile1#", "#THIS.relativepath_prefix#\admin\_img\boxes\#myfile1#", #mynewsizes.newwidth#);
				}
			</CFSCRIPT>
		</CFIF>

		<CFIF #THIS.uploadfile2# NEQ "">
			<CFSCRIPT>
				imgObj = CreateObject("component", "birkhillincludes.tmt_img");
				picHeight = imgObj.getHeight("#THIS.relativepath_prefix#\_img\boxes\#myfile2#");
			    picWidth = imgObj.getWidth("#THIS.relativepath_prefix#\_img\boxes\#myfile2#");
				resizeObj = CreateObject("component", "birkhillincludes.image_resize");
				resizeObj.maxWidth = request.homethumbnail_width;
				resizeObj.maxHeight = 300;
				resizeObj.picHeight = picHeight;
				resizeObj.picWidth = picWidth;
				mynewsizes = resizeObj.resize_image();
				if ((picHeight GT 300) OR (picWidth GT request.homethumbnail_width)) {
					imgObj.resize("#THIS.relativepath_prefix#\_img\boxes\#myfile2#", "#THIS.relativepath_prefix#\_img\boxes\#myfile2#", #mynewsizes.newwidth#);
				}
			</CFSCRIPT>
		</CFIF>

		<CFIF #THIS.uploadfile3# NEQ "">
			<CFSCRIPT>
				imgObj = CreateObject("component", "birkhillincludes.tmt_img");
				picHeight = imgObj.getHeight("#THIS.relativepath_prefix#\_img\boxes\#myfile3#");
			    picWidth = imgObj.getWidth("#THIS.relativepath_prefix#\_img\boxes\#myfile3#");
				resizeObj = CreateObject("component", "birkhillincludes.image_resize");
				resizeObj.maxWidth = request.homethumbnail_width;
				resizeObj.maxHeight = 300;
				resizeObj.picHeight = picHeight;
				resizeObj.picWidth = picWidth;
				mynewsizes = resizeObj.resize_image();
				if ((picHeight GT 300) OR (picWidth GT request.homethumbnail_width)) {
					imgObj.resize("#THIS.relativepath_prefix#\_img\boxes\#myfile3#", "#THIS.relativepath_prefix#\_img\boxes\#myfile3#", #mynewsizes.newwidth#);
				}
			</CFSCRIPT>
		</CFIF>

		<CFIF #THIS.uploadfile4# NEQ "">
			<CFSCRIPT>
				imgObj = CreateObject("component", "birkhillincludes.tmt_img");
				picHeight = imgObj.getHeight("#THIS.relativepath_prefix#\_img\boxes\#myfile4#");
			    picWidth = imgObj.getWidth("#THIS.relativepath_prefix#\_img\boxes\#myfile4#");
				resizeObj = CreateObject("component", "birkhillincludes.image_resize");
				resizeObj.maxWidth = request.homethumbnail_width;
				resizeObj.maxHeight = 300;
				resizeObj.picHeight = picHeight;
				resizeObj.picWidth = picWidth;
				mynewsizes = resizeObj.resize_image();
				if ((picHeight GT 300) OR (picWidth GT request.homethumbnail_width)) {
					imgObj.resize("#THIS.relativepath_prefix#\_img\boxes\#myfile4#", "#THIS.relativepath_prefix#\_img\boxes\#myfile4#", #mynewsizes.newwidth#);
				}
			</CFSCRIPT>
		</CFIF>

		<CFIF #THIS.uploadfile5# NEQ "">
			<CFSCRIPT>
				imgObj = CreateObject("component", "birkhillincludes.tmt_img");
				picHeight = imgObj.getHeight("#THIS.relativepath_prefix#\_img\boxes\#myfile5#");
			    picWidth = imgObj.getWidth("#THIS.relativepath_prefix#\_img\boxes\#myfile5#");
				resizeObj = CreateObject("component", "birkhillincludes.image_resize");
				resizeObj.maxWidth = request.homethumbnail_width;
				resizeObj.maxHeight = 300;
				resizeObj.picHeight = picHeight;
				resizeObj.picWidth = picWidth;
				mynewsizes = resizeObj.resize_image();
				if ((picHeight GT 300) OR (picWidth GT request.homethumbnail_width)) {
					imgObj.resize("#THIS.relativepath_prefix#\_img\boxes\#myfile5#", "#THIS.relativepath_prefix#\_img\boxes\#myfile5#", #mynewsizes.newwidth#);
				}
			</CFSCRIPT>
		</CFIF>

		<CFIF #THIS.uploadfile6# NEQ "">
			<CFSCRIPT>
				imgObj = CreateObject("component", "birkhillincludes.tmt_img");
				picHeight = imgObj.getHeight("#THIS.relativepath_prefix#\_img\boxes\#myfile6#");
			    picWidth = imgObj.getWidth("#THIS.relativepath_prefix#\_img\boxes\#myfile6#");
				resizeObj = CreateObject("component", "birkhillincludes.image_resize");
				resizeObj.maxWidth = request.homethumbnail_width;
				resizeObj.maxHeight = 300;
				resizeObj.picHeight = picHeight;
				resizeObj.picWidth = picWidth;
				mynewsizes = resizeObj.resize_image();
				if ((picHeight GT 300) OR (picWidth GT request.homethumbnail_width)) {
					imgObj.resize("#THIS.relativepath_prefix#\_img\boxes\#myfile6#", "#THIS.relativepath_prefix#\_img\boxes\#myfile6#", #mynewsizes.newwidth#);
				}
			</CFSCRIPT>
		</CFIF>
		--->

		<CFTRY>

			<CFQUERY NAME="edititem1" DATASOURCE="#THIS.dsn#">

				UPDATE sys_boxes

					SET box_heading = <CFIF THIS.box_heading1 NEQ "">
									      <CFQUERYPARAM VALUE="#THIS.box_heading1#" CFSQLTYPE="cf_sql_varchar" />
									  <CFELSE>
										  <CFQUERYPARAM VALUE="" CFSQLTYPE="cf_sql_varchar" NULL="yes" />
									  </CFIF> ,
						box_content = <CFIF THIS.box_content1 NEQ "">
										  <CFQUERYPARAM VALUE="#THIS.box_content1#" CFSQLTYPE="cf_sql_varchar" />
									  <CFELSE>
										  <CFQUERYPARAM VALUE="" CFSQLTYPE="cf_sql_varchar" NULL="yes" />
									  </CFIF>,
						box_link = <CFIF THIS.box_link1 NEQ "">
									   <CFQUERYPARAM VALUE="#THIS.box_link1#" CFSQLTYPE="cf_sql_varchar" />
								   <CFELSE>
									   <CFQUERYPARAM VALUE="" CFSQLTYPE="cf_sql_varchar" NULL="yes" />
								   </CFIF>,
						box_uploadfile = <CFIF myfile1 is not "">
										 	 <CFQUERYPARAM VALUE="#myfile1#" CFSQLTYPE="cf_sql_varchar" />
										 <CFELSE>
											 <CFQUERYPARAM VALUE="" CFSQLTYPE="cf_sql_varchar" NULL="yes" />
									     </CFIF>,
						button_text = <CFIF THIS.button_text1 NEQ "">
										  <CFQUERYPARAM VALUE="#THIS.button_text1#" CFSQLTYPE="cf_sql_varchar" />
									  <CFELSE>
										  <CFQUERYPARAM VALUE="" CFSQLTYPE="cf_sql_varchar" NULL="yes" />
									  </CFIF>

					WHERE box_UUID = <CFQUERYPARAM VALUE="1" CFSQLTYPE="cf_sql_integer" />

			</CFQUERY>

			<CFQUERY NAME="edititem2" DATASOURCE="#THIS.dsn#">

				UPDATE sys_boxes

					SET box_heading = <CFIF THIS.box_heading2 NEQ "">
									      <CFQUERYPARAM VALUE="#THIS.box_heading2#" CFSQLTYPE="cf_sql_varchar" />
									  <CFELSE>
										  <CFQUERYPARAM VALUE="" CFSQLTYPE="cf_sql_varchar" NULL="yes" />
									  </CFIF> ,
						box_content = <CFIF THIS.box_content2 NEQ "">
										  <CFQUERYPARAM VALUE="#THIS.box_content2#" CFSQLTYPE="cf_sql_varchar" />
									  <CFELSE>
										  <CFQUERYPARAM VALUE="" CFSQLTYPE="cf_sql_varchar" NULL="yes" />
									  </CFIF>,
						box_link = <CFIF THIS.box_link2 NEQ "">
									   <CFQUERYPARAM VALUE="#THIS.box_link2#" CFSQLTYPE="cf_sql_varchar" />
								   <CFELSE>
									   <CFQUERYPARAM VALUE="" CFSQLTYPE="cf_sql_varchar" NULL="yes" />
								   </CFIF>,
						box_uploadfile = <CFIF myfile2 is not "">
										 	 <CFQUERYPARAM VALUE="#myfile2#" CFSQLTYPE="cf_sql_varchar" />
										 <CFELSE>
											 <CFQUERYPARAM VALUE="" CFSQLTYPE="cf_sql_varchar" NULL="yes" />
									     </CFIF>,
						button_text = <CFIF THIS.button_text2 NEQ "">
										  <CFQUERYPARAM VALUE="#THIS.button_text2#" CFSQLTYPE="cf_sql_varchar" />
									  <CFELSE>
										  <CFQUERYPARAM VALUE="" CFSQLTYPE="cf_sql_varchar" NULL="yes" />
									  </CFIF>

					WHERE box_UUID = <CFQUERYPARAM VALUE="2" CFSQLTYPE="cf_sql_integer" />

			</CFQUERY>

			<CFQUERY NAME="edititem3" DATASOURCE="#THIS.dsn#">

				UPDATE sys_boxes

					SET box_heading = <CFIF THIS.box_heading3 NEQ "">
									      <CFQUERYPARAM VALUE="#THIS.box_heading3#" CFSQLTYPE="cf_sql_varchar" />
									  <CFELSE>
										  <CFQUERYPARAM VALUE="" CFSQLTYPE="cf_sql_varchar" NULL="yes" />
									  </CFIF> ,
						box_content = <CFIF THIS.box_content3 NEQ "">
										  <CFQUERYPARAM VALUE="#THIS.box_content3#" CFSQLTYPE="cf_sql_varchar" />
									  <CFELSE>
										  <CFQUERYPARAM VALUE="" CFSQLTYPE="cf_sql_varchar" NULL="yes" />
									  </CFIF>,
						box_link = <CFIF THIS.box_link3 NEQ "">
									   <CFQUERYPARAM VALUE="#THIS.box_link3#" CFSQLTYPE="cf_sql_varchar" />
								   <CFELSE>
									   <CFQUERYPARAM VALUE="" CFSQLTYPE="cf_sql_varchar" NULL="yes" />
								   </CFIF>,
						box_uploadfile = <CFIF myfile3 is not "">
										 	 <CFQUERYPARAM VALUE="#myfile3#" CFSQLTYPE="cf_sql_varchar" />
										 <CFELSE>
											 <CFQUERYPARAM VALUE="" CFSQLTYPE="cf_sql_varchar" NULL="yes" />
									     </CFIF>,
						button_text = <CFIF THIS.button_text3 NEQ "">
										  <CFQUERYPARAM VALUE="#THIS.button_text3#" CFSQLTYPE="cf_sql_varchar" />
									  <CFELSE>
										  <CFQUERYPARAM VALUE="" CFSQLTYPE="cf_sql_varchar" NULL="yes" />
									  </CFIF>

					WHERE box_UUID = <CFQUERYPARAM VALUE="3" CFSQLTYPE="cf_sql_integer" />

			</CFQUERY>

			<CFQUERY NAME="edititem4" DATASOURCE="#THIS.dsn#">

				UPDATE sys_boxes

					SET box_heading = <CFIF THIS.box_heading4 NEQ "">
									      <CFQUERYPARAM VALUE="#THIS.box_heading4#" CFSQLTYPE="cf_sql_varchar" />
									  <CFELSE>
										  <CFQUERYPARAM VALUE="" CFSQLTYPE="cf_sql_varchar" NULL="yes" />
									  </CFIF> ,
						box_content = <CFIF THIS.box_content4 NEQ "">
										  <CFQUERYPARAM VALUE="#THIS.box_content4#" CFSQLTYPE="cf_sql_varchar" />
									  <CFELSE>
										  <CFQUERYPARAM VALUE="" CFSQLTYPE="cf_sql_varchar" NULL="yes" />
									  </CFIF>,
						box_link = <CFIF THIS.box_link4 NEQ "">
									   <CFQUERYPARAM VALUE="#THIS.box_link4#" CFSQLTYPE="cf_sql_varchar" />
								   <CFELSE>
									   <CFQUERYPARAM VALUE="" CFSQLTYPE="cf_sql_varchar" NULL="yes" />
								   </CFIF>,
						box_uploadfile = <CFIF myfile4 is not "">
										 	 <CFQUERYPARAM VALUE="#myfile4#" CFSQLTYPE="cf_sql_varchar" />
										 <CFELSE>
											 <CFQUERYPARAM VALUE="" CFSQLTYPE="cf_sql_varchar" NULL="yes" />
									     </CFIF>,
						button_text = <CFIF THIS.button_text4 NEQ "">
										  <CFQUERYPARAM VALUE="#THIS.button_text4#" CFSQLTYPE="cf_sql_varchar" />
									  <CFELSE>
										  <CFQUERYPARAM VALUE="" CFSQLTYPE="cf_sql_varchar" NULL="yes" />
									  </CFIF>

					WHERE box_UUID = <CFQUERYPARAM VALUE="4" CFSQLTYPE="cf_sql_integer" />

			</CFQUERY>

			<CFQUERY NAME="edititem5" DATASOURCE="#THIS.dsn#">

				UPDATE sys_boxes

					SET box_heading = <CFIF THIS.box_heading5 NEQ "">
									      <CFQUERYPARAM VALUE="#THIS.box_heading5#" CFSQLTYPE="cf_sql_varchar" />
									  <CFELSE>
										  <CFQUERYPARAM VALUE="" CFSQLTYPE="cf_sql_varchar" NULL="yes" />
									  </CFIF> ,
						box_content = <CFIF THIS.box_content5 NEQ "">
										  <CFQUERYPARAM VALUE="#THIS.box_content5#" CFSQLTYPE="cf_sql_varchar" />
									  <CFELSE>
										  <CFQUERYPARAM VALUE="" CFSQLTYPE="cf_sql_varchar" NULL="yes" />
									  </CFIF>,
						box_link = <CFIF THIS.box_link5 NEQ "">
									   <CFQUERYPARAM VALUE="#THIS.box_link5#" CFSQLTYPE="cf_sql_varchar" />
								   <CFELSE>
									   <CFQUERYPARAM VALUE="" CFSQLTYPE="cf_sql_varchar" NULL="yes" />
								   </CFIF>,
						box_uploadfile = <CFIF myfile5 is not "">
										 	 <CFQUERYPARAM VALUE="#myfile5#" CFSQLTYPE="cf_sql_varchar" />
										 <CFELSE>
											 <CFQUERYPARAM VALUE="" CFSQLTYPE="cf_sql_varchar" NULL="yes" />
									     </CFIF>,
						button_text = <CFIF THIS.button_text5 NEQ "">
										  <CFQUERYPARAM VALUE="#THIS.button_text5#" CFSQLTYPE="cf_sql_varchar" />
									  <CFELSE>
										  <CFQUERYPARAM VALUE="" CFSQLTYPE="cf_sql_varchar" NULL="yes" />
									  </CFIF>

					WHERE box_UUID = <CFQUERYPARAM VALUE="5" CFSQLTYPE="cf_sql_integer" />

			</CFQUERY>

			<CFQUERY NAME="edititem6" DATASOURCE="#THIS.dsn#">

				UPDATE sys_boxes

					SET box_heading = <CFIF THIS.box_heading6 NEQ "">
									      <CFQUERYPARAM VALUE="#THIS.box_heading6#" CFSQLTYPE="cf_sql_varchar" />
									  <CFELSE>
										  <CFQUERYPARAM VALUE="" CFSQLTYPE="cf_sql_varchar" NULL="yes" />
									  </CFIF> ,
						box_content = <CFIF THIS.box_content6 NEQ "">
										  <CFQUERYPARAM VALUE="#THIS.box_content6#" CFSQLTYPE="cf_sql_varchar" />
									  <CFELSE>
										  <CFQUERYPARAM VALUE="" CFSQLTYPE="cf_sql_varchar" NULL="yes" />
									  </CFIF>,
						box_link = <CFIF THIS.box_link6 NEQ "">
									   <CFQUERYPARAM VALUE="#THIS.box_link6#" CFSQLTYPE="cf_sql_varchar" />
								   <CFELSE>
									   <CFQUERYPARAM VALUE="" CFSQLTYPE="cf_sql_varchar" NULL="yes" />
								   </CFIF>,
						box_uploadfile = <CFIF myfile6 is not "">
										 	 <CFQUERYPARAM VALUE="#myfile6#" CFSQLTYPE="cf_sql_varchar" />
										 <CFELSE>
											 <CFQUERYPARAM VALUE="" CFSQLTYPE="cf_sql_varchar" NULL="yes" />
									     </CFIF>,
						button_text = <CFIF THIS.button_text6 NEQ "">
										  <CFQUERYPARAM VALUE="#THIS.button_text6#" CFSQLTYPE="cf_sql_varchar" />
									  <CFELSE>
										  <CFQUERYPARAM VALUE="" CFSQLTYPE="cf_sql_varchar" NULL="yes" />
									  </CFIF>

					WHERE box_UUID = <CFQUERYPARAM VALUE="6" CFSQLTYPE="cf_sql_integer" />

			</CFQUERY>

			<CFSET resultStruct.toastCode = 1 />
	        <CFSET resultStruct.toastTitle = "success" />
	        <CFSET resultStruct.toastMessage = "The boxes have been updated!" />
	        <CFRETURN resultStruct />

			<CFCATCH>

				<CFSET resultStruct.toastCode = -1 />
	            <CFSET resultStruct.toastTitle = "error" />
	            <CFSET resultStruct.toastMessage = "Sorry, there was an error updating the boxes!" />
	            <CFRETURN resultStruct />

			</CFCATCH>

		</CFTRY>

	</CFFUNCTION>
	<!--- End Box Process --->


	<!--- Get Box Details --->
	<CFFUNCTION NAME="getbox" ACCESS="public" RETURNTYPE="query">

		<CFQUERY NAME="getBoxDetails" DATASOURCE="#THIS.dsn#">

			SELECT *

				FROM sys_boxes

				WHERE 0 = 0

	            <CFIF THIS.box_UUID NEQ "">
	            	AND box_UUID = <CFQUERYPARAM VALUE="#THIS.box_UUID#" CFSQLTYPE="cf_sql_integer" />
	            </CFIF>

		</CFQUERY>

		<CFRETURN getBoxDetails />

	</CFFUNCTION>
	<!--- End Get Box Details --->


</CFCOMPONENT>
