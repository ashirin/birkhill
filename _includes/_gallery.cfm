<CFSET pagesObj = CreateObject("component","birkhillincludes.pages") />
<cfoutput query="pagedetails">
  <cfset pagesObj.page_UUID = page_UUID />
</cfoutput>
<CFSCRIPT>
  pagesObj.dsn = dsn;
  images = pagesObj.getimages();
</CFSCRIPT>
<div class="">
  <div class="container-fluid" id="gallery">
    <section class="variable slider">
      <CFOUTPUT QUERY="images">
        <div>
          <div class="image" style="background-image: url('images/uploads/#pagesObj.page_UUID#/#image_url#')"></div>
        </div>
      </CFOUTPUT>
    </section>
  </div>
</div>