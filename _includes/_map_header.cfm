


<style>
#map{
  width: 100%;
  height: 420px;
  margin-bottom: 100px;
}
</style>


<script src="https://maps.googleapis.com/maps/api/js?v=3.exp"></script>

<script type="text/javascript">
                function initialize() {
                <CFOUTPUT>
                  var myLatlng = new google.maps.LatLng('56.492877', '-3.051906');


                  var mapOptions = {
                    center: new google.maps.LatLng('56.492877', '-3.051906'),
                    zoom: 12,
                    scrollwheel: false,
                    disableDoubleClickZoom: true,
                    mapTypeId: google.maps.MapTypeId.ROADMAP
                  };
                  var map = new google.maps.Map(document.getElementById("map"),
                      mapOptions);
                  </CFOUTPUT>
                   // To add the marker to the map, use the 'map' property
               <CFOUTPUT>
              var marker = new google.maps.Marker({
                  position: myLatlng,
                  map: map,
                title: 'Birkhill Inn'

              });
              </CFOUTPUT>




                }
                google.maps.event.addDomListener(window, 'load', initialize);
              </script>
