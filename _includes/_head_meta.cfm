
<!--- Get Details of the apge to be used throughout --->
<CFSET pagesObj = CreateObject("component","birkhillincludes.pages") />
<CFSCRIPT>
	pagesObj.dsn = #dsn#;
	pagesObj.page_name = '#ListLast(CGI.SCRIPT_NAME, '/')#';
	pagedetails = pagesObj.getPageDetails();
</CFSCRIPT>


<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">

<!--- Tell the browser to be responsive to screen width --->
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

<CFOUTPUT QUERY="pagedetails">
	<meta name="description" content="#page_description#">
	<meta name="keywords" content="#page_keywords#">

	<title>#page_title#</title>
</CFOUTPUT>

<CFOUTPUT>
	<!--- Favicon --->
	<link type="image/x-icon" rel="shortcut icon" href="#request.baseurl#_img/favicon.ico" >
	<link type="image/x-icon" rel="icon" href="#request.baseurl#_img/favicon.ico" >
</CFOUTPUT>

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-84158632-1', 'auto');
  ga('send', 'pageview');

</script>