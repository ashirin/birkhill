<CFSET slideshowObj = CreateObject("component","birkhillincludes.box") />
<CFSCRIPT>
  slideshowObj.dsn = dsn;
  GetCarousel = slideshowObj.getcarouselFull();
</CFSCRIPT>

<div id="myCarousel" class="carousel slide" data-ride="carousel">
  <!-- Indicators -->
<!---   <ol class="carousel-indicators">
    <cfoutput query="GetCarousel">
      <cfif GetCarousel.id EQ 1>
        <li data-target="##myCarousel" data-slide-to="#GetCarousel.id-1#" class="active"></li>
      <cfelse>
        <li data-target="##myCarousel" data-slide-to="#GetCarousel.id-1#"></li>
      </cfif>
    </cfoutput>
  </ol> --->

  <!-- Wrapper for slides -->
  <div class="carousel-inner">
    <cfoutput query="GetCarousel">
      <div class="item <cfif GetCarousel.id EQ 1>active</cfif>" style="cursor: pointer;">
        <div class="slide" style="background-image: url('admin/_img/slideshow/#GetCarousel.image#')">
          <div class="carousel-caption carouselbox">
            <h3>#GetCarousel.header#</h3>
            <!--- <p>#GetCarousel.span#</p> --->
          </div>
        </div>
      </div>
    </cfoutput>
  </div>

  <!-- Left and right controls -->
<!---   <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a> --->
</div>