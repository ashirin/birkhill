
<!--- jQuery 2.1.4 --->
<script type="text/javascript" language="javascript" src="./_js/jQuery-2.1.4.min.js"></script>

<!--- Bootstrap 3.3.5 --->
<script type="text/javascript" language="javascript" src="./_bootstrap/js/bootstrap.min.js"></script>

<!--- modernizr.js --->
<script type="text/javascript" language="javascript" src="./_js/modernizr.js"></script>

<!--- Toastr --->
<script type="text/javascript" language="javascript" src="./_plugins/toastr/toastr.min.js"></script>

<!--- Validate --->
<script type="text/javascript" language="javascript" src="./_plugins/validate/dist/jquery.validate.js"></script>

<!--- External libraries: jQuery & GreenSock --->
<script src="./_plugins/layerslider/layerslider/js/jquery.js" type="text/javascript"></script>
<script src="./_plugins/layerslider/layerslider/js/greensock.js" type="text/javascript"></script>

<!--- LayerSlider script files --->
<script src="./_plugins/layerslider/layerslider/js/layerslider.transitions.js" type="text/javascript"></script>
<script src="./_plugins/layerslider/layerslider/js/layerslider.kreaturamedia.jquery.js" type="text/javascript"></script>


<script type="text/javascript" language="javascript">
	$('#header-navigation-button').on('click', function() {
		$('.header-navigation-small').slideToggle();
	});
</script>
