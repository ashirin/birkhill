
<!--- Bootstrap 3.3.5 --->
<link type="text/css" rel="stylesheet" href="./_bootstrap/css/bootstrap.min.css">

<!--- Font Awesome --->
<link type="text/css" rel="stylesheet" href="./_css/font-awesome.min.css">

<!--- Parallax CSS --->
<!--- <link type="text/css" rel="stylesheet" href="./_css/parallax.css"> --->

<!--- Google Fonts - Roboto --->
<link href="//fonts.googleapis.com/css?family=Open+Sans+Condensed:300" rel="stylesheet">
<link href='https://fonts.googleapis.com/css?family=Roboto:400,300' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Lato:400,700' rel='stylesheet' type='text/css'>

<!--- Base CSS --->
<link type="text/css" rel="stylesheet" href="./_css/base.css">

<!--- Toastr --->
<link type="text/css" rel="stylesheet" href="./_plugins/toastr/toastr.min.css">

<!--- LayerSlider stylesheet --->
<link rel="stylesheet" href="./_plugins/layerslider/layerslider/css/layerslider.css" type="text/css">

