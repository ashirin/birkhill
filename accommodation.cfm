
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

  <head>
		<CFINCLUDE TEMPLATE="./_includes/_head_meta.cfm" />
    <link type="text/css" rel="stylesheet" href="./_js/slick/slick.css">
    <link rel="stylesheet" type="text/css" href="_js/slick/slick-theme.css">
    <CFINCLUDE TEMPLATE="./_includes/_head_css.cfm" />
    <CFINCLUDE TEMPLATE="./_includes/_head_shim.cfm" />
	</head>

	<body>
		<CFINCLUDE template="./parts/_header.cfm" />
    <cfinclude template="./parts/_subbanner.cfm" />
    <cfinclude template="./_includes/_gallery.cfm" />
    <section class="page content">
      <div class="container">
        <CFOUTPUT QUERY="pagedetails">
          #page_content1#
        </CFOUTPUT>
      </div><!--- ./container --->
    </section>

    <cfinclude template="./parts/_includes.cfm" />
    <cfinclude template="./parts/_tour.cfm" />
    <cfinclude template="./parts/_pagecontent.cfm" />
    <cfinclude template="./parts/_testimonials.cfm" />
    <cfinclude template="./parts/_socials.cfm" />

		<footer>
      <div id="footer">
        <div class="container">
          <CFINCLUDE template="./parts/_footer.cfm" />
        </div><!--- ./container --->
      </div>
    </footer>

		<CFINCLUDE TEMPLATE="./_includes/_head_js.cfm" />
    <script type="text/javascript" language="javascript" src="./_js/slick/slick.js"></script>
    <script type="text/javascript">
       $(document).on('ready', function() {
        // $(".regular").slick({
        //   dots: true,
        //   infinite: true,
        //   slidesToShow: 3,
        //   slidesToScroll: 3
        // });
        // $(".center").slick({
        //   dots: true,
        //   infinite: true,
        //   centerMode: true,
        //   slidesToShow: 3,
        //   slidesToScroll: 3
        // });
        $(".variable").slick({
          infinite: true,
          centerMode: true,
          slidesToShow: 3,
          variableWidth: true
        });
      });
    </script>
    </script>
	</body>
</html>
