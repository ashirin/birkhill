<CFSET boxObj = CreateObject("component","birkhillincludes.box") />
<CFSCRIPT>
  boxObj.dsn = #dsn#;
  boxObj.id = 1;
  getbox1 = boxObj.getpostcard();
  boxObj.id = 2;
  getbox2 = boxObj.getpostcard();
</CFSCRIPT>
<div class="container">
  <div class="row boxes">
    <div class="col-md-6">
      <cfoutput query="getbox1">
        <div class="heading">
          <h1>#box_heading1#</h1>
          <h1>#box_heading2#</h1>
          <h3>#box_heading3#</h3>
        </div>
        <div class="img-container" style='background-image: url("_img/postcards/#box_image#")'>
        </div>
        <div class="content">
          #box_text#
        </div>
      </cfoutput>
    </div>
    <div class="col-md-6">
      <cfoutput query="getbox2">
        <div class="heading">
          <h1>#box_heading1#</h1>
          <h1>#box_heading2#</h1>
          <h3>#box_heading3#</h3>
        </div>
        <div class="img-container" style='background-image: url("_img/postcards/#box_image#")'>
        </div>
        <div class="content">
          #box_text#
        </div>
      </cfoutput>
    </div>
  </div>
</div>