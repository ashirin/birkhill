<cfif pagedetails.ribbon NEQ ''>
  <section id="includes">
    <h3>Every hotel room includes: </h3>

    <ul class="list-inline">
      <li>
        <img src="admin/_img/icon/wifi.png" />
        <p>Free Wi-Fi</p>
      </li>
      <li>
        <img src="admin/_img/icon/monitor.png" />
        <p>Satellite TV</p>
      </li>
      <li>
        <img src="admin/_img/icon/iron.png" />
        <p>Iron</p>
      </li>
      <li>
        <img src="admin/_img/icon/phone.png" />
        <p>Phone</p>
      </li>
      <li>
        <img src="admin/_img/icon/table.png" />
        <p>Writing desk</p>
      </li>
      <li>
        <img src="admin/_img/icon/hairdryer.png" />
        <p>Hairdryer</p>
      </li>
      <li>
        <img src="admin/_img/icon/teeth.png" />
        <p>Toiletries</p>
      </li>
    </ul>
  </section>
</cfif>