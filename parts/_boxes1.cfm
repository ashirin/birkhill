
<CFSET boxObj = CreateObject("component","birkhillincludes.boxes") />
<CFSCRIPT>
	boxObj.dsn = #dsn#;
	boxObj.box_UUID = 1;
	getbox1 = boxObj.getbox();
	boxObj.box_UUID = 2;
	getbox2 = boxObj.getbox();
	boxObj.box_UUID = 3;
	getbox3 = boxObj.getbox();
	boxObj.box_UUID = 4;
	getbox4 = boxObj.getbox();
</CFSCRIPT>


<div class="icon">
  <h1>Our Facilities</h1>
  <div class="container">
    <div class="row icons">
        <div class="col-md-6">
        	<CFOUTPUT QUERY="getbox1">
        		<div class="row">
        			<div class="col-xs-9 col-sm-9 col-md-9 col-lg-9">
        				<div class="heading right">
        					<CFIF box_link NEQ "">
          					<a href="#box_link#"><h1><strong>#box_heading#</strong></h1></a>
          					<p>#box_content#</p>
									<CFELSE>
          					<h1>#box_heading#</h1>
          					<p>#box_content#</p>
          				</CFIF>
        				</div>
        			</div>
        			<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 image">
          			<CFIF box_link NEQ "">
									<a href="#box_link#"><img src="#baseurl#admin/_img/boxes/#box_uploadfile#"></a>
								<CFELSE>
          				<img src="#baseurl#admin/_img/boxes/#box_uploadfile#">
          			</CFIF>
        			</div>
        		</div>
          </CFOUTPUT>
        </div>
        <div class="col-md-6">
            <div class="row">
            	<CFOUTPUT QUERY="getbox2">
                <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 image">
                  <CFIF box_link NEQ "">
										<a href="#box_link#"><img src="#baseurl#admin/_img/boxes/#box_uploadfile#"></a>
									<CFELSE>
            				<img src="#baseurl#admin/_img/boxes/#box_uploadfile#">
            			</CFIF>
                </div>
                <div class="col-xs-9 col-sm-9 col-md-9 col-lg-9">
                  <div class="heading left">
                    <CFIF box_link NEQ "">
            					<a href="#box_link#"><h1>#box_heading#</h1></a>
            					<p>#box_content#</p>
										<CFELSE>
            					<h1>#box_heading#</h1>
            					<p>#box_content#</p>
            				</CFIF>
                  </div>
                </div>
              </CFOUTPUT>
            </div>
        </div>
    </div>
    <div class="row icons">
        <div class="col-md-6">
        	<CFOUTPUT QUERY="getbox3">
        		<div class="row">
        			<div class="col-xs-9 col-sm-9 col-md-9 col-lg-9">
        				<div class="heading right">
        					<CFIF box_link NEQ "">
          					<a href="#box_link#"><h1>#box_heading#</h1></a>
          					<p>#box_content#</p>
									<CFELSE>
          					<h1>#box_heading#</h1>
          					<p>#box_content#</p>
          				</CFIF>
        				</div>
        			</div>
        			<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 image">
          			<CFIF box_link NEQ "">
									<a href="#box_link#"><img src="#baseurl#admin/_img/boxes/#box_uploadfile#"></a>
								<CFELSE>
          				<img src="#baseurl#admin/_img/boxes/#box_uploadfile#">
          			</CFIF>
        			</div>
        		</div>
          </CFOUTPUT>
        </div>
        <div class="col-md-6">
            <div class="row">
            	<CFOUTPUT QUERY="getbox4">
                <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 image">
                  <CFIF box_link NEQ "">
										<a href="#box_link#"><img src="#baseurl#admin/_img/boxes/#box_uploadfile#"></a>
									<CFELSE>
            				<img src="#baseurl#admin/_img/boxes/#box_uploadfile#">
            			</CFIF>
                </div>
                <div class="col-xs-9 col-sm-9 col-md-9 col-lg-9">
                  <div class="heading left">
                    <CFIF box_link NEQ "">
            					<a href="#box_link#"><h1>#box_heading#</h1></a>
            					<p>#box_content#</p>
										<CFELSE>
            					<h1>#box_heading#</h1>
            					<p>#box_content#</p>
            				</CFIF>
                  </div>
                </div>
              </CFOUTPUT>
            </div>
        </div>
      </div>
    </div>
  <div class="bubble"></div>
</div>

