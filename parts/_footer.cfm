<div class="row first">
    <div class="col-md-4 col-sm-12 col-xs-12">
    <cfoutput><img src="#request.baseurl#_img/Birkhill Logo White.png"/></cfoutput>
  </div>
  <div class="col-md-8 col-sm-12">
    <div class="center contact">
      <div class="phone-up">
        01382 581297
      </div>
      <div class="email-up">
        <i class="fa fa-envelope" aria-hidden="true"></i>
        <a href="mailto:woodlands@bestwestern.co.uk"><span class="header-email">infobirkhill@redwoodleisure.co.uk</span></a>
      </div>
    </div>
  </div>
</div>
<div class="row text-left">
  <div class="footer-part col-md-4 hidden-sm hidden-xs">
    <strong>THE BIRKHILL INN</strong><br />
    124 Coupar Angus Road<br />
    Birkhill, By Dundee<br />
    DD5 5PG<br />
  </div>
  <div class="footer-part col-md-3 hidden-sm hidden-xs">
    <strong>ABOUT US</strong><br />
      <a href="./about.cfm">BIRKHILL INN</a><br />
  </div>
  <div class="footer-part col-md-3 hidden-sm hidden-xs">
    <strong>SERVICES</strong><br />
      <a href="./menus.cfm">MENUS</a><br />
<!---       <a href="./weddings.cfm">SPECIAL OFFERS</a><br /> --->
  </div>
	<div class="footer-part col-md-2 hidden-sm hidden-xs">
		<strong>LINKS</strong><br />
      <a href="about.cfm">ABOUT US</a><br />
      <a href="http://www.invercarsehotel.co.uk/">INVERCARSE HOTEL</a><br />
      <a href="http://woodlandsdundee.co.uk/">WOODLANDS HOTEL</a><br />
      <!--- <a href="contact.cfm">CONTACT US</a><br /> --->
	</div>
</div>
