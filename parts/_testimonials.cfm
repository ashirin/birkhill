
<CFSET slideshowObj = CreateObject("component","birkhillincludes.box") />
<CFSCRIPT>
  slideshowObj.dsn = dsn;
  getTestimonials = slideshowObj.gettestimonials();
</CFSCRIPT>

<div id="tCarousel" class="carousel slide" data-ride="carousel">
  <!-- Indicators -->
  <ol class="carousel-indicators">
    <cfif getTestimonials.line1 NEQ "" >
      <li data-target="#tCarousel" data-slide-to="0" class="active"></li>
    </cfif>
    <cfif getTestimonials.line2 NEQ "" >
      <li data-target="#tCarousel" data-slide-to="1"></li>
    </cfif>
    <cfif getTestimonials.line3 NEQ "" >
      <li data-target="#tCarousel" data-slide-to="2"></li>
    </cfif>
    <cfif getTestimonials.line4 NEQ "" >
      <li data-target="#tCarousel" data-slide-to="3"></li>
    </cfif>
    <cfif getTestimonials.line5 NEQ "" >
      <li data-target="#tCarousel" data-slide-to="4"></li>
    </cfif>
    <cfif getTestimonials.line6 NEQ "" >
      <li data-target="#tCarousel" data-slide-to="5"></li>
    </cfif>
  </ol>

  <!-- Wrapper for slides -->
  <div class="carousel-inner">
    <cfoutput>
      <cfif getTestimonials.line1 NEQ "" >
        <div class="item active">
          <div class="carousel-caption carouselbox">
            <p>#getTestimonials.line1#</p>
          </div>
        </div>
      </cfif>
      <cfif getTestimonials.line2 NEQ "" >
        <div class="item">
          <div class="carousel-caption carouselbox">
            <p>#getTestimonials.line2#</p>
          </div>
        </div>
      </cfif>
      <cfif getTestimonials.line3 NEQ "" >
        <div class="item">
          <div class="carousel-caption carouselbox">
            <p>#getTestimonials.line3#</p>
          </div>
        </div>
      </cfif>
      <cfif getTestimonials.line4 NEQ "" >
        <div class="item">
          <div class="carousel-caption carouselbox">
            <p>#getTestimonials.line4#</p>
          </div>
        </div>
      </cfif>
      <cfif getTestimonials.line5 NEQ "" >
        <div class="item">
          <div class="carousel-caption carouselbox">
            <p>#getTestimonials.line5#</p>
          </div>
        </div>
      </cfif>
      <cfif getTestimonials.line6 NEQ "" >
        <div class="item">
          <div class="carousel-caption carouselbox">
            <p>#getTestimonials.line6#</p>
          </div>
        </div>
      </cfif>
    </cfoutput>

  </div>

  <!-- Left and right controls -->
<!---   <a class="left carousel-control" href="#tCarousel" role="button" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="right carousel-control" href="#tCarousel" role="button" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a> --->
</div>