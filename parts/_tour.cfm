<section id="tour">
  <div class="container">
    <a href="https://www.google.co.uk/maps/place/Best+Western+Woodlands+Hotel+Dundee/@56.4739085,-2.8518494,14a,66.4y,338.46h,77.53t/data=!3m8!1e1!3m6!1sFkS8hbDPcksAAAQzq_lBjQ!2e0!3e2!6s%2F%2Fgeo1.ggpht.com%2Fcbk%3Fpanoid%3DFkS8hbDPcksAAAQzq_lBjQ%26output%3Dthumbnail%26cb_client%3Dsearch.TACTILE.gps%26thumb%3D2%26w%3D129%26h%3D106%26yaw%3D335.24167%26pitch%3D0!7i13312!8i6656!4m13!1m7!3m6!1s0x48865ef26f7f2d51:0xdf53ce0ee85813c5!2sThe+Cottage,+Dundee!3b1!8m2!3d56.4738246!4d-2.8511081!3m4!1s0x0:0xc35b7d6ff0cab7d3!8m2!3d56.4745877!4d-2.8522161!6m1!1e1" target="_blank" class="custom dark">TAKE A VIRTUAL TOUR</a>
    <ul class="list-inline">
      <li>
        <div>
          <img src="admin/_img/single.jpg" />
        </div>
        <p><strong>Single</strong></p>
      </li>
      <li>
        <div>
          <img src="admin/_img/twin.jpg" />
        </div>
        <p><strong>Twin</strong></p>
      </li>
      <li>
        <div>
          <img src="admin/_img/double.jpg" />
        </div>
        <p><strong>Double</strong></p>
      </li>

  </div>
</section>