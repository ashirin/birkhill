<section id="top">
  <div class="container">
    <div class="row">
      <div class="hidden-xs hidden-sm col-md-4 pull-bottom">
        <div class="phone-up hidden-xs hidden-sm">
          01382 581297
        </div>
      </div>
      <div class="col-xs-12 col-sm-6 col-md-5 pull-bottom">
          <cfoutput><img src="#request.baseurl#_img/Birkhill Logo.png" class="img-responsive weird-margin " />
        </cfoutput>
      </div>
      <div class="hidden-xs col-sm-3 pull-bottom bigger">
        <div class="book-container">
<!---           <div>
            <a id="book-now" href="book.cfm">BOOK NOW</a>
          </div> --->
          <ul class="brandings list-inline pull-right no-margin">
            <li><a href="##"></a></li>
            <li><a href="http://twitter.com/birkhillinn" id="twitter" target="_blank"></a></li>
            <li><a href="http://www.facebook.com/BirkhillInn" id="facebook" target="_blank"></a></li>
          </ul>
        </div>
      </div>
    </div>
  </div>
</section>
<CFSET menusObj = CreateObject("component","birkhillincludes.menus") />
<CFSCRIPT>
	menusObj.dsn = dsn;
	menusObj.menu_type = 'main';
	menusObj.parent_menu_UUID = 'top';
	getmenu = menusObj.getMenu();
</CFSCRIPT>

<div id="navcontainer extra-margin">
  <div class="navbar-wrapper">
    <div>
      <nav id="menu" class="navbar navbar-inverse navbar-static-top">
        <div class="container">
          <div class="visible-xs-block navbar-header">
            <button type="button" id="hamburger" class="collapsed navbar-toggle" data-toggle="collapse" data-target="#navbar" aria-expanded="false" href="#navbar">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="index.cfm">
              <div class="branding"><span class="sr-only">Biopta</span></div>
            </a>
          </div>
          <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav  text-center">
							<CFOUTPUT QUERY="getmenu">
								<li><a href="./#page_name#">#menu_name#</a></li>
							</CFOUTPUT>
            </ul>
          </div>
        </div>
      </nav>
    </div>
  </div>
</div> <!-- navcontaienr -->




<!--->
<div class="header-navigation">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<CFINCLUDE template="./_menu_main.cfm" />
			</div><!--- ./col --->
		</div><!--- /.row --->
	</div><!--- ./container --->
</div><!--- ./header-logo --->
--->

<!--->
<div class="header-logo">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<img src="./_img/logo.png" width="250" />
			</div><!--- ./col --->
		</div><!--- /.row --->
	</div><!--- ./container --->
</div><!--- ./header-logo --->

<div class="header-navigation">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<CFINCLUDE template="./_menu_main.cfm" />
			</div><!--- ./col --->
		</div><!--- /.row --->
	</div><!--- ./container --->
</div><!--- ./header-navigation --->
--->

<!--->
<div id="header-large-menu" class="col-lg-6 col-md-6 hidden-sm hidden-xs">
		<CFINCLUDE template="./_menu_main.cfm" />
	</div><!--- ./col --->

	<div class="row">
	<div id="header-small-menu" class="hidden-lg hidden-md">
		<CFINCLUDE template="./_menu_main.cfm" />
	</div><!--- ./col --->
</div><!--- ./row --->
--->
