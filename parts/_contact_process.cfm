
<!--- Check for sessionUUID --->
<CFIF NOT IsDefined('session.UUID')>
	<CFSET locationurl = "../contact.cfm?toastcode=-1&toasttitle=Error&toastmessage=The contact form has expired. Please go backand refresh the page before submitting the form." />
<CFELSE>

	<!--- Validate the sessionUUID --->
	<CFIF IsDefined('form.UUID')>
		<CFSET myUUID = #form.UUID#>
	<CFELSE>
		<CFSET myUUID = "123">
	</CFIF>

	<CFIF session.UUID EQ myUUID>

		<CFSET myflag = 0 />

		<!--- Check for spam by looking for suspicious strings --->
		<CFSCRIPT>
			function spam_test( str1 ) {
				myflag = 0;
				if ( FindNoCase( "Content-Type:", str1 ) NEQ 0 ) { myflag = 1; }
				if ( FindNoCase( "MIME-Version:", str1 ) NEQ 0 ) { myflag = 1; }
	  			if ( FindNoCase( "To: ", str1 ) NEQ 0 ) { myflag = 1; }
	  			if ( FindNoCase( "From: ", str1 ) NEQ 0 ) { myflag = 1; }
	  			if ( FindNoCase( "<a href=", str1 ) NEQ 0 ) { myflag = 1; }
				if ( FindNoCase( "bcc: ", str1 ) NEQ 0 ) { myflag = 1; }
		  		if ( FindNoCase( "Subject: ", str1 ) NEQ 0 ) { myflag = 1; }
		  		return myflag;
		  	}
		</CFSCRIPT>

		<CFSET myflag1 = spam_test(form.contact_name) />
		<CFSET myflag2 = spam_test(form.contact_email) />
		<CFSET myflag3 = spam_test(form.contact_company) />
		<CFSET myflag4 = spam_test(form.contact_message) />

		<CFSET myflag = myflag1 + myflag2 + myflag3 + myflag4 />

		<!--- Check the name and email are different values --->
		<CFIF form.contact_name EQ form.contact_email>
			<CFSET myflag = 1>
		</CFIF>

		<!--- Check for an invalid email --->
		<CFIF NOT isValid('email', #form.contact_email#)>
			<CFSET myflag = 1>
		</CFIF>

		<!--- Check domain the message has come from --->
		<CFSWITCH EXPRESSION="#cgi.SERVER_NAME#">
			<CFCASE VALUE="127.0.0.1">
	        	<CFSET myflag = 0 />
	    	</CFCASE>
	    	<CFCASE VALUE="localhost">
	        	<CFSET myflag = 0 />
	    	</CFCASE>
	    	<CFCASE VALUE="splwebsite.bannermantechnology.co.uk">
	        	<CFSET myflag = 0 />
	    	</CFCASE>
			<!--- Used to check actual domain when known
			<CFCASE VALUE="splwebsite.bannermantechnology.co.uk">
	        	<CFSET myflag = 0 />
	    	</CFCASE>
			--->
	    	<CFDEFAULTCASE>
	        	<CFSET myflag = 1 />
	    	</CFDEFAULTCASE>
		</CFSWITCH>

		<!--- Check date difference on submission --->
	  	<CFIF DateDiff("s", form.timeposted, now()) LTE 3>
	    	<CFSET timetocomplete = DateDiff("s", form.timeposted, now()) />
	    	<CFSET myflag = 1 />
	  	</CFIF>

	  	<CFIF DateDiff("d", form.timeposted, now()) GTE 1>
	  		<CFSET myflag = 1 />
		</CFIF>

		<!--- Span attempt email --->
		<CFIF myflag GT 0>

			<CFSET locationurl = "../contact.cfm?toastcode=-1&toasttitle=Error&toastmessage=This submission contains code that can be used to send spam so will not be sent." />

			<!--- Email to send if a spam attempt is made
			<CFMAIL FROM="information@munro-group.eu" TO="information@munro-group.eu" TYPE="html" SUBJECT="SPL Website: Spam Attempt">

				<p>SPAM Attempt - Code</p>
	      		<CFOUTPUT>#cgi.REMOTE_ADDR#</CFOUTPUT>

			</CFMAIL>
			--->
		<CFELSE>

			<CFSCRIPT>
				function trimFalseEmailHeaders( str2 ) {
					str2 = replaceNoCase( str2, "Content-Type:", "content-type;", "ALL" );
					str2 = replaceNoCase( str2, "MIME-Version:", "mime-version;", "ALL" );
					str2 = replaceNoCase( str2, "To: ", "to; ", "ALL" );
					str2 = replaceNoCase( str2, "From: ", "from; ", "ALL" );
					str2 = replaceNoCase( str2, "bcc: ", "bcc; ", "ALL" );
					str2 = replaceNoCase( str2, "Subject: ", "subject; ", "ALL" );
					return str2;
				}
			</CFSCRIPT>

			<CFSET myname = trimFalseEmailHeaders(StripCR(trim(htmleditformat(form.contact_name)))) />
			<CFSET myemail = trimFalseEmailHeaders(StripCR(trim(htmleditformat(form.contact_email)))) />
			<CFSET mycompany = trimFalseEmailHeaders(StripCR(trim(htmleditformat(form.contact_company)))) />
			<CFSET mymessage = trimFalseEmailHeaders(StripCR(trim(htmleditformat(form.contact_message)))) />


			<!--- Forward the contact submission --->
			<CFMAIL FROM="information@munro-group.eu" TO="information@munro-group.eu" TYPE="html" SUBJECT="SPL Website: Website Enquiry">

				<strong>Name</strong>: #myname#<br/>
	    		<strong>Email</strong>: #myemail#<br/>
	    		<strong>Company</strong>: #mycompany#<br/>
				<br/>
	    		<strong>Message</strong>: #mymessage#

			</CFMAIL>

			<CFSET locationurl = "../contact.cfm?toastcode=1&toasttitle=Success&toastmessage=Thank you for contacting us with your enquiry. We will be in touch shortly." />

		</CFIF>

	<CFELSE>

		<CFSET locationurl = "../contact.cfm?toastcode=-1&toasttitle=Error&toastmessage=This email has not been sent as it may be spam." />

		<!--- Email to send if a spam attempt is made
		<CFMAIL FROM="information@munro-group.eu" TO="information@munro-group.eu" TYPE="html" SUBJECT="SPL Website: Spam Attempt">

			<p>SPAM Attempt - Code</p>
	     	<CFOUTPUT>#cgi.REMOTE_ADDR#</CFOUTPUT>

		</CFMAIL>
		--->

	</CFIF>

</CFIF>

<!--- Send result back to contact.cfm with toast variables --->
<CFLOCATION URL="#locationurl#" />
