
<form role="form" action="./parts/_contact_process.cfm" method="post" id="myform" name="myform">

	<!--- Set a hidden sessionUUID and timestamp --->
	<CFOUTPUT>
		<CFSET session.UUID = CreateUUID()>
		<input type="hidden" name="UUID" id="UUID" value="#session.UUID#" />
		<input type="hidden" name="timeposted" id="timeposted" value="#NOW()#" />
	</CFOUTPUT>

    <div class="row">

		<div class="col-lg-10 col-md-10 col-sm-12 col-xs-12 contact-form">

			<div class="form-group">
				<label for="contact_name">Name</label>  <span style="font-size: 20px; color: #ff0000">*</span><br />
				<input type="text" value="" class="form-control" name="contact_name" id="contact_name" />
			</div>

			<div class="form-group">
				<label for="contact_email">Email</label> <span style="font-size: 20px; color: #ff0000">*</span><br />
				<input type="email" value="" class="form-control" name="contact_email" id="contact_email" />
			</div>

			<div class="form-group">
				<label for="contact_company">Company</label> <span style="font-size: 20px;">&nbsp;</span><br />
				<input type="text" value="" class="form-control" name="contact_company" id="contact_company" id="required" />
			</div>

			<div class="form-group">
				<label for="contact_message">Message</label> <span style="font-size: 20px; color: #ff0000">*</span><br />
				<textarea class="form-control" name="contact_message" id="contact_message"></textarea>
			</div>

			<br />

			<div class="form-group">
	    		<input type="submit" value="Send Message" name="submit" id="submit" class="btn btn-lg btn-primary" />
	    	</div>

		</div><!--- .col --->

    </div><!--- .row --->

</form>
