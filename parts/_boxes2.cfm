
<CFSET boxObj = CreateObject("component","birkhillincludes.boxes") />
<CFSCRIPT>
	boxObj.dsn = #dsn#;
	boxObj.box_UUID = 4;
	getbox4 = boxObj.getbox();
	boxObj.box_UUID = 5;
	getbox5 = boxObj.getbox();
	boxObj.box_UUID = 6;
	getbox6 = boxObj.getbox();
</CFSCRIPT>


<div class="row">

	<div class="cms-box col-md-4 col-sm-12 col-xs-12">
		<CFOUTPUT QUERY="getbox4">
			<CFIF box_link NEQ "">
				<a href="#box_link#"><img src="#request.baseurl#admin/_img/boxes/#box_uploadfile#"></a>
				<br />
				<a href="#box_link#"><h2>#box_heading#</h2></a>
				<div class="boxcontent2">#box_content#</div>
			<CFELSE>
				<img src="#request.baseurl#admin/_img/boxes/#box_uploadfile#">
				<br />
				<h2>#box_heading#</h2>
				<div class="boxcontent2">#box_content#</div>
			</CFIF>
		</CFOUTPUT>
	</div>

	<div class="cms-box col-md-4 col-sm-12 col-xs-12">
		<CFOUTPUT QUERY="getbox5">
			<CFIF box_link NEQ "">
				<a href="#box_link#"><img src="#request.baseurl#admin/_img/boxes/#box_uploadfile#"></a>
				<br />
				<a href="#box_link#"><h2>#box_heading#</h2></a>
				<div class="boxcontent2">#box_content#</div>
			<CFELSE>
				<img src="#request.baseurl#admin/_img/boxes/#box_uploadfile#">
				<br />
				<h2>#box_heading#</h2>
				<div class="boxcontent2">#box_content#</div>
			</CFIF>
		</CFOUTPUT>
	</div>

	<div class="cms-box col-md-4 col-sm-12 col-xs-12">
		<CFOUTPUT QUERY="getbox6">
			<CFIF box_link NEQ "">
				<a href="#box_link#"><img src="#request.baseurl#admin/_img/boxes/#box_uploadfile#"></a>
				<br />
				<a href="#box_link#"><h2>#box_heading#</h2></a>
				<div class="boxcontent2">#box_content#</div>
			<CFELSE>
				<img src="#request.baseurl#admin/_img/boxes/#box_uploadfile#">
				<br />
				<h2>#box_heading#</h2>
				<div class="boxcontent2">#box_content#</div>
			</CFIF>
		</CFOUTPUT>
	</div>

</div>
